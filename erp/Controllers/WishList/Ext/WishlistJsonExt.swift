//
//  GetWishListsJsonExtensionVC.swift
//  erp-ios
//
//  Created by apple on 13/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation

extension WishListVC{
    func getWishListsFromServer(){
        CustomActivityIndicator.instance.showCustomActivityIndicator(view)
        getApiClient().fetchApiResults(urlString: Urls.GET_USER_WISHLISTS, parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<WishList>)->()
            in
            self.performUIUpdatesOnMain {
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                if response.data.count > 0 {
                    setAllWishLists(wishLists:  response.data)
                    //TODO : UNCOMENT AFTER FINISHIN TESTIN
                    //                        storeWishListsInRealm()
                    self.wishlistItemsUpdated()
                }
                CustomActivityIndicator.instance.hideCustomActivityIndicator()
                self.refresher.endRefreshing()
                self.showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: self.wishlists.count)
            }
        })
    }
}
