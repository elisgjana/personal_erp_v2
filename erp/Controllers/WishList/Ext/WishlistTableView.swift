//
//  TableView.swift
//  erp
//
//  Created by Akil Rajdho on 14/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension WishListVC {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        warehouseId = wishlists[indexPath.row].warehouse_id!
        self.performSegue(withIdentifier: "showWishListItems", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wishlists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "wishListCell", for: indexPath) as! WishListCell
        let wishlist = wishlists[indexPath.row]
        cell.warehouseNameLabel.text = wishlist.warehouse_name ?? "-"
        cell.dateLabel.text = transfromDateToLongDate(date: String(describing: wishlist.date ?? "1970-01-01"))
        cell.totalNumberOfProductsLabel.text = String(describing: wishlist.details?.count ?? 0)
        styleWishlistTable(cell,indexPath)
        
        return cell
    }
}
