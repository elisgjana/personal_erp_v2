//
//  NewWishListViewController.swift
//  erp-ios
//
//  Created by apple on 07/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import UIKit
import MTBBarcodeScanner
import RealmSwift
import CoreLocation
import NVActivityIndicatorView

class NewWishlistItemVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, AVCaptureMetadataOutputObjectsDelegate {
    @IBOutlet weak var calendarImage: UIImageView!
    @IBOutlet weak var selectDateView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    @IBOutlet weak var searchImage: UIImageView!
    @IBOutlet weak var scaleImage: UIImageView!
    @IBOutlet weak var warehouseLabel: UILabel!
    @IBOutlet weak var searchWarehouseView: UIView!
    @IBOutlet weak var warehouseImage: UIImageView!
    @IBOutlet var barcodeView: UIView!
    @IBOutlet weak var searchProductView: UIView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var roundBackgroundView: UIView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var searchProductLabel: UILabel!

    var blackView = UIView()
    var whiteView = UIView()
    var productsSearchBar = UISearchBar()
    var warehousesSearchBar = UISearchBar()
    var productsTableView = UITableView()
    var warehousesTableView = UITableView()
    var viewTapped = ""
    var products = [ProductModel]()
    var productsCopy = [ProductModel]()
    var warehouses = [WarehouseModel]()
    var warehousesCopy = [WarehouseModel]()
    var temp = 0
    var scanner: MTBBarcodeScanner?
    var wishlistDetailUuid = ""
    var wishListDetailDate = ""
    var selectedWarehouseId = 0
    var selectedProductId = 0
    var wishListDetailQuantity = 0.0
    var selectedDate = ""
    var wishListDetailLatitude = currentLatitude
    var wishListDetailLongitude = currentLongitude
    var defaultWarehouseName = getWishlistDetailWarehouse()
    
    let calendarLauncher = CalendarLauncher()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeAppearanceComponents()
        queryProducts()
        queryWarehouses()
        productsCopy = products
        warehousesCopy = warehouses
        initializeComponents()
        scanner = MTBBarcodeScanner(previewView: cameraView)
        initializeDateSelection()
        initializeProductSelection()
        initializeWarehouseSelection()
        
    }
    
    func queryProducts(){
        for each in uiRealm.objects(ProductModel.self){
            self.products.append(each)
        }
    }
    
    func queryWarehouses(){
        for each in uiRealm.objects(WarehouseModel.self){
            self.warehouses.append(each)
        }
    }
    
    func initializeDateSelection(){
        
        let tapDateGesture = UITapGestureRecognizer(target: self, action: #selector(NewWishlistItemVC.dateTapped(gesture:)))
        selectDateView.addGestureRecognizer(tapDateGesture)
        selectDateView.isUserInteractionEnabled = true
        
        calendarLauncher.chooseButton.addTarget(self, action: #selector(handleChooseDate), for: .touchUpInside)
    }
    
    func initializeProductSelection(){
        //here I display the choose product slide from bottom
        let tapProduct = UITapGestureRecognizer(target: self, action: #selector(NewWishlistItemVC.productTapped(gesture:)))
        
        //here I add the barcodescanner button on nav bar
        let barcodeButton = UIBarButtonItem(image: UIImage(named:"barcode"), style: .plain, target: self, action: #selector(NewWishlistItemVC.barcodeScannerTUP(_:)))
        self.navigationItem.rightBarButtonItem = barcodeButton
        searchProductView.addGestureRecognizer(tapProduct)
        searchProductView.isUserInteractionEnabled = true
    }
    func initializeWarehouseSelection(){
        //here I make the choose warehouse slide from bottom
        let tapWarehouseGesture = UITapGestureRecognizer(target: self, action: #selector(NewWishlistItemVC.warehouseTapped(gesture:)))
        searchWarehouseView.addGestureRecognizer(tapWarehouseGesture)
        searchWarehouseView.isUserInteractionEnabled = true
    }
    
    @objc func dateTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            calendarLauncher.showCalendar()
        }
        
    }
    @objc func productTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            viewTapped = "Product"
            handleSelectItem()
        }
        
    }
    
    @objc func warehouseTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            viewTapped = "Warehouse"
            handleSelectItem()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.scanner?.stopScanning()
    }
    
    @objc func barcodeScannerTUP(_ sender:UIBarButtonItem!)
    {
//        darkView.isHidden = false
        barcodeView.isHidden = false
        view.addSubview(barcodeView)
        QRGenerator()
        barcodeView.center = CGPoint(x: view.frame.width / 2, y: view.frame.height / 2 - 30)
    }
    
    func initializeAppearanceComponents(){
        self.title = Strings.NEW_WISH_LIST_TITLE
        createRoundCorner(barcodeView, 10)
        createRoundCorner(roundBackgroundView, 10)
        titleView.roundCorners([.topRight, .topLeft], radius: 10)
        searchImage.tintColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        scaleImage.tintColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        warehouseImage.tintColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        calendarImage.tintColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        createRoundCorner(saveButton, 10)
        activityIndicator.color = createColorFromHex(hex: Colors.PRIMARY_DARK)
        activityIndicator.type = .ballSpinFadeLoader
        amountField.keyboardType = .decimalPad
    }
    
    func initializeComponents(){
        productsTableView.delegate = self
        productsTableView.dataSource = self
        warehousesTableView.delegate = self
        warehousesTableView.dataSource = self
        productsSearchBar.delegate = self
        warehousesSearchBar.delegate = self
        let productNib = UINib(nibName: "ProductCell", bundle: nil)
        let warehouseNib = UINib(nibName: "SearchWarehouseCell", bundle: nil)
        productsTableView.register(productNib, forCellReuseIdentifier: "productCell")
        warehousesTableView.register(warehouseNib, forCellReuseIdentifier: "searchWarehouseCell")
        productsTableView.rowHeight = 151
        warehousesTableView.rowHeight = 50
        self.productsTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.warehousesTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.productsTableView.backgroundColor = UIColor.groupTableViewBackground
        self.warehousesTableView.backgroundColor = UIColor.groupTableViewBackground
        barcodeView.isHidden = true
        hideKeyboardOnViewTap()
        let defaultWarehouse = uiRealm.objects(WarehouseModel.self).filter("warehouse_id = \(SavedVariables.getDefaultWarehouseId())").first
        defaultWarehouseName = warehouseLabel.text!
        self.selectedWarehouseId = SavedVariables.getDefaultWarehouseId() ?? 0
        
    }
    @IBAction func saveButtonTUP(_ sender: Any) {
        if validateData(){
            wishlistDetailUuid = NSUUID().uuidString
            wishListDetailDate = Device.getCurrentDate()
            if let quantity = Double(amountField.text!) {
                wishListDetailQuantity = quantity
            }
            activityIndicator.startAnimating()
            let when = DispatchTime.now() + 0.75 // change 0.75 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                // Code with delay
              self.createWishListItem()
            }
            
            
        }
    }
    func validateData()->Bool{
        if searchProductLabel.text! == Strings.PRODUCT_SEARCHBAR_PLACEHOLDER || amountField.text! == "" || selectedWarehouseId == 0   {
            presentTopNotification(title: Strings.WARNING_TITLE, subtitle: Strings.FILL_ALL_THE_DATA, image: "warning", color: Colors.INOVACION_YELLOW, duration: Constants.TOP_NOTIFICATION_DURATION)
            return false
        }
        return true
    }
    
    @IBAction func closeBarcodeViewTUP(_ sender: Any) {
        self.scanner?.stopScanning()
        barcodeView.removeFromSuperview()
    }
    func emptyFields(){
        warehouseLabel.text = getWishlistDetailWarehouse()
        selectedWarehouseId = Int(getWishlistDetailWarehouseId())
        warehouseLabel.textColor = UIColor.gray
        searchProductLabel.text = Strings.PRODUCT_SEARCHBAR_PLACEHOLDER
        searchProductLabel.textColor = UIColor.gray
        amountField.text = ""
        amountField.textColor = UIColor.gray
    }
    
    @objc private func  handleChooseDate(){
        let date = calendarLauncher.getSelectedDate()
        dateLabel.text = date
        selectedDate = date
        calendarLauncher.handleDismiss()
    }
}
