//
//  NewWishListViewControllerExtension.swift
//  erp-ios
//
//  Created by apple on 21/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation
import AVKit
import AVFoundation
import MTBBarcodeScanner
import UIKit

extension NewWishlistItemVC{
    
    func QRGenerator(){
        MTBBarcodeScanner.requestCameraPermission(success: { success in
            if success {
                do {
                    try self.scanner?.startScanning(resultBlock: { codes in
                        if let codes = codes {
                            let code = codes[0]
                            let stringValue = code.stringValue!
                            if stringValue != ""{
                                var finalCode: String = ""
                                var productId: Int32 = 0
                                var productWeightInKg: Double = 0.0
                                
                                if stringValue.substring(fromIndex: 0, toIndex: 1) != "00" && stringValue.substring(fromIndex: 0, toIndex: 1) != "01" {
                                    finalCode = stringValue
                                    let productScanned = uiRealm.objects(ProductModel.self).filter("external_bar_code == '\(finalCode)' OR internal_bar_code == '\(finalCode)' ").first
                                    if let productName = productScanned?.name!{
                                        self.searchProductLabel.text = productName
                                        self.searchProductLabel.textColor = UIColor.black
                                    }

                                }else{
                                    //TODO : CHECK LOGIC HERE WITH PROB 99% ELIS IS WRONG
                                    finalCode = stringValue.substring(fromIndex: 2, toIndex: 6)
                                    if let id = Int32(finalCode) {
                                        productId = id
                                        let productScanned = uiRealm.objects(ProductModel.self).filter("id == \(productId)").first
                                        if let productName = productScanned?.name!{
                                            self.searchProductLabel.text = productName
                                            self.searchProductLabel.textColor = UIColor.black
                                        }
                                    }
                                    if let weightInGram = Int32(stringValue.substring(fromIndex: 7, toIndex: 11)){
                                        productWeightInKg = Double(weightInGram) / 1000.0
                                        self.amountField.text = "\(String(productWeightInKg)) KG"
                                    }
                                }
                                self.scanner?.stopScanning()
                                self.barcodeView.removeFromSuperview()
                            }
                        }
                    })
                } catch {
                    NSLog("Unable to start scanning")
                }
            } else {
                self.displayAlert(title: Strings.SCANNING_UNAVAILABLE, message: Strings.NO_CAMERA_PERMISSION)
            }
        })
    }
    
}
