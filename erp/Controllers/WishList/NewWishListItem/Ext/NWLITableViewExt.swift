//
//  NewWishlistItemExt.swift
//  erp
//
//  Created by Akil Rajdho on 20/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import MTBBarcodeScanner
import RealmSwift
import CoreLocation
import NVActivityIndicatorView
extension NewWishlistItemVC {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == productsTableView{
            return productsCopy.count
        }
        return warehousesCopy.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == productsTableView{
            searchProductLabel.text = productsCopy[indexPath.row].name!
            searchProductLabel.textColor = UIColor.black
            self.selectedProductId = Int(productsCopy[indexPath.row].id.value!)
        }
        else if tableView == warehousesTableView{
            warehouseLabel.text = warehousesCopy[indexPath.row].name!
            warehouseLabel.textColor = UIColor.black
            self.selectedWarehouseId = Int(warehousesCopy[indexPath.row].warehouse_id.value!)
            setWishlistDetailWarehouse(warehouse: warehousesCopy[indexPath.row].name!)
            setWishlistDetailWarehouseId(warehouseId: warehousesCopy[indexPath.row].warehouse_id.value!)
        }
        
        whiteView.endEditing(true)
        blackView.endEditing(true)
        handleSlideWindowDismiss()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewTapped == "Product"{
            return createProductCell(indexPath: indexPath)
        }
        return createWarehouseCell(indexPath: indexPath)
    }
    
    func createProductCell(indexPath: IndexPath)->UITableViewCell{
        let cell = productsTableView.dequeueReusableCell(withIdentifier: "productCell", for: indexPath) as! ProductCell
        let product = productsCopy[indexPath.row]
        cell.productNameLabel.text = product.name ?? "-"
        if AuthUser.has(PermissionTo.SEE_PRODUCT_SALE_PRICE){
            cell.salePriceLabel.text = String(product.sale_price.value ?? 0)
        }
        if AuthUser.has(PermissionTo.SEE_PRODUCT_BUY_PRICE){
            cell.buyPirceLabel.text = String(product.buy_price.value ?? 0)
        }
        if AuthUser.has(PermissionTo.SEE_PRODUCT_CODE){
            cell.itemCodeLabel.text = product.code ?? "-"
        }
        cell.idLabel.text = String(product.id.value ?? 0)
        let imageUrl = "\(Urls.BASE_IMAGE_URL)\(product.photo ?? "")"
        cell.productImage.pin_setImage(from: URL(string: imageUrl))
        if cell.productImage.image == nil {
            cell.productImage.image = UIImage(named: "default_product_image")
        }
        styleProductsTable(cell , indexPath)
        return cell
    }
    
    func createWarehouseCell(indexPath: IndexPath)->UITableViewCell{
        let cell = warehousesTableView.dequeueReusableCell(withIdentifier: "searchWarehouseCell", for: indexPath) as! SearchWarehouseCell
        let warehouse = warehousesCopy[indexPath.row]
        cell.warehouseNameLabel.text = warehouse.name ?? ""
        createRoundCorner(cell.containerView, 10)
        cell.colorView.layer.backgroundColor = getRandomColor(indexPath.row).cgColor
        cell.colorView.roundCorners([.topLeft, .bottomLeft], radius: 10)
        return cell
    }
}
