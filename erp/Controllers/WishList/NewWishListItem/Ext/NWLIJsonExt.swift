//
//  File.swift
//  erp
//
//  Created by Akil Rajdho on 20/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
extension NewWishlistItemVC{
    func createWishListItem(){
        
        var wishlistItem = WishListItem()
        wishlistItem.uuid = wishlistDetailUuid
        wishlistItem.date = wishListDetailDate
        wishlistItem.user_id = SavedVariables.getUserId()
        wishlistItem.warehouse_id = selectedWarehouseId
        wishlistItem.product_id = selectedProductId
        wishlistItem.latitude = wishListDetailLatitude
        wishlistItem.longitude = wishListDetailLongitude
        wishlistItem.quantity = wishListDetailQuantity
        
        getApiClient().fetchApiResults(urlString: Urls.CREATE_WISHLIST_DETAIL, parameters: wishlistItem , method: "POST", callback: {(response: GenericResponse<WishList>)->()
            in
            self.performUIUpdatesOnMain {
                self.activityIndicator.stopAnimating()
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                self.presentSuccessPopup()
                self.emptyFields()
                self.getWishLists()
            }
        })
        self.activityIndicator.stopAnimating()
    }
    
    func getWishLists(){
        getApiClient().fetchApiResults(urlString: Urls.GET_USER_WISHLISTS, parameters: EmptyParamStruct(), method: "GET", model:"WishList", callback: {(results: GenericResponse<WishList>)->() in
            self.performUIUpdatesOnMain {
                setAllWishLists(wishLists: results.data)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.WISHLIST_ITEMS_UPDATED), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.WISHLISTS_RETRIEVED), object: nil)
            }
        })
    }
}

