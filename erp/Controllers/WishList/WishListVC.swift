//
//  WishListViewController.swift
//  erp-ios
//
//  Created by apple on 07/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import UIKit
import CoreLocation
class WishListVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    var warehouseId : Int = 0
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var wasFiltering = false
    var refresher = UIRefreshControl()
    var wishlists = getAllWishLists()
    
    let noItemsView = NoItemsFoundView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("The user permissions are:")
        print(SavedVariables.getUserPermissions())
        if SavedVariables.getIsSync(){
           self.performSegue(withIdentifier: "syncData", sender: self)
        }

        initializeComponents()
        initializeAppearanceComponents()
        
    }
    
    @objc func pullToRefreshFunc(){
        wasFiltering = false
        getWishListsFromServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getWishListsFromServer()
        registerToEvents()
        wishlistItemsUpdated()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       unregisterFromEvents()
    }
    
    func initializeAppearanceComponents(){
        self.title = Strings.WISHLISTS_TITLE
        createRoundButton(button: addButton)
    }
    
    func initializeComponents(){
        //requestGpsCordinates()
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "WishListCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "wishListCell")
        tableView.rowHeight = 100
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        refresher = UIRefreshControl()
        refresher.attributedTitle = NSAttributedString(string: Strings.PULL_TO_REFRESH_TITLE)
        refresher.addTarget(self, action: #selector(pullToRefreshFunc), for: UIControlEvents.valueChanged)
        tableView.addSubview(refresher)
        registerToFilterItemsEvent()
    }
    //Create an event in order to make the table reload after data filter
    func registerToFilterItemsEvent(){
        NotificationCenter.default.addObserver(self, selector: #selector(wishListItemsFiltered), name: NSNotification.Name(rawValue: Notifications.WISHLIST_ITEMS_FILTERED), object: nil)
    }
    @objc func wishListItemsFiltered(){
        wasFiltering = true
        wishlists = getAllWishLists()
        self.tableView.reloadData()
    }
    
    @objc func wishlistItemsUpdated(){
        performUIUpdatesOnMain {
            if AuthUser.has(PermissionTo.ACCESS_WISHLISTS){
                self.wishlists = getAllWishLists()
                self.showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: self.wishlists.count)
                self.tableView.reloadData()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //when we say as? Create...VC we can access each object of that class
        if let itemsVC = segue.destination as? WishListItemsVC{
            if let indexPath = tableView.indexPathForSelectedRow{
                let selectedRow = indexPath.row
                itemsVC.indexToShow = selectedRow
                itemsVC.warehouseId = warehouseId
            }
        }
    }
    
    func registerToEvents(){
        NotificationCenter.default.addObserver(self, selector: #selector(wishlistItemsUpdated), name: NSNotification.Name(rawValue: Notifications.WISHLISTS_RETRIEVED), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(resetUI), name: NSNotification.Name(rawValue: Notifications.REQUEST_RETURNED_ERROR), object: nil)
        
          NotificationCenter.default.addObserver(self, selector: #selector(notifyTokkenExpired), name: NSNotification.Name(rawValue: Notifications.TOKEN_EXPIRED), object: nil)
    }
    
    func unregisterFromEvents(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Notifications.WISHLISTS_RETRIEVED), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Notifications.REQUEST_RETURNED_ERROR), object: nil)
        
           NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Notifications.TOKEN_EXPIRED), object: nil)
    }
    
    @objc func resetUI(){
        performUIUpdatesOnMain {
            self.refresher.endRefreshing()
            self.presentApiErrorPopup()
        }
    }
    
    @objc func notifyTokkenExpired(){
        performUIUpdatesOnMain {
            self.refresher.endRefreshing()
            self.presentTokenExpiredPopup()
        }
    }
    
    @IBAction func addButtonTUP(_ sender: Any) {
    }
}
