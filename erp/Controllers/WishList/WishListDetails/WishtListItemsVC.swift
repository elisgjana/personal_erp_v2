//
//  WishListProductsViewController.swift
//  erp-ios
//
//  Created by apple on 07/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import UIKit
import PINRemoteImage

class WishListItemsVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    @IBOutlet weak var addNewItemButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var wishlistItems = [WishListItem]()
    var refresher = UIRefreshControl()
    var warehouseId = 0
    
    let noItemsView = NoItemsFoundView()
    
    var indexToShow : Int = 0
    var indexToDelete = 0
    var uuid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeAppearanceComponents()
        initializeComponents()
    }
    
    func initializeAppearanceComponents(){
        self.title = Strings.WISHLIST_ITEMS_TITLE
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        createRoundButton(button: addNewItemButton)
    }
    
    func initializeComponents(){
        refresher.attributedTitle = NSAttributedString(string: Strings.PULL_TO_REFRESH_TITLE)
        refresher.addTarget(self, action: #selector(getWishlistDetails), for: UIControlEvents.valueChanged)
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "WishlistItemCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "wishlistItemCell")
        tableView.rowHeight = 112
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.addSubview(refresher)
    }
    
    @objc func getWishlistDetails(){
        getWishListItemsFromServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getWishListItemsFromServer()
    }
    
    @IBAction func deleteWishlistDetail(sender: UIButton){
        indexToDelete = sender.tag
        let uuid = getAllWishLists()[indexToShow].details![sender.tag].uuid!
        deleteWishlistItemFromServer(uuid: uuid)
    }
}
