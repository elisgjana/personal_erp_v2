//
//  WLITableViewExt.swift
//  erp
//
//  Created by Akil Rajdho on 21/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit
import Realm
extension WishListItemsVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wishlistItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "wishlistItemCell", for: indexPath) as! WishlistItemCell
        let item = wishlistItems[indexPath.row]
        let productId = item.product_id!
        let product = uiRealm.objects(ProductModel.self).filter("id = \(productId)").first
        
        
        //here I change the image of the product using URL
        let imageUrl = "\(Urls.BASE_IMAGE_URL)\(product?.photo ?? "")"
        cell.productImage.pin_setImage(from: URL(string: imageUrl))
        if cell.productImage.image == nil {
            cell.productImage.image = UIImage(named: "default_product_image") //default product image when no
        }
        
        cell.productNameLabel.text = product?.name ?? ""
        cell.amountLabel.text = "\(String(item.quantity!)) \(product?.measurement_unit ?? "")"
        
        if AuthUser.has(PermissionTo.DELETE_WISHLIST_ITEM){
            cell.deleteButton.addTarget(self, action: #selector(deleteWishlistDetail), for: UIControlEvents.touchUpInside)
            cell.deleteButton.tag = indexPath.row
            cell.deleteView.isHidden = false
        }
        styleWishlistItemTable(cell, indexPath)

        return cell
    }
}
