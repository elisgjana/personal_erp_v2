//
//  WLIJsonExt.swift
//  erp
//
//  Created by Akil Rajdho on 22/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
extension WishListItemsVC{
    func deleteWishlistItemFromServer(uuid : String){
        CustomActivityIndicator.instance.showCustomActivityIndicator(view)
        getApiClient().fetchApiResults(urlString: Urls.DELETE_WISHLISTS_DETAIL + uuid, parameters: EmptyParamStruct(), method: "DELETE", callback: {(response: GenericResponse<String>)->()
            in
            self.performUIUpdatesOnMain {
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                
                if response.data.count > 0 {
                    self.wishlistItems.remove(at: self.indexToDelete)
                    self.tableView.deleteRows(at: [IndexPath(item:self.indexToDelete, section:0)], with: .automatic)
                    self.tableView.reloadData()
                }
                CustomActivityIndicator.instance.hideCustomActivityIndicator()
                self.showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: self.wishlistItems.count)
                self.presentSuccessPopup()
            }
        })
    }
    
    func getWishListItemsFromServer(){
        CustomActivityIndicator.instance.showCustomActivityIndicator(view)
        let url = Urls.GET_WISHLIST_ITEMS + "&warehouse_id=\(warehouseId)"
        getApiClient().fetchApiResults(urlString: url, parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<WishList>)->()
            in
            self.performUIUpdatesOnMain {
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                if response.data.count > 0 {
                    self.wishlistItems.removeAll()
                    
                    for item in response.data[0].details!{
                        self.wishlistItems.append(item)
                    }
                    self.tableView.reloadData()
                }
                CustomActivityIndicator.instance.hideCustomActivityIndicator()
                self.refresher.endRefreshing()
                self.showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: self.wishlistItems.count)
                
            }
        })
    }
}
