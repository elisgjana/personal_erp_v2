//
//  FilterWListExtension.swift
//  erp
//
//  Created by Admin on 11/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
extension FilterWishlistsVC{
    /**
     This method is used to filter Wishlists based on start date, end date and warehouse
     -Parameter: startDate : optional parameter
     -Parameter: endDate : optional parameter
     -Parameter: warehouseId : optional parameter
     **/
    
    func getAllWishListsFromServer(_ startDate: String = "", _ endDate: String = "", _ warehouseId: Int = 0, _ shouldClose : Bool = false){
        var url = Urls.GET_ALL_FILTERED_WISHLISTS
        if startDate != ""{
            url += "&start_date=\(startDate)"
        }
        if endDate != ""{
            url += "&end_date=\(endDate)"
        }
        if warehouseId != 0{
            url += "&warehouse_id=\(warehouseId)"
        }
        
        getApiClient().fetchApiResults(urlString: url, parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<WishList>)->()
            in
            self.performUIUpdatesOnMain {
                self.activityIndicator.stopAnimating()
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                if response.data.count > 0 {
                    setAllWishLists(wishLists:  response.data)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.WISHLIST_ITEMS_FILTERED), object: nil)
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
        })
        self.activityIndicator.stopAnimating()
    }
}
