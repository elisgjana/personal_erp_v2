//
//  FilterWListTableViewExt.swift
//  erp
//
//  Created by Admin on 11/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension FilterWishlistsVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return warehousesCopy.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        warehouseLabel.text = warehousesCopy[indexPath.row].name!
        selectedWarehouseId = Int(warehousesCopy[indexPath.row].warehouse_id.value!)
        handleTapOutsideDismiss(blackView: self.blackView, whiteView: self.whiteView)
        whiteView.endEditing(true)
        blackView.endEditing(true)        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let randomIndex = Int(arc4random_uniform(UInt32(Colors.randomColors.count)))
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchWarehouseCell", for: indexPath) as! SearchWarehouseCell
        if let warehouseName = warehousesCopy[indexPath.row].name{
            cell.warehouseNameLabel.text = warehouseName
            cell.warehouseNameLabel.textColor = UIColor.gray
            if cell.warehouseNameLabel.text == "" {
                cell.warehouseNameLabel.text = "-"
            }
        }else{
            cell.warehouseNameLabel.text = "-"
        }
        createRoundCorner(cell.containerView, 10)
        cell.colorView.layer.backgroundColor = createColorFromHex(hex: Colors.randomColors[randomIndex]).cgColor
        cell.colorView.roundCorners([.topLeft, .bottomLeft], radius: 10)
        
        return cell
    }

}
