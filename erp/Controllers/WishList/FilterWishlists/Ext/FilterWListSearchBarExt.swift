//
//  FilterWListSearchBarExt.swift
//  erp
//
//  Created by Admin on 11/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension FilterWishlistsVC{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        warehousesCopy = warehouses.filter({ warehouse -> Bool in
            guard !searchText.isEmpty else {
                tableView.reloadData()
                return true
            }
            
            return  (warehouse.name?.uppercased().contains(searchText.uppercased()))!
        })
        tableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
}
