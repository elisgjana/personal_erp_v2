//
//  FilterWishlistsVC.swift
//  erp
//
//  Created by Admin on 11/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class FilterWishlistsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    @IBOutlet weak var calendarImage: UIImageView!
    @IBOutlet weak var startDateView: UIView!
    @IBOutlet weak var endDateView: UIView!
    @IBOutlet weak var warehouseView: UIView!
    @IBOutlet weak var warehouseImage: UIImageView!
    @IBOutlet weak var calendar2Image: UIImageView!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var warehouseLabel: UILabel!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    @IBOutlet weak var searchButton: UIButton!

    //I declare this variable in order to define which is tapped the start date or the end  date
    var selectedView = ""
    var blackView = UIView()
    var whiteView = UIView()
    var searchBar = UISearchBar()
    var tableView = UITableView()
    var warehouses = [WarehouseModel]()
    var warehousesCopy = [WarehouseModel]()
    var temp = 0
    var startDate = ""
    var endDate = ""
    var selectedWarehouseId = 0
    let calendarLauncher = CalendarLauncher()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardOnViewTap()
        initializeAppearanceComponents()
        initializeComponents()
        queryWarehouses()
        warehousesCopy = warehouses
        registerCalendarEvent()

        let tapStartDateGesture = UITapGestureRecognizer(target: self, action: #selector(FilterWishlistsVC.startDateTapped(gesture:)))
        startDateView.addGestureRecognizer(tapStartDateGesture)
        startDateView.isUserInteractionEnabled = true
        
        let tapEndDateGesture = UITapGestureRecognizer(target: self, action: #selector(FilterWishlistsVC.endDateTapped(gesture:)))
        endDateView.addGestureRecognizer(tapEndDateGesture)
        endDateView.isUserInteractionEnabled = true
        

        let tapWarehouseGesture = UITapGestureRecognizer(target: self, action: #selector(FilterWishlistsVC.warehouseTapped(gesture:)))
        warehouseView.addGestureRecognizer(tapWarehouseGesture)
        warehouseView.isUserInteractionEnabled = true
    }
    
    func queryWarehouses(){
        for item in uiRealm.objects(WarehouseModel.self){
            warehouses.append(item)
        }
    }
    
    @objc func startDateTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            selectedView = "start_date"
            calendarLauncher.showCalendar()
        }
    }
    
    @objc func endDateTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            selectedView = "end_date"
            calendarLauncher.showCalendar()
        }
    }
    
    @objc func warehouseTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            handleSelectWarehouse()
        }
    }
    
    func initializeAppearanceComponents(){
        self.title = Strings.FILTER_WISH_LISTS_TITLE
        self.navigationController?.navigationBar.tintColor = UIColor.white
        createRoundCorner(startDateView, 10)
        calendarImage.tintColor = UIColor.gray
        createRoundCorner(endDateView, 10)
        calendar2Image.tintColor = UIColor.gray
        createRoundCorner(warehouseView, 10)
        warehouseImage.tintColor = UIColor.gray
        warehouseLabel.textColor = UIColor.gray
        createRoundCorner(searchButton, 10)
        activityIndicator.color = createColorFromHex(hex: Colors.PRIMARY_DARK)
        activityIndicator.type = .ballPulse
        
    }
    func initializeComponents(){
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        let nib = UINib(nibName: "SearchWarehouseCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "searchWarehouseCell")
        tableView.rowHeight = 50
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.backgroundColor = UIColor.groupTableViewBackground
    }
    

    //Here I reload the table depending on the date filters
    @IBAction func searchButtonTUP(_ sender: Any) {
        activityIndicator.startAnimating()
        //here I simulate a delay in order to watch the activity indicator animation due to fast network request
        let when = DispatchTime.now() + 0.75 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Code with delay
            if self.startDateLabel.text! != Strings.FILTER_VC_CHOOSE{
                self.startDate = self.startDateLabel.text!
            }
            if self.endDateLabel.text! != Strings.FILTER_VC_CHOOSE{
                self.endDate = self.endDateLabel.text!
            }
            self.getAllWishListsFromServer(self.startDate, self.endDate, self.selectedWarehouseId, true)
        }
    }
    
    //Here I choose the value of start date and end date and then I make the validation
    @objc private func handleChooseDate(){
        let date = calendarLauncher.getSelectedDate()
        if selectedView == Strings.FILTER_VC_START_DATE{
            startDateLabel.text = date
        }else if selectedView == Strings.FILTER_VC_END_DATE{
            endDateLabel.text = date
        }
        selectedView = ""
        calendarLauncher.handleDismiss()
    }

    override func viewWillDisappear(_ animated: Bool) {
        hideKeyboardOnViewTap()
    }
    
    fileprivate func registerCalendarEvent(){
        calendarLauncher.chooseButton.addTarget(self, action: #selector(handleChooseDate), for: .touchUpInside)
    }
    
}
