//
//  MoreMenuTableViewExtension.swift
//  erp
//
//  Created by Admin on 30/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension MoreMenuVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItemsIndexToName.count + 3
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row >= 0 && indexPath.row < menuItemsIndexToName.count{
        self.performSegue(withIdentifier: menuItemsNameToSegue[menuItemsIndexToName[indexPath.row]!]!, sender: nil)
        }else{
            if indexPath.row == (menuItemsIndexToName.count){
                self.performSegue(withIdentifier: Constants.MENUS["QRCode"]!, sender: nil)
            }else if indexPath.row == (menuItemsIndexToName.count + 1){
                self.performSegue(withIdentifier: Constants.MENUS["Sinkronizo"]!, sender: nil)
            }else{
                self.performSegue(withIdentifier: Constants.MENUS["Dil"]!, sender: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "moreItemCell", for: indexPath) as! MoreItemCell
        if indexPath.row >= 0 && indexPath.row < menuItemsIndexToName.count{
            cell.menuImage.image = UIImage(named: menuItemsNameToSegue[menuItemsIndexToName[indexPath.row]!]!)
            cell.menuNameLabel.text = menuItemsIndexToName[indexPath.row]!
        }else{
            //The last three menus in More Menu need no permissions so they will always be displayed
            if indexPath.row == (menuItemsIndexToName.count){
                cell.menuImage.image = UIImage(named: Constants.MENUS["QRCode"]!)
                cell.menuNameLabel.text = "Qr Code"
            }else if  indexPath.row == (menuItemsIndexToName.count + 1){
                cell.menuImage.image = UIImage(named: Constants.MENUS["Sinkronizo"]!)
                cell.menuNameLabel.text = "Sinkronizo"
            }else{
                cell.menuImage.image = UIImage(named: Constants.MENUS["Dil"]!)
                cell.menuNameLabel.text = "Dil"
            }
        }
        
        styleMoreItemTable(cell , indexPath)
        
        return cell
    }
}
