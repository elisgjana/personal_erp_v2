//
//  MoreMenuVC.swift
//  erp
//
//  Created by Admin on 30/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class MoreMenuVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var permissionIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeComponents()
        initializeAppearanceComponents()
    }
    
    func initializeComponents(){
        self.title = Strings.MENU_TITLE
        checkPermissions()
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "MoreItemCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "moreItemCell")
        navigationController?.navigationBar.tintColor = UIColor.white
        usernameLabel.text = SavedVariables.getUserFirstName() + " \(SavedVariables.getUserLastName())" 
        tableView.rowHeight = 48
    }
    
    func initializeAppearanceComponents(){
        createRound(imageView: profileImage, borderColor: UIColor.white)
    }
    
    func checkPermissions(){
        var i = 0
        if AuthUser.has(PermissionTo.ACCESS_REJECTIONS){
            menuItemsNameToSegue["Skarcot"] = "rejections"
            menuItemsIndexToName[i] = "Skarcot"
            i += 1
        }
        if AuthUser.has(PermissionTo.ACCESS_PRODUCTIONS){
            menuItemsNameToSegue["Prodhimet"] = "production"
            menuItemsIndexToName[i] = "Prodhimet"
            i += 1
        }
        if AuthUser.has(PermissionTo.ACCESS_CLIENTS){
            menuItemsNameToSegue["Klientët"] = "clients"
            menuItemsIndexToName[i] = "Klientët"
            i += 1
        }
        if AuthUser.has(PermissionTo.ACCESS_SUPPLIERS){
            menuItemsNameToSegue["Furnitorët"] = "suppliers"
            menuItemsIndexToName[i] = "Furnitorët"
            i += 1
        }
        
        if AuthUser.has(PermissionTo.ACCESS_REPORTS){
            menuItemsNameToSegue["Raportet"] = "charts"
            menuItemsIndexToName[i] = "Raportet"
            i += 1
        }
    }

}
