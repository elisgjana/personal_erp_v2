//
//  LogoutVC.swift
//  erp
//
//  Created by Admin on 16/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import RealmSwift

class LogoutVC: UIViewController {

    @IBOutlet weak var warningImage: UIImageView!
    @IBOutlet weak var syncButton: UIButton!
    @IBOutlet weak var notSyncView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeComponents()
        registerToEvents()

    }
    
    func initializeComponents(){
        createRoundCorner(notSyncView, 10)
        createRoundCorner(syncButton, 5)
        warningImage.tintColor = createColorFromHex(hex: Colors.INOVACION_RED)
        notSyncView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let notSyncPurchases = uiRealm.objects(PurchaseModel.self).filter("is_sync = false")
        if notSyncPurchases.count != 0{
            notSyncView.isHidden = false
        }else{
            notSyncView.isHidden = true
            //set value of user id to 0 after logout
            UserDefaults.standard.setValue(nil, forKey: "userId")
            sync()
            SavedVariables.setIsSync(isSync: true)
            performSegue(withIdentifier: "logout", sender: nil)
        }
    }
    
    func registerToEvents(){
        NotificationCenter.default.addObserver(self, selector: #selector(performLogout), name: NSNotification.Name(Notifications.PERFORM_LOGOUT), object: nil)
    }
    
    @objc func performLogout(){
                //set value of user id to 0 after logout
                UserDefaults.standard.setValue(nil, forKey: "userId")
                sync()
                SavedVariables.setIsSync(isSync: true)
                performSegue(withIdentifier: "logout", sender: nil)
    }

    @IBAction func syncButtonTUP(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
