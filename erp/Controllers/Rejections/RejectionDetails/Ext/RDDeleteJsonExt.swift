//
//  RDDeleteJsonExt.swift
//  erp
//
//  Created by Admin on 09/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation

extension RejectionDetailsVC{
    func deleteRejectionDetailFromServer(id : Int32){
        CustomActivityIndicator.instance.showCustomActivityIndicator(view)
        getApiClient().fetchApiResults(urlString: Urls.DELETE_REJECTION_DETAIL + "\(id)", parameters: EmptyParamStruct(), method: "DELETE", callback: {(response: GenericResponse<String>)->()
            in
            self.performUIUpdatesOnMain {
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                
                if response.data.count > 0 {
                    self.rejectionDetails.remove(at: self.indexToDelete)
                    self.tableView.deleteRows(at: [IndexPath(item:self.indexToDelete, section:0)], with: .automatic)
                    self.tableView.reloadData()
                }
                CustomActivityIndicator.instance.hideCustomActivityIndicator()
                self.showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: self.rejectionDetails.count)
                self.presentSuccessPopup()
            }
        })
    }
}

