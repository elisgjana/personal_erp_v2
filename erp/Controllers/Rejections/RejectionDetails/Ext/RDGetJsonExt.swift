//
//  RDGetJsonExt.swift
//  erp
//
//  Created by Admin on 09/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation

extension RejectionDetailsVC{
    func getRejectionDetailsFromServer(){
        CustomActivityIndicator.instance.showCustomActivityIndicator(view)
        getApiClient().fetchApiResults(urlString: Urls.GET_REJECTION_DETAILS + "warehouse_id=\(warehouseId)", parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<Rejection>)->()
            in
            self.performUIUpdatesOnMain {
                
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                
                if response.data.count > 0 {
                    self.rejectionDetails.removeAll()
                    for item in response.data {
                        if item.details?.count != 0{
                            for detail in item.details!{
                                self.rejectionDetails.append(detail)
                            }
                        }
                    }
                    self.tableView.reloadData()
                }
                CustomActivityIndicator.instance.hideCustomActivityIndicator()
                self.refresher.endRefreshing()
                self.showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: self.rejectionDetails.count)
            }
        })
    }
}

