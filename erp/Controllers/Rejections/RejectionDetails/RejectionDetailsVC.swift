//
//  RejectionDetailsVC.swift
//  erp
//
//  Created by Admin on 09/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import PINRemoteImage

class RejectionDetailsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var rejectionDetails = [RejectionDetail]()
    var refresher = UIRefreshControl()
    var warehouseId:Int = 0
    
    var indexToShow : Int = 0
    var indexToDelete = 0
    var uuid = ""
    let noItemsView = NoItemsFoundView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeAppearanceComponents()
        initializeComponents()
    }
    
    func initializeAppearanceComponents(){
        self.title = Strings.REJECTION_DETAILS_TITLE
        self.navigationController?.navigationBar.tintColor = UIColor.white;
    }
    
    func initializeComponents(){
        refresher.attributedTitle = NSAttributedString(string: Strings.PULL_TO_REFRESH_TITLE)
        refresher.addTarget(self, action: #selector(getRejectionDetails), for: UIControlEvents.valueChanged)
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "RejectionDetailCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "rejectionDetailCell")
        tableView.rowHeight = 112
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.addSubview(refresher)
        showNoItemLogo(insetionView: tableView, noItemsView: noItemsView, itemsNumber: rejectionDetails.count)
    }
    
    @objc func getRejectionDetails(){
       getRejectionDetailsFromServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getRejectionDetailsFromServer()
    }
    
    @IBAction func deleteRejectionDetail(sender: UIButton){
        indexToDelete = sender.tag
        let id = getAllRejections()[indexToShow].details![sender.tag].id!
        deleteRejectionDetailFromServer(id: id)
    }
}
