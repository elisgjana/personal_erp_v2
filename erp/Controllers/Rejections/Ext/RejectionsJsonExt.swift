//
//  RejectionsJsonExt.swift
//  erp
//
//  Created by Admin on 01/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension RejectionsVC{
    func getRejectionsFromServer(){
            CustomActivityIndicator.instance.showCustomActivityIndicator(view)
            getApiClient().fetchApiResults(urlString: Urls.GET_ALL_REJECTIONS_URL, parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<Rejection>)->()
                in
                self.performUIUpdatesOnMain {
                    if response.status != 0 {
                        self.presentApiErrorPopup(statusCode: response.status!)
                        return
                    }
                    if response.data.count > 0 {
                        setAllRejections(rejections: response.data)
                        self.tableView.reloadData()
                        self.rejections = getAllRejections()
                        self.tableView.reloadData()
                    }
                    self.apiCallFinished()
                }
            })
    }
    
    @objc func apiCallFinished(){
        self.performUIUpdatesOnMain {
            CustomActivityIndicator.instance.hideCustomActivityIndicator()
            self.showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: self.rejections.count)
            self.refresher.endRefreshing()
        }
    }
}
