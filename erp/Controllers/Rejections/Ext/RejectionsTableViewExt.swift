//
//  RejectionsTableViewExt.swift
//  erp
//
//  Created by Admin on 01/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension RejectionsVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rejections.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        warehouseId = Int(getAllRejections()[indexPath.row].warehouse_id!) 
        performSegue(withIdentifier: "rejectionDetails", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var warehouseLabel = ""
        let cell = tableView.dequeueReusableCell(withIdentifier: "rejectionCell", for: indexPath) as! RejectionCell
        let currentRejection = rejections[indexPath.row]
        
        let date = currentRejection.date!
        cell.dateLabel.text = String(describing: date)
        
        if let startWarehouse = currentRejection.warehouse_name{
            warehouseLabel = "Nga " + startWarehouse
        }
        
        if let endWarehouse = currentRejection.destination_warehouse_name{
            warehouseLabel += " Tek " + endWarehouse
        }
        
        cell.warehousesLabel.text = warehouseLabel
        styleRejectionTable(cell, indexPath)
        
        return cell
    }
}

