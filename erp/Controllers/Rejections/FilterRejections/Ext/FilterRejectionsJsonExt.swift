//
//  FilterRejectionsJsonExt.swift
//  erp
//
//  Created by Admin on 04/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension FilterRejectionsVC{
    /**
     This method is used to filter Rejections based on start date, end date and warehouses
     -Parameter: startDate : optional parameter
     -Parameter: endDate : optional parameter
     -Parameter: warehouse : optional parameter
     **/
    func getAllRejectionsFromServer(_ startDate: String = "", _ endDate: String = "", _ warehouseId: Int = 0, _ shouldClose : Bool = false){
        var url = Urls.GET_ALL_FILTERED_REJECTIONS
        if startDate != ""{
            url += "&start_date=\(startDate)"
        }
        if endDate != ""{
            url += "&end_date=\(endDate)"
        }
        if warehouseId != 0{
            url += "&warehouse_id=\(warehouseId)"
        }
        
        getApiClient().fetchApiResults(urlString: url, parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<Rejection>)->()
            in
            self.performUIUpdatesOnMain {
                self.activityIndicator.stopAnimating()
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                if response.data.count > 0 {
                    setAllRejections(rejections: response.data)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.REJECTION_ITEMS_UPDATED), object: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        })
        self.activityIndicator.stopAnimating()
    }
}
