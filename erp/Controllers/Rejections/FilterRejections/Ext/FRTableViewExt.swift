//
//  FRTableViewExt.swift
//  erp
//
//  Created by Admin on 04/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension FilterRejectionsVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return warehousesCopy.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        warehouseLabel.text = warehousesCopy[indexPath.row].name!
        selectedWarehouseId = Int(warehousesCopy[indexPath.row].warehouse_id.value!)
        whiteView.endEditing(true)
        blackView.endEditing(true)
        handleTapOutsideDismiss(blackView: self.blackView, whiteView: self.whiteView)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchWarehouseCell", for: indexPath) as! SearchWarehouseCell
        let currentWarehouse = warehousesCopy[indexPath.row]
        let warehouseName = currentWarehouse.name ?? ""
        cell.warehouseNameLabel.text = warehouseName
        cell.warehouseNameLabel.textColor = UIColor.gray
        styleSearchWarehousesTable(cell, indexPath)
        return cell
    }
}
