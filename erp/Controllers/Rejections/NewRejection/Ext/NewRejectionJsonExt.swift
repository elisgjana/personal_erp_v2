//
//  NewRejectionJsonExt.swift
//  erp
//
//  Created by Admin on 02/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension NewRejectionVC{
    func createNewRejection(){
        var rejectionDetail = RejectionDetail()
        rejectionDetail.user_id = Int32(SavedVariables.getUserId())
        rejectionDetail.warehouse_id = selectedWarehouseId
        rejectionDetail.product_id = Int32(selectedProductId)
        rejectionDetail.quantity = productQuantity
        
        activityIndicator.startAnimating()
        getApiClient().fetchApiResults(urlString: Urls.CREATE_REJECTION, parameters: rejectionDetail , method: "POST", callback: {(response: GenericResponse<Rejection>)->()
            in
            self.performUIUpdatesOnMain {
                self.activityIndicator.stopAnimating()
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                self.presentSuccessPopup()
                self.emptyFields()
            }
        })
        self.activityIndicator.stopAnimating()
    }
}
