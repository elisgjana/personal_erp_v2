//
//  NRSearchBarExt.swift
//  erp
//
//  Created by Admin on 02/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension NewRejectionVC{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar == productsSearchBar{
            let searchString = searchText.uppercased()
            productsCopy = products.filter({ product -> Bool in
                guard !searchString.isEmpty else {
                    productsTableView.reloadData()
                    return true
                }
                let productName = product.name!.uppercased()
                let productCode = product.code?.uppercased() ?? ""
                return  (productName.contains(searchString) || productCode.contains(searchString))
            })
            productsTableView.reloadData()
        }
        else{
            warehousesCopy = warehouses.filter({ warehouse -> Bool in
                guard !searchText.isEmpty else {
                    warehousesTableView.reloadData()
                    return true
                }
                return  (warehouse.name?.uppercased().contains(searchText.uppercased()))!
            })
            warehousesTableView.reloadData()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar == productsSearchBar{
            self.productsSearchBar.endEditing(true)
            productsSearchBar.resignFirstResponder()
        }else{
            self.warehousesSearchBar.endEditing(true)
            warehousesSearchBar.resignFirstResponder()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.productsSearchBar.endEditing(true)
        productsSearchBar.showsCancelButton = false
        productsSearchBar.resignFirstResponder()
        self.warehousesSearchBar.endEditing(true)
        warehousesSearchBar.showsCancelButton = false
        warehousesSearchBar.resignFirstResponder()
    }
}
