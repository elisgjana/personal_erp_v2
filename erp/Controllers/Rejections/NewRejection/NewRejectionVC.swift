//
//  NewRejectionVC.swift
//  erp
//
//  Created by Admin on 02/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import MTBBarcodeScanner
import RealmSwift
import CoreLocation
import NVActivityIndicatorView

class NewRejectionVC: UIViewController , UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    @IBOutlet weak var searchImage: UIImageView!
    @IBOutlet weak var scaleImage: UIImageView!
    @IBOutlet weak var warehouseLabel: UILabel!
    @IBOutlet weak var searchWarehouseView: UIView!
    @IBOutlet weak var selectDateView: UIView!
    @IBOutlet weak var warehouseImage: UIImageView!
    @IBOutlet var barcodeView: UIView!
    @IBOutlet weak var searchProductView: UIView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var roundBackgroundView: UIView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var calendarImage: UIImageView!
    @IBOutlet weak var searchProductLabel: UILabel!
    
    var blackView = UIView()
    var whiteView = UIView()
    var productsSearchBar = UISearchBar()
    var warehousesSearchBar = UISearchBar()
    var productsTableView = UITableView()
    var warehousesTableView = UITableView()
    var viewTapped = ""
    var products = [ProductModel]()
    var productsCopy = [ProductModel]()
    var warehouses = [WarehouseModel]()
    var warehousesCopy = [WarehouseModel]()
    var temp = 0
    var scanner: MTBBarcodeScanner?
    var wishlistDetailUuid = ""
    var wishListDetailDate = ""
    var selectedDate = ""
    var selectedWarehouseId:Int32 = 0
    var selectedProductId = 0
    var productQuantity = 0.0
    var wishListDetailLatitude = currentLatitude
    var wishListDetailLongitude = currentLongitude
    var defaultWarehouseName = ""
    let calendarLauncher = CalendarLauncher()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeAppearanceComponents()
        initializeComponents()
        queryProducts()
        queryWarehouses()
        productsCopy = products
        warehousesCopy = warehouses
        scanner = MTBBarcodeScanner(previewView: cameraView)
        initializeDateSelection()
        initializeProductSelection()
        initializeWarehouseSelection()
        registerCalendarEvent()
    }
    
    func queryProducts(){
        for each in uiRealm.objects(ProductModel.self){
            self.products.append(each)
        }
    }
    
    func queryWarehouses(){
        for each in uiRealm.objects(WarehouseModel.self){
            self.warehouses.append(each)
        }
    }
    
    func initializeDateSelection(){
        
        let tapDateGesture = UITapGestureRecognizer(target: self, action: #selector(NewRejectionVC.dateTapped(gesture:)))
        selectDateView.addGestureRecognizer(tapDateGesture)
        selectDateView.isUserInteractionEnabled = true
    }
    
    func initializeProductSelection(){
        //here I make the choose product slide from bottom
        let tapProduct = UITapGestureRecognizer(target: self, action: #selector(NewRejectionVC.productTapped(gesture:)))
        
        //here I add the barcodescanner button on nav bar
        let barcodeButton = UIBarButtonItem(image: UIImage(named:"barcode"), style: .plain, target: self, action: #selector(NewRejectionVC.barcodeScannerTUP(_:)))
        self.navigationItem.rightBarButtonItem = barcodeButton
        
        //add it to view
        searchProductView.addGestureRecognizer(tapProduct)
        // make sure View can be interacted with by user
        searchProductView.isUserInteractionEnabled = true
    }
    
    func initializeAppearanceComponents(){
        self.title = Strings.NEW_REJECTION_TITLE
        createRoundCorner(barcodeView, 10)
        createRoundCorner(roundBackgroundView, 10)
        titleView.roundCorners([.topRight, .topLeft], radius: 10)
        searchImage.tintColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        scaleImage.tintColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        calendarImage.tintColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        warehouseImage.tintColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        createRoundCorner(saveButton, 10)
        activityIndicator.color = createColorFromHex(hex: Colors.PRIMARY_DARK)
        activityIndicator.type = .ballSpinFadeLoader
        amountField.keyboardType = .decimalPad
    }
    
    func initializeWarehouseSelection(){
        //here I make the choose warehouse slide from bottom
        let tapWarehouse = UITapGestureRecognizer(target: self, action: #selector(NewRejectionVC.warehouseTapped(gesture:)))
        //add it to view
        searchWarehouseView.addGestureRecognizer(tapWarehouse)
        // make sure View can be interacted with by user
        searchWarehouseView.isUserInteractionEnabled = true
    }
    
    @objc func dateTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            calendarLauncher.showCalendar()
        }
    }
    
    @objc func productTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            viewTapped = "Product"
            handleSelectItem()
        }
    }
    
    @objc func warehouseTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            viewTapped = "Warehouse"
            handleSelectItem()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.scanner?.stopScanning()
    }
    
    @objc func barcodeScannerTUP(_ sender:UIBarButtonItem!){
        barcodeView.isHidden = false
        view.addSubview(barcodeView)
        QRGenerator()
        barcodeView.center = CGPoint(x: view.frame.width / 2, y: view.frame.height / 2 - 30)
    }
    
    func initializeComponents(){
        productsTableView.delegate = self
        productsTableView.dataSource = self
        warehousesTableView.delegate = self
        warehousesTableView.dataSource = self
        productsSearchBar.delegate = self
        warehousesSearchBar.delegate = self
        let productNib = UINib(nibName: "ProductCell", bundle: nil)
        let warehouseNib = UINib(nibName: "SearchWarehouseCell", bundle: nil)
        productsTableView.register(productNib, forCellReuseIdentifier: "productCell")
        warehousesTableView.register(warehouseNib, forCellReuseIdentifier: "searchWarehouseCell")
        productsTableView.rowHeight = 151
        warehousesTableView.rowHeight = 50
        self.productsTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.warehousesTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.productsTableView.backgroundColor = UIColor.groupTableViewBackground
        self.warehousesTableView.backgroundColor = UIColor.groupTableViewBackground
        barcodeView.isHidden = true
        hideKeyboardOnViewTap()
        let defaultWarehouse = uiRealm.objects(WarehouseModel.self).filter("warehouse_id = \(SavedVariables.getDefaultWarehouseId())").first
        if let warehouseName = defaultWarehouse?.name as? String{
            warehouseLabel.text = warehouseName
            defaultWarehouseName = warehouseLabel.text!
            selectedWarehouseId = (defaultWarehouse?.warehouse_id.value)!
        }else{
            warehouseLabel.text = Strings.CHOOSE
            defaultWarehouseName = Strings.CHOOSE
        }
    }
    
    @IBAction func saveButtonTUP(_ sender: Any) {
        if warehouseLabel.text == "" {
            presentTopNotification(title: Strings.WARNING_TITLE, subtitle: Strings.WAREHOUSE_NOT_FOUND, image: "warning", color: Colors.INOVACION_YELLOW, duration: Constants.TOP_NOTIFICATION_DURATION)
            return
        }
        wishlistDetailUuid = NSUUID().uuidString
        wishListDetailDate = Device.getCurrentDate()
        if let quantity = Double(amountField.text!) {
            productQuantity = quantity
        }
        if (searchProductLabel.text! != Strings.PRODUCT_SEARCHBAR_PLACEHOLDER && amountField.text! != ""){
            activityIndicator.startAnimating()
            //here I simulate a delay in order to watch the activity indicator animation due to fast network request
            let when = DispatchTime.now() + 0.75 // change 0.75 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                // Code with delay
                self.createNewRejection()
            }
        }
    }

    func emptyFields(){
        let defaultWarehouse = uiRealm.objects(WarehouseModel.self).filter("warehouse_id = \(SavedVariables.getDefaultWarehouseId())").first
        if let warehouseName = defaultWarehouse?.name as? String{
            warehouseLabel.text = warehouseName
            defaultWarehouseName = warehouseLabel.text!
        }else{
            warehouseLabel.text = Strings.CHOOSE
        }
        warehouseLabel.text = defaultWarehouseName
        warehouseLabel.textColor = UIColor.gray
        searchProductLabel.text = Strings.PRODUCT_SEARCHBAR_PLACEHOLDER
        searchProductLabel.textColor = UIColor.gray
        amountField.text = ""
        amountField.textColor = UIColor.gray
    }
    
    @objc fileprivate func handleChooseDate(){
        let date = calendarLauncher.getSelectedDate()
        dateLabel.text = date
        selectedDate = date
        calendarLauncher.handleDismiss()
    }
    
    fileprivate func registerCalendarEvent(){
        calendarLauncher.chooseButton.addTarget(self, action: #selector(handleChooseDate), for: .touchUpInside)
    }

}
    

