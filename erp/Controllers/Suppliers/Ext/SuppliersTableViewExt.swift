//
//  SuppliersTableViewExt.swift
//  erp
//
//  Created by Admin on 15/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension SuppliersVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suppliersCopy.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "clientCell", for: indexPath) as! ClientCell
        
        let currentSupplier = suppliersCopy[indexPath.row]
        
        var subjectName = currentSupplier.name ?? "-"
        if subjectName == ""{
            subjectName = "-"
        }
        cell.subjectNameLabel.text = subjectName
        
        var contactName = currentSupplier.contact_name ?? "-"
        if contactName == ""{
            contactName = "-"
        }
        cell.contactNameLabel.text = contactName
        
        var nuis = currentSupplier.nuis ?? "-"
        if nuis == ""{
            nuis = "-"
        }
        cell.nuisLabel.text = nuis
        
        var city = currentSupplier.city ?? "="
        if city == ""{
            city = "-"
        }
        cell.cityLabel.text = city
        
        var tel = currentSupplier.mobile_phone ?? "-"
        if tel == ""{
            tel = "-"
        }
        cell.telLabel.text = tel
        
        styleClientDetailTable(cell, indexPath)
        
        cell.callButton.addTarget(self, action: #selector(callFunc), for: UIControlEvents.touchUpInside)
        cell.callButton.tag = indexPath.row
        cell.directionButton.addTarget(self, action: #selector(getDirections), for: UIControlEvents.touchUpInside)
        cell.directionButton.tag = indexPath.row
        
        return cell
    }
}
