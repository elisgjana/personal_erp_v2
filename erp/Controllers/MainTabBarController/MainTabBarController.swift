//
//  MainTabBarController.swift
//  erp
//
//  Created by Akil Rajdho on 19/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import Foundation

class MainTabBarController: UITabBarController {
    
    var accessableViewControllers = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewControllers?.removeAll()
        
        //here we add accessable ViewControllers in TabBar depending on User Permissions
        if AuthUser.has(PermissionTo.ACCESS_WISHLISTS){
            addViewControllerToTabBar(storyboard: "Wishlists", identifier: "wishlistNav")
        }
        
        if AuthUser.has(PermissionTo.ACCESS_PRODUCTS){
            addViewControllerToTabBar(storyboard: "Products", identifier: "productNav")
        }
        
        if AuthUser.has(PermissionTo.ACCESS_ORDERS){
            addViewControllerToTabBar(storyboard: "Orders", identifier: "orderNav")
        }
        
        if AuthUser.has(PermissionTo.ACCESS_PURCHASES){
            addViewControllerToTabBar(storyboard: "Purchases", identifier: "purchaseNav")
        }
        addViewControllerToTabBar(storyboard: "Main", identifier: "moreNav")
    
        self.viewControllers = accessableViewControllers
    }
    
    func addViewControllerToTabBar(storyboard: String, identifier: String){
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        let currentViewController = storyboard.instantiateViewController(withIdentifier: identifier)
        accessableViewControllers.append(currentViewController)
    }

}
