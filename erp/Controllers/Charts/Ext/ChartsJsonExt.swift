//
//  ChartsJsonExt.swift
//  erp
//
//  Created by Admin on 23/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension ChartsVC{
    func getBarChartDataFromServer(){
            getApiClient().fetchApiResults(urlString: Urls.GET_MRPRODUCT_URL, parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<MRProduct>)->()
                in
                self.performUIUpdatesOnMain {
                    self.activityIndicator.startAnimating()
                    if response.status != 0 {
                        self.presentApiErrorPopup(statusCode: response.status!)
                        self.activityIndicator.stopAnimating()
                        return
                    }
                    if response.data.count > 0 {
                        self.activityIndicator.stopAnimating()
                        self.monthValues = response.data[0].chart!
                        self.productName.text = response.data[0].product_name!
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.FETCH_BARCHART_DATA), object: nil)
                    }
                }
            })
    }
}
