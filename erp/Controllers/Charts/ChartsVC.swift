//
//  ChartsVC.swift
//  erp
//
//  Created by Admin on 21/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import Charts

class ChartsVC: UIViewController {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var barChart: BarChartView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var monthValues = [Double]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeComponents()
        registerToEvents() 
        getBarChartDataFromServer()
        
    }
    
    func initializeComponents(){
        cardView.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        cardView.layer.shadowRadius = 1
        cardView.layer.shadowOpacity = 0.1
        createRoundCorner(cardView, 10)
        
        self.title = Strings.REPORTS_TITLE
        
    }
    
    func registerToEvents(){
        NotificationCenter.default.addObserver(self, selector: #selector(initBarsChart), name: NSNotification.Name(rawValue: Notifications.FETCH_BARCHART_DATA), object: nil)
    }
    
    @objc func initBarsChart(){
        
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 1...monthValues.count{
            dataEntries.append(BarChartDataEntry(x: Double(i), y: monthValues[i-1]))
        }
        let dataSet = BarChartDataSet(values: dataEntries, label: Strings.MONTHLY_STATISCTICS)
        let data = BarChartData(dataSets: [dataSet])
        
        barChart.data = data
        barChart.chartDescription?.text = ""
        barChart.animate(xAxisDuration: 2)
        
        // Color
        dataSet.colors = ChartColorTemplates.material()
        
        // Refresh chart with new data
        barChart.notifyDataSetChanged()
        
    }
    
}
