//
//  ProductsViewController.swift
//  erp-ios
//
//  Created by apple on 23/01/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import UIKit

class ProductsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var refresher = UIRefreshControl()
    var temp = 0
    var products = [ProductModel]()
    var productsCopy = [ProductModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardOnViewTap()
        initializeComponents()
        queryProducts()
        productsCopy = products
    }
    func queryProducts(){
        for each in uiRealm.objects(ProductModel.self){
            self.products.append(each)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        productsCopy = products
    }
    
    @objc func pullToRefreshFunc(){
        getProductsFromServer()
    }
    
    func initializeComponents(){
        self.title = Strings.PRODUCTS_TITLE
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        let nib = UINib(nibName: "ProductCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "productCell")
        navigationController?.navigationBar.tintColor = UIColor.white
        tableView.rowHeight = 151
        refresher = UIRefreshControl()
        refresher.attributedTitle = NSAttributedString(string: Strings.PULL_TO_REFRESH_TITLE)
        refresher.addTarget(self, action: #selector(pullToRefreshFunc), for: UIControlEvents.valueChanged)
        tableView.addSubview(refresher)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        productsCopy = products.filter({ product -> Bool in
            guard !searchText.isEmpty else {
                tableView.reloadData()
                return true
            }
            let productCode = product.code ?? ""
            return (product.name?.uppercased().contains(searchText.uppercased()))! || (productCode.contains(searchText.uppercased()))
        })
        tableView.reloadData()
    }

}
