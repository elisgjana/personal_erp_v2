//
//  TableView.swift
//  erp
//
//  Created by Akil Rajdho on 16/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit
import PINRemoteImage

extension ProductsVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productsCopy.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "productCell", for: indexPath) as! ProductCell
        let product = productsCopy[indexPath.row]
        cell.productNameLabel.text = product.name ?? "-"
        cell.descriptionLabel.text = product.external_bar_code ?? ""
        
        if AuthUser.has(PermissionTo.SEE_PRODUCT_SALE_PRICE){
            cell.salePriceLabel.text = String(product.sale_price.value ?? 0)
        }
        if AuthUser.has(PermissionTo.SEE_PRODUCT_BUY_PRICE){
            cell.buyPirceLabel.text = String(product.buy_price.value ?? 0)
        }
        if AuthUser.has(PermissionTo.SEE_PRODUCT_CODE){
            cell.itemCodeLabel.text = product.code ?? "-"
        }
        cell.idLabel.text = String(product.id.value ?? 0)
        let imageUrl = "\(Urls.BASE_IMAGE_URL)\(product.photo ?? "")"
        cell.productImage.pin_setImage(from: URL(string: imageUrl))
        if cell.productImage.image == nil {
            cell.productImage.image = UIImage(named: "default_product_image")
        }
        
        styleProductsTable(cell , indexPath)
        return cell
    }
    
}
