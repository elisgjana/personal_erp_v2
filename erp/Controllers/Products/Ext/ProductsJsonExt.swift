//
//  GetProductsJsonExtensionViewController.swift
//  erp-ios
//
//  Created by apple on 27/01/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation
extension ProductsVC{
    func getProductsFromServer(){
        getApiClient().fetchApiResults(urlString: Urls.GET_ALL_PRODUCTS, parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<Product>)->()
            in
            self.performUIUpdatesOnMain {
                self.activityIndicator.stopAnimating()
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                if response.data.count > 0 {
                    setAllProducts(products: response.data)
                    storeProductsInRealm()
                }
                self.refresher.endRefreshing()
            }
        })
    }
}
