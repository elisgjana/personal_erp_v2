//
//  NPPaymentVC.swift
//  erp
//
//  Created by Admin on 28/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import BEMCheckBox

class NPPaymentVC: UIViewController, BEMCheckBoxDelegate{
    
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    @IBOutlet var popupView: UIView!
    @IBOutlet weak var darkView: UIView!
    @IBOutlet weak var payedAmountView: UIView!
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var amountPayedSuperviewField: UITextField!
    @IBOutlet weak var calculationView: UIView!
    @IBOutlet weak var totalInvoicePriceField: UITextField!
    @IBOutlet weak var amountPayedField: UITextField!
    @IBOutlet weak var payAllBox: BEMCheckBox!
    @IBOutlet weak var paymentMethodView: UIView!
    @IBOutlet weak var differenceToBePayedField: UITextField!
    var amountPayed = 0.00
    var payingMethod = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeAppearanceComponents()
        initializeComponents()
        let tapAmountPayed = UITapGestureRecognizer(target: self, action: #selector(NPPaymentVC.amountTapped(gesture:)))
        //add it to view
        payedAmountView.addGestureRecognizer(tapAmountPayed)
        // make sure View can be interacted with by user
        payedAmountView.isUserInteractionEnabled = true
        registerToPurchasePaymentUpdateEvent()
        
        
    }
    
    func registerToPurchasePaymentUpdateEvent(){
        NotificationCenter.default.addObserver(self, selector: #selector(purchasePaymentUpdated), name: NSNotification.Name(rawValue: "purchasePaymentUpdated"), object: nil)
    }
    
    @objc func purchasePaymentUpdated(){
        totalInvoicePriceField.text = String(getPurchaseInvoiceTotal())
        differenceToBePayedField.text = String(getPurchaseInvoiceTotal() * -1)
        differenceToBePayedField.textColor = UIColor.red
    }
    
    @objc func amountTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            view.addSubview(popupView)
            popupView.center = CGPoint(x: view.frame.width / 2, y: view.frame.height / 2 - 30)
            darkView.isHidden = false
        }
        
    }
    
    func initializeAppearanceComponents(){
        createRoundCorner(paymentMethodView, 10)
        createRoundCorner(calculationView, 10)
        createRoundCorner(payButton, 10)
        createRoundCorner(popupView, 10)
        activityIndicator.color = createColorFromHex(hex: Colors.PRIMARY_DARK)
        activityIndicator.type = .ballSpinFadeLoader
    }
    
    func initializeComponents(){
        payAllBox.delegate = self
        darkView.isHidden = true
        
    }
    
    @IBAction func cashPaymentTUP(_ sender: Any) {
        payingMethod = "cash"
    }
    
    @IBAction func bankPaymentTUP(_ sender: Any) {
        payingMethod = "bank"
    }
    
    func didTap(_ checkBox: BEMCheckBox) {
        if (payAllBox.on){
            amountPayed = getPurchaseInvoiceTotal()
            amountPayedSuperviewField.text = String(amountPayed)
            totalInvoicePriceField.text = String(getPurchaseInvoiceTotal())
            differenceToBePayedField.text = String(amountPayed - getPurchaseInvoiceTotal())
            if let difference = Double(differenceToBePayedField.text!){
                if difference < 0{
                    differenceToBePayedField.textColor = UIColor.red
                }else{
                    differenceToBePayedField.textColor = createColorFromHex(hex: Colors.PRIMARY_LIGHT)
                }
            }
        }else{
            amountPayed = 0.00
            amountPayedSuperviewField.text = String(amountPayed)
            totalInvoicePriceField.text = String(getPurchaseInvoiceTotal())
            differenceToBePayedField.text = String(amountPayed - getPurchaseInvoiceTotal())
            if let difference = Double(differenceToBePayedField.text!){
                if difference < 0{
                    differenceToBePayedField.textColor = UIColor.red
                }else{
                    differenceToBePayedField.textColor = createColorFromHex(hex: Colors.PRIMARY_LIGHT)
                }
            }
        }
    }
    
    @IBAction func okButtonTUP(_ sender: Any) {
        if(amountPayedField.text != ""){
            amountPayed = Double(amountPayedField.text!)!
            amountPayedSuperviewField.text = amountPayedField.text
            differenceToBePayedField.text = String(amountPayed - getPurchaseInvoiceTotal())
            if let difference = Double(differenceToBePayedField.text!){
                if difference < 0{
                    differenceToBePayedField.textColor = UIColor.red
                }else{
                    differenceToBePayedField.textColor = createColorFromHex(hex: Colors.PRIMARY_LIGHT)
                }
            }
        }else{
            amountPayedSuperviewField.text = "0.00"
        }
        darkView.isHidden = true
        popupView.removeFromSuperview()
        
    }
    
    @IBAction func payButtonTUP(_ sender: Any) {
        if !validateInput(){
            return
        }
        
        uuid = NSUUID().uuidString
        payment_method = payingMethod
        invoice_value = getPurchaseInvoiceTotal()
        let amountPayed = Double(amountPayedSuperviewField.text!) ?? 0.0
        payment_value = amountPayed
        if amountPayed == 0{
            status_id = 11
        }else{
            if amountPayed == invoice_value{
                status_id = 6
            }else{
                status_id = 10
            }
        }
        payments.append(PurchasePayment(payment_method: payingMethod, amount: amountPayed, date: Device.getCurrentDate(), user_id: Int32(SavedVariables.getUserId())))
        
        //here I simulate a delay in order to watch the activity indicator animation due to fast network request
        let when = DispatchTime.now() + 0.75 // change 0.75 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Code with delay
            self.createNewPurchase()
            
            if purchaseImage != ""{
                self.createNewPurchaseImage() 
            }
        }
    }
    
    func validateInput()->Bool{
        
        var valid = true
        
        if payingMethod == ""{
            valid = false
        }
        
        if getPurchaseInvoiceTotal() == 0{
            valid = false
        }
        
        if !infoIsConfirmed{
            valid = false
        }
        
        if !valid{
            displayAlert(title: Strings.MISSING_DATA, message: Strings.PURCHASE_PAYMENT_WARNING)
        }
        
        return valid
    }
}
