//
//  NPCreatePurchaseJsonExtension.swift
//  erp
//
//  Created by Admin on 30/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation

extension NPPaymentVC{
    func createNewPurchase(){
        var newPurchase = Purchase()
        newPurchase.supplier_uuid = supplier_uuid
        newPurchase.supplier_name = supplier_name_search
        newPurchase.date = date
        newPurchase.user_id = Int32(SavedVariables.getUserId())
        newPurchase.uuid = uuid
        newPurchase.value = invoice_value
        newPurchase.warehouse_id = Int32(warehouse_id)
        newPurchase.invoice_no = invoice_no
        newPurchase.serial_no = serial_no
        newPurchase.discount = discount
        newPurchase.delivered = delivered
        newPurchase.latitude = latitude
        newPurchase.longitude = longitude
        newPurchase.debt = 0.0
        newPurchase.notes = notes
        newPurchase.payment_method = payment_method
        newPurchase.payment_value = payment_value
        newPurchase.status_id = status_id
        
        var allDetails : [Detail] = []
        for element in purchaseDetails{
            var purchaseDetail = Detail()
            purchaseDetail.product_id = element.product_id
            purchaseDetail.product_name = element.product_name
            purchaseDetail.amount = element.amount
            purchaseDetail.product_price = element.product_price
            purchaseDetail.discount = element.discount
            purchaseDetail.total = element.value
            allDetails.append(purchaseDetail)
        }
        
        var allPayments : [Payment] = []
        for element in payments{
            var purchasePayment = Payment()
            purchasePayment.payment_method = element.payment_method
            purchasePayment.amount = element.amount
            purchasePayment.date = element.date
            purchasePayment.user_id = Int32(SavedVariables.getUserId())
            allPayments.append(purchasePayment)
        }
        newPurchase.details = allDetails
        newPurchase.payments = allPayments
        storeIndividualPurchaseToRealm(purchase: newPurchase)
        self.presentTopNotification(title: Strings.SUCCESS_TITLE, subtitle: Strings.CREATE_PURCHASE_SUCCESSFULLY, image: "success", color: Colors.INOVACION_GREEN, duration: Constants.TOP_NOTIFICATION_DURATION)
        resetData()
                
    }
    
    func resetData(){
        supplier_uuid = ""
        infoIsConfirmed = false
        date = Device.getCurrentDate()
        supplier_name_search = ""
        selected_product_id = 0
        invoice_value = 0.0
        warehouse_id = 0
        invoice_no = ""
        serial_no = ""
        delivered = false
        discount = 0.0
        latitude = 0.0
        longitude = 0.0
        notes = ""
        payment_method = ""
        payment_value = 0.0
        purchaseDetails = [PurchaseDetail]()
        payments = [PurchasePayment]()
    }
}
