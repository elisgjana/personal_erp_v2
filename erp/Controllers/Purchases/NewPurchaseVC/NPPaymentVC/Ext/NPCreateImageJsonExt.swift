//
//  NPCreateImageJsonExt.swift
//  erp
//
//  Created by Admin on 06/06/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension NPPaymentVC{
    func createNewPurchaseImage(){
        var newPurchaseImage = PurchaseImage()
        newPurchaseImage.photo = purchaseImage
        newPurchaseImage.uuid = uuid
        storeIndividualPurchaseImageToRealm(purchaseImage: newPurchaseImage)
        self.activityIndicator.stopAnimating()
    }
    
}
