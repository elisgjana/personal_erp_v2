//
//  NPProductsTableViewExt.swift
//  erp
//
//  Created by Admin on 23/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension NewPurchaseProducts{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return purchaseDetails.count
    }
    //Swipe to delete
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete){
            var currentItemPrice = purchaseDetails[indexPath.row].product_price! * purchaseDetails[indexPath.row].amount!
            currentItemPrice = currentItemPrice - purchaseDetails[indexPath.row].discount! * 0.01 * currentItemPrice
            setPurchaseInvoiceTotal(total: getPurchaseInvoiceTotal()-currentItemPrice)
            totalPrizeLabel.text = "\(getPurchaseInvoiceTotal()) lekë"
            purchaseDetails.remove(at: indexPath.item)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: purchaseDetails.count)
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let product = uiRealm.objects(ProductModel.self).filter("id = \(purchaseDetails[indexPath.row].product_id!)").first
        let cell = tableView.dequeueReusableCell(withIdentifier: "purchaseProductCell", for: indexPath) as! PurchaseProductCell
        
        let price = purchaseDetails[indexPath.row].product_price!
        cell.productPriceField.text = String(price) + " lekë"
        
        let amount = purchaseDetails[indexPath.row].amount!
        cell.productAmountField.text = String(amount)
        
        var currentPrice = purchaseDetails[indexPath.row].product_price! * purchaseDetails[indexPath.row].amount!
        currentPrice = currentPrice - purchaseDetails[indexPath.row].discount! * 0.01 * currentPrice
        cell.totalProductPriceLabel.text = "\(currentPrice) lekë"
        
        let productName = product?.name!
        cell.productNameLabel.text = productName
        
        //here I change the image of the product using URL
        if let imgUrl = product?.photo as? String{
            let imageUrl = "\(Urls.BASE_IMAGE_URL)\(imgUrl)"
            cell.productImage.pin_setImage(from: URL(string: imageUrl))
            if cell.productImage.image == nil {
                cell.productImage.image = UIImage(named: "default_product_image") //default product image when no image on server
            }
        }
        stylePurchaseProductTable(cell , indexPath)
        
        return cell
    }
}
