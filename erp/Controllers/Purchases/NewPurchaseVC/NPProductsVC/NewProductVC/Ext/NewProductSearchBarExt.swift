//
//  NewProductSearchBarExt.swift
//  erp
//
//  Created by Admin on 23/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension NPPNewProductVC{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        productsCopy = allProducts.filter({ product -> Bool in
            guard !searchText.isEmpty else {
                tableView.reloadData()
                return true
            }
            return  (product.name?.uppercased().contains(searchText.uppercased()))!
        })
        tableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
}
