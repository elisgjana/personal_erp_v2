//
//  NewProductTableViewExt.swift
//  erp
//
//  Created by Admin on 23/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension NPPNewProductVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productsCopy.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        productLabel.text = productsCopy[indexPath.row].name!
        productLabel.textColor = UIColor.black
        selectedProductId = Int(productsCopy[indexPath.row].id.value!)
        selected_product_id = Int(productsCopy[indexPath.row].id.value!)
        selectedProductName = productsCopy[indexPath.row].name!
        self.priceField.text = String(describing: productsCopy[indexPath.row].buy_price.value!)
        whiteView.endEditing(true)
        blackView.endEditing(true)
        handleSelectProductDismiss()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productCell", for: indexPath) as! ProductCell
        let product = productsCopy[indexPath.row]
        cell.productNameLabel.text = product.name ?? "-"
        if AuthUser.has(PermissionTo.SEE_PRODUCT_SALE_PRICE){
            cell.salePriceLabel.text = String(describing: product.sale_price.value!)
        }
        if AuthUser.has(PermissionTo.SEE_PRODUCT_BUY_PRICE){
            cell.buyPirceLabel.text = String(describing: product.buy_price.value!)
        }
        if AuthUser.has(PermissionTo.SEE_PRODUCT_CODE){
            cell.itemCodeLabel.text = product.code!
        }
        cell.idLabel.text = String(describing: product.id.value!)
        let imageUrl = "\(Urls.BASE_IMAGE_URL)\(product.photo ?? "")"
        cell.productImage.pin_setImage(from: URL(string: imageUrl))
        if cell.productImage.image == nil {
            cell.productImage.image = UIImage(named: "default_product_image")
        }
        styleProductsTable(cell , indexPath)
        return cell
    }
    
}
