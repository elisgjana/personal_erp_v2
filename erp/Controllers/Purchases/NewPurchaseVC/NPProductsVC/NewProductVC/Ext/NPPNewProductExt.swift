//
//  NPPNewProductExt.swift
//  erp
//
//  Created by Admin on 23/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension NPPNewProductVC{
    @objc func handleSelectProduct(){
        if let window = UIApplication.shared.keyWindow{
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProductDismiss)))
            blackView.backgroundColor = UIColor(white:0, alpha:0.5)
            whiteView.backgroundColor = UIColor.white
            searchBar.searchBarStyle = .minimal
            searchBar.placeholder = Strings.PRODUCT_SEARCHBAR_PLACEHOLDER
            window.addSubview(blackView)
            window.addSubview(whiteView)
            let height = Int(window.frame.height) - 55
            let y = Int(window.frame.height) - height
            whiteView.frame = CGRect(x: 0, y: Int(window.frame.height), width: Int(window.frame.width), height: height)
            whiteView.addSubview(tableView)
            whiteView.addSubview(searchBar)
            blackView.frame = window.frame
            blackView.alpha = 0
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping :1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.whiteView.frame = CGRect(x: 0, y: y, width: Int(window.frame.width), height: height)
                self.searchBar.addContstraintsRelatedToSuperview(leading: 0, trailing: 0, top: 5, bottom: Int(self.whiteView.frame.height) - 44)
                self.tableView.addContstraintsRelatedToSuperview(leading: 0, trailing: 0, top: 44, bottom: 0)
                
            },completion : nil)
        }
    }
    
    @objc func handleSelectProductDismiss(){
        UIView.animate(withDuration: 0.5, animations: {
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow{
                self.whiteView.frame = CGRect(x: 0, y: window.frame.height, width: self.whiteView.frame.width, height: self.whiteView.frame.height)
            }
        })
    }
}
