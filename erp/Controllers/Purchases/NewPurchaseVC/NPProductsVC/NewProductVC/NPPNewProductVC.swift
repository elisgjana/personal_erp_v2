//
//  NPPNewProductVC.swift
//  erp
//
//  Created by Admin on 23/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class NPPNewProductVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var amountImage: UIImageView!
    @IBOutlet weak var discountField: UITextField!
    @IBOutlet weak var priceField: UITextField!
    @IBOutlet weak var priceImage: UIImageView!
    @IBOutlet weak var discountImage: UIImageView!
    @IBOutlet weak var detailsView: UIView!
    var blackView = UIView()
    var whiteView = UIView()
    var searchBar = UISearchBar()
    var tableView = UITableView()
    var allProducts = [ProductModel]()
    var productsCopy = [ProductModel]()
    var temp = 0
    var selectedProductId = 0
    var selectedProductRecomandedPrice = 0
    var selectedProductName = ""
    var productDiscount = 0.00
    var productAmount = 0.00
    var productPrice = 0.00
    var productValue = 0.00
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeAppearanceComponents()
        initializeComponents()
        queryPurchases()
        productsCopy = allProducts
        let tapProducts = UITapGestureRecognizer(target: self, action: #selector(NPPNewProductVC.productTapped(gesture:)))
        //add it to view
        productView.addGestureRecognizer(tapProducts)
        // make sure View can be interacted with by user
        productView.isUserInteractionEnabled = true
        
    }
    func queryPurchases(){
        for each in uiRealm.objects(ProductModel.self){
            self.allProducts.append(each)
        }
    }
    
    @objc func productTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            handleSelectProduct()
        }
    }
    
    func initializeAppearanceComponents(){
        createRoundCorner(addButton, 10)
        createRoundCorner(cancelButton, 10)
        amountImage.tintColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        priceImage.tintColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        discountImage.tintColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        createRoundCorner(productView, 10)
        createRoundCorner(detailsView, 10)
        self.title = Strings.ADD_PRODUCT
        amountField.keyboardType = .decimalPad
        discountField.keyboardType = .decimalPad
    }
    
    func initializeComponents(){
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        let nib = UINib(nibName: "ProductCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "productCell")
        tableView.rowHeight = 151
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        hideKeyboardOnViewTap()
    }
    
    @IBAction func addButtonTUP(_ sender: Any) {
        //check if all required fields are filled
        if  !validateInput() {
            return
        }
        if let discount = Double(discountField.text!){
            productDiscount = discount
        }
        if let amount = Double(amountField.text!){
            productAmount = amount
        }
        if let price = Double(priceField.text!){
            productPrice = price
        }
        
        productValue = productAmount*productPrice - (productPrice*productAmount*productDiscount*0.01)
        purchaseDetails.append(PurchaseDetail(product_id: Int32(selectedProductId),product_name:selectedProductName, amount: productAmount, product_price: productPrice, discount: productDiscount, value: productValue))
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "purchaseProductsUpdated"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "purchasePaymentUpdated"), object: nil)
        navigationController?.popViewController(animated: true)
    }
    @IBAction func cancelButtonTUP(_ sender: Any) {
        setProductId(id: 0)
        navigationController?.popViewController(animated: true)
    }
    
    func validateInput()->Bool{
        var missingData = ""
        var valid = true
        if productLabel.text?.trim().count == 0 || productLabel.text == "Zgjidhni Produktin..."{
            missingData = missingData + Strings.MISSING_PRODUCT_NAME_DATA
            valid = false
        }
        if amountField.text?.trim().count == 0{
            missingData = missingData + Strings.MISSING_AMOUNT_DATA
            valid = false
        }
        if priceField.text?.trim().count == 0{
            missingData = missingData + Strings.MISSING_PRICE_DATA
            valid = false
        }
        if discountField.text?.trim().count == 0{
            missingData = missingData + Strings.MISSING_DISCOUNT_DATA
            valid = false
        }
        if !valid{
            displayAlert(title: Strings.ADD_INVOICE_PRODUCT_TITLE, message: Strings.ADD_INVOICE_PRODUCT_MESSAGE + missingData)
        }
        return valid
    }
}
