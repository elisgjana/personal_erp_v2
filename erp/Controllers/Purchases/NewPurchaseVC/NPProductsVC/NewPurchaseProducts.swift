//
//  NewPurchaseProducts.swift
//  erp
//
//  Created by Admin on 22/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class NewPurchaseProducts: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var totalPrizeLabel: UILabel!
    @IBOutlet weak var totalPrizeView: UIView!
    @IBOutlet weak var moneyImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var hiddenButton: UIButton!
    
    let noItemsView = NoItemsFoundView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeComponents()
        initializeAppearanceComponents()
        reloadDataPurchaseWishlistObserver()
    }
    func initializeComponents(){
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "PurchaseProductCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "purchaseProductCell")
        tableView.rowHeight = 153
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        hideKeyboardOnViewTap()
    }
    override func viewWillAppear(_ animated: Bool) {
        registerToPurchaseProductAddedEvent()
        showNoItemLogo(insetionView: tableView, noItemsView: noItemsView, itemsNumber: purchaseDetails.count)
    }
    func registerToPurchaseProductAddedEvent(){
        NotificationCenter.default.addObserver(self, selector: #selector(purchaseProductsUpdated), name: NSNotification.Name(rawValue: "purchaseProductsUpdated"), object: nil)
    }
    
    @objc func purchaseProductsUpdated(){
        calculatePurchaseInvoiceTotal()
        totalPrizeLabel.text = "\(getPurchaseInvoiceTotal()) lekë"
        self.tableView.reloadData()
    }
    func initializeAppearanceComponents(){
        moneyImage.tintColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        totalPrizeView.layer.borderWidth = 2
        createRoundCorner(totalPrizeView, 10)
        totalPrizeView.layer.borderColor = createColorFromHex(hex: Colors.PRIMARY_DARK).cgColor
        createRoundButton(button: addButton)
        createRoundButton(button: hiddenButton)
    }

    @IBAction func addButtonTUP(_ sender: Any) {
        
    }

    @IBAction func hiddenButtonTUP(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showHiddenView"), object: nil)
    }
    func reloadDataPurchaseWishlistObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(reloadPurchaseProducts), name: NSNotification.Name(rawValue: "reloadPurchaseProducts"), object: nil)
    }
    @objc func reloadPurchaseProducts(){
        calculatePurchaseInvoiceTotal()
        totalPrizeLabel.text = String(getPurchaseInvoiceTotal())
        self.tableView.reloadData()
        showNoItemLogo(insetionView: tableView, noItemsView: noItemsView, itemsNumber: purchaseDetails.count)
    }
}
