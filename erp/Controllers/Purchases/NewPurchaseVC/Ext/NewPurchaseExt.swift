//
//  NewPurchaseExt.swift
//  erp
//
//  Created by Admin on 20/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import TwicketSegmentedControl
import SnapKit

extension NewPurchaseVC{
    
    // VIEWS
    func configureViewHierarchy(){
        view.addSubview(segmentControlView)
        segmentControlView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(view)
        }
        
        segmentControlView.addSubview(twicketSegmentControl)
        twicketSegmentControl.delegate = self;
        twicketSegmentControl.snp.makeConstraints { (make) in
            
            // PUTS THE SEGMENT CONTROL 30px (= THE HEIGHT OF THE CONTROL) OVER THE VIEW
            make.top.equalTo(view)
            
            
            // DEFINE THE BORDERS TO THE SIDES
            make.left.equalTo(segmentControlView).offset(16)
            make.right.equalTo(segmentControlView).offset(-16)
            
            make.bottom.equalTo(segmentControlView).offset(-10)
            
            // DEFINE THE HEIGHT
            make.height.equalTo(20)
        }
    }
    
    // CHOSING VIEW BASED ON SEGMENT CHOSEN
    func didSelect(_ segmentIndex: Int) {
        
        switch segmentIndex
        {
        case 0:
            // SHOWING THE INFO VIEW
            infoView.alpha = 1
            productsView.alpha = 0
            paymentView.alpha = 0
            wishlistDetailsView.alpha = 0
            break
        case 1:
            // SHOWING THE PRODUCTS VIEW
            infoView.alpha = 0
            productsView.alpha = 1
            paymentView.alpha = 0
            wishlistDetailsView.alpha = 0
            break
        case 2:
            // SHOWING THE PAYMENT VIEW
            infoView.alpha = 0
            productsView.alpha = 0
            paymentView.alpha = 1
            wishlistDetailsView.alpha = 0
            break
        default:
            break
        }
        
    }
}
