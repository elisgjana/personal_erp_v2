//
//  NPInfoExt.swift
//  erp
//
//  Created by Admin on 20/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit
extension NPInfoVC{
    @objc func handleSelectItem(){
        if let window = UIApplication.shared.keyWindow{
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSlideWindowDismiss)))
            blackView.backgroundColor = UIColor(white:0, alpha:0.5)
            whiteView.backgroundColor = UIColor.white
            warehousesSearchBar.searchBarStyle = .minimal
            suppliersSearchBar.searchBarStyle = .minimal
            suppliersSearchBar.placeholder = Strings.SUPPLIER_SEARCHBAR_PLACEHOLDER
            warehousesSearchBar.placeholder = Strings.WAREHOUSE_SEARCHBAR_PLACEHOLDER
            window.addSubview(blackView)
            window.addSubview(whiteView)
            let height = Int(window.frame.height) - 55
            let y = Int(window.frame.height) - height
            whiteView.frame = CGRect(x: 0, y: Int(window.frame.height), width: Int(window.frame.width), height: height)
            if viewTapped == "Supplier"{
                warehousesTableView.removeFromSuperview()
                warehousesSearchBar.removeFromSuperview()
                whiteView.addSubview(suppliersTableView)
                suppliersTableView.delegate = self
                suppliersTableView.reloadData()
                whiteView.addSubview(suppliersSearchBar)
            }else if viewTapped == "Warehouse"{
                suppliersTableView.removeFromSuperview()
                suppliersSearchBar.removeFromSuperview()
                whiteView.addSubview(warehousesTableView)
                whiteView.addSubview(warehousesSearchBar)
                warehousesTableView.delegate = self
                warehousesTableView.reloadData()
            }
            blackView.frame = window.frame
            blackView.alpha = 0
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping :1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.whiteView.frame = CGRect(x: 0, y: y, width: Int(window.frame.width), height: height)
                if self.viewTapped == "Supplier"{
                    self.suppliersSearchBar.addContstraintsRelatedToSuperview(leading: 0, trailing: 0, top: 5, bottom: Int(self.whiteView.frame.height) - 44)
                    self.suppliersTableView.addContstraintsRelatedToSuperview(leading: 0, trailing: 0, top: 44, bottom: 0)
                }else if self.viewTapped == "Warehouse"{
                    self.warehousesSearchBar.addContstraintsRelatedToSuperview(leading: 0, trailing: 0, top: 5, bottom: Int(self.whiteView.frame.height) - 44)
                    self.warehousesTableView.addContstraintsRelatedToSuperview(leading: 0, trailing: 0, top: 44, bottom: 0)
                }
            },completion : nil)
        }
    }
    
    @objc func handleSlideWindowDismiss(){
        UIView.animate(withDuration: 0.5, animations: {
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow{
                self.whiteView.frame = CGRect(x: 0, y: window.frame.height, width: self.whiteView.frame.width, height: self.whiteView.frame.height)
                self.viewTapped = ""
            }
        })
    }
}
