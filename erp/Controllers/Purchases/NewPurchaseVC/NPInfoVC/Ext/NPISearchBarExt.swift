//
//  NPISearchBarExt.swift
//  erp
//
//  Created by Admin on 22/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit
extension NPInfoVC{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar == suppliersSearchBar{
            suppliersCopy = suppliers.filter({ supplier -> Bool in
                guard !searchText.isEmpty else {
                    suppliersTableView.reloadData()
                    return true
                }
                
                return  (supplier.name?.uppercased().contains(searchText.uppercased()))!
            })
            suppliersTableView.reloadData()
        }else{
            warehousesCopy = warehouses.filter({ warehouse -> Bool in
                guard !searchText.isEmpty else {
                    warehousesTableView.reloadData()
                    return true
                }
                return  (warehouse.name?.uppercased().contains(searchText.uppercased()))!
            })
            warehousesTableView.reloadData()
        }
        
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar == suppliersSearchBar{
            self.suppliersSearchBar.endEditing(true)
            suppliersSearchBar.resignFirstResponder()
        }else{
            self.warehousesSearchBar.endEditing(true)
            warehousesSearchBar.resignFirstResponder()
        }
        
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.suppliersSearchBar.endEditing(true)
        suppliersSearchBar.showsCancelButton = false
        suppliersSearchBar.resignFirstResponder()
        self.warehousesSearchBar.endEditing(true)
        warehousesSearchBar.showsCancelButton = false
        warehousesSearchBar.resignFirstResponder()
    }
}
