//
//  NPITableViewExt.swift
//  erp
//
//  Created by Admin on 20/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension NPInfoVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == suppliersTableView{
            return suppliersCopy.count
        }else{
            return warehousesCopy.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == suppliersTableView{
            supplierLabel.text = suppliersCopy[indexPath.row].name!
            selectedSupplierUuid = suppliersCopy[indexPath.row].uuid!
            supplier_name_search = suppliersCopy[indexPath.row].name!
            view.endEditing(true)
            handleSlideWindowDismiss()
        }else if tableView == warehousesTableView{
            warehouseLabel.text = warehousesCopy[indexPath.row].name!
            selectedWarehouseId = Int(warehousesCopy[indexPath.row].warehouse_id.value!)
            view.endEditing(true)
            handleSlideWindowDismiss()
        }
        whiteView.endEditing(true)
        blackView.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewTapped == "Supplier"{
            return createSupplierCell(indexPath: indexPath)
        }else{
            return createWarehouseCell(indexPath: indexPath)
        }
        
    }
    func createSupplierCell(indexPath: IndexPath)->UITableViewCell{

        let cell = suppliersTableView.dequeueReusableCell(withIdentifier: "searchSupplierCell", for: indexPath) as! SearchSupplierCell
        let supplier = suppliersCopy[indexPath.row]
        cell.supplierLabel.text = supplier.name!
        
        styleSearchSuppliersTable(cell, indexPath)
        return cell
    }
    func createWarehouseCell(indexPath: IndexPath)->UITableViewCell{
        
        let cell = warehousesTableView.dequeueReusableCell(withIdentifier: "searchWarehouseCell", for: indexPath) as! SearchWarehouseCell
        let warehouse = warehousesCopy[indexPath.row]
        cell.warehouseNameLabel.text = warehouse.name!
        styleSearchWarehousesTable(cell, indexPath)
        
        return cell
    }
}
