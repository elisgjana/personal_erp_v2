//
//  NPInfoVC.swift
//  erp
//
//  Created by Admin on 20/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import CoreLocation

class NPInfoVC: UIViewController , UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var warehouseView: UIView!
    @IBOutlet weak var supplierView: UIView!
    @IBOutlet weak var invoiceNrView: UIView!
    @IBOutlet weak var seriesNrView: UIView!
    @IBOutlet weak var notesView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var warehouseLabel: UILabel!
    @IBOutlet weak var seriesNrField: UITextField!
    @IBOutlet weak var notesField: UITextField!
    @IBOutlet weak var supplierLabel: UILabel!
    @IBOutlet weak var invoiceNrField: UITextField!
    @IBOutlet weak var calendarImage: UIImageView!
    @IBOutlet weak var search1Image: UIImageView!
    @IBOutlet weak var search2Image: UIImageView!
    @IBOutlet weak var edit1Image: UIImageView!
    @IBOutlet weak var edit2Image: UIImageView!
    @IBOutlet weak var edit3Image: UIImageView!
    @IBOutlet weak var confirmedStatusLabel: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var cameraImage: UIImageView!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var uploadedImage: UIImageView!
    var blackView = UIView()
    var whiteView = UIView()
    var suppliersSearchBar = UISearchBar()
    var warehousesSearchBar = UISearchBar()
    var suppliersTableView = UITableView()
    var warehousesTableView = UITableView()
    var viewTapped = ""
    var suppliers = [SupplierModel]()
    var warehouses = [WarehouseModel]()
    var suppliersCopy = [SupplierModel]()
    var warehousesCopy = [WarehouseModel]()
    var selectedSupplierUuid = ""
    var selectedWarehouseId = 0
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var imagePicker: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeComponents()
        initializeAppearanceComponents()
        querySuppliers()
        queryWarehouses()
        initializeSupplierSelection()
        initializeWarehouseSelection()
        dateLabel.text = Device.getCurrentDate()
        
    }
    func querySuppliers(){
        let allItems = uiRealm.objects(SupplierModel.self)
        for item in allItems{
            suppliersCopy.append(item)
            suppliers.append(item)
        }
    }
    func queryWarehouses(){
        let allItems = uiRealm.objects(WarehouseModel.self)
        for item in allItems{
            warehousesCopy.append(item)
            warehouses.append(item)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        confirmedStatusLabel.text = Strings.NOT_CONFIRMED
        confirmedStatusLabel.textColor = UIColor.red
    }
    func initializeWarehouseSelection(){
        //here I make the choose warehouse slide from bottom
        let tapWarehouse = UITapGestureRecognizer(target: self, action: #selector(NPInfoVC.warehouseTapped(gesture:)))
        //add it to view
        warehouseView.addGestureRecognizer(tapWarehouse)
        // make sure View can be interacted with by user
        warehouseView.isUserInteractionEnabled = true
    }
    @objc func warehouseTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            viewTapped = "Warehouse"
            handleSelectItem()
        }
        
    }
    func initializeSupplierSelection(){
        //here I make the choose product slide from bottom
        let tapSupplier = UITapGestureRecognizer(target: self, action: #selector(NPInfoVC.supplierTapped(gesture:)))
        //add it to view
        supplierView.addGestureRecognizer(tapSupplier)
        // make sure View can be interacted with by user
        supplierView.isUserInteractionEnabled = true
    }
    @objc func supplierTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            viewTapped = "Supplier"
            handleSelectItem()
        }
    }
    func initializeComponents(){
        suppliersTableView.delegate = self
        suppliersTableView.dataSource = self
        warehousesTableView.delegate = self
        warehousesTableView.dataSource = self
        suppliersSearchBar.delegate = self
        warehousesSearchBar.delegate = self
        let supplierNib = UINib(nibName: "SearchSupplierCell", bundle: nil)
        let warehouseNib = UINib(nibName: "SearchWarehouseCell", bundle: nil)
        suppliersTableView.register(supplierNib, forCellReuseIdentifier: "searchSupplierCell")
        warehousesTableView.register(warehouseNib, forCellReuseIdentifier: "searchWarehouseCell")
        suppliersTableView.rowHeight = 58
        warehousesTableView.rowHeight = 50
        self.suppliersTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.warehousesTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.suppliersSearchBar.backgroundColor = UIColor.groupTableViewBackground
        self.warehousesTableView.backgroundColor = UIColor.groupTableViewBackground
        hideKeyboardOnViewTap()
        locManager.requestWhenInUseAuthorization()
    }
    func initializeAppearanceComponents(){
        createRoundCorner(dateView, 10)
        createRoundCorner(warehouseView, 10)
        createRoundCorner(supplierView, 10)
        createRoundCorner(invoiceNrView, 10)
        createRoundCorner(seriesNrView, 10)
        createRoundCorner(notesView, 10)
        createRoundCorner(imageView, 10)
        createRoundCorner(confirmButton, 10)
        createRoundCorner(photoButton, 10)
        calendarImage.tintColor = UIColor.gray
        search1Image.tintColor = UIColor.gray
        search2Image.tintColor = UIColor.gray
        edit1Image.tintColor = UIColor.gray
        edit2Image.tintColor = UIColor.gray
        edit3Image.tintColor = UIColor.gray
        cameraImage.tintColor = UIColor.gray
    }
    @IBAction func confirmButtonTUP(_ sender: Any) {
        if  !validateInput() {
            return
        }
        if let photo = uploadedImage.image{
            purchaseImage = convertToBase64(image: photo)
        }
        supplier_uuid = selectedSupplierUuid
        supplier_name_search = supplierLabel.text!
        uuid = NSUUID().uuidString
        warehouse_id = selectedWarehouseId
        invoice_no = invoiceNrField.text!
        serial_no = seriesNrField.text!
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            currentLocation = locManager.location
            latitude = currentLocation.coordinate.latitude
            longitude = currentLocation.coordinate.longitude
        }
        notes = notesField.text!
        confirmedStatusLabel.text = Strings.CONFIRMED
        confirmedStatusLabel.textColor = createColorFromHex(hex: Colors.INOVACION_GREEN)
        infoIsConfirmed = true
    }
    func validateInput()->Bool{
        var valid = true
        var displayedMissingFieldsMessage = ""
        if warehouseLabel.text! == Strings.WAREHOUSE{
            valid = false
            displayedMissingFieldsMessage += Strings.WAREHOUSE + " \n"
        }
        if supplierLabel.text! == Strings.SUPPLIER{
            valid = false
            displayedMissingFieldsMessage += Strings.SUPPLIER + " \n"
        }
        
        if invoiceNrField.text?.trim().count == 0{
            valid = false
            displayedMissingFieldsMessage += Strings.INVOICE_NO + "\n"
        }
        
        if seriesNrField.text?.trim().count == 0{
            valid = false
            displayedMissingFieldsMessage += Strings.SERIES_NO + " \n"
        }
        if !valid{
            displayAlert(title: Strings.MISSING_DATA, message: Strings.COMPLETE_EMPTY_FIELDS + " \n \(displayedMissingFieldsMessage)")
        }
        return valid
    }
    
    @IBAction func photoButtonTUP(_ sender: Any) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        let cameraAction = UIAlertAction(title: "Përdor Kamerën", style: .default){ (action) in
            self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        let photoLibraryAction = UIAlertAction(title: "Hap Albumin e Fotove", style: .default){ (action) in
            self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cameraAction)
        alertController.addAction(photoLibraryAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let choosenImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            uploadedImage.image = choosenImage
            uploadedImage.layer.borderWidth = 3
            uploadedImage.layer.borderColor = UIColor.white.cgColor
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}
