//
//  NewPurchaseVC.swift
//  erp
//
//  Created by Admin on 20/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import TwicketSegmentedControl

class NewPurchaseVC: UIViewController , TwicketSegmentedControlDelegate {

    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var productsView: UIView!
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var wishlistDetailsView: UIView!
    
    //Background of the top slider
    var segmentControlView:UIView = {
        var uiView = UIView()
        uiView.backgroundColor = UIColor.white
        return uiView
    }()
    
    var contentView:UIView = {
        var uiView = UIView()
        return uiView
    }()
    
    // DESIGN OF THE SEGMENT
    var twicketSegmentControl:TwicketSegmentedControl = {
        var twicketSegmentControl : TwicketSegmentedControl = TwicketSegmentedControl(frame:CGRect.zero)
        
        // SETTING THE TITLES
        let titles = ["INFO", "PRODUKTET", "PAGESA"]
        twicketSegmentControl.setSegmentItems(titles)
        
        // SETTING THE TEXT COLOR OF THE CHOSEN SEGMENT TO WHITE
        twicketSegmentControl.highlightTextColor = UIColor.white
        
        // SETTING THE TEXT COLOR OF THE OTHER SEGMENTS TO A DARK GRAY
        twicketSegmentControl.defaultTextColor = UIColor.init(red: 171/255.0, green: 183/255.0, blue: 183/255.0, alpha: 1.0)
        
        // SETTING THE BACKGROUND COLOR OF THE CHOSEN SEGMENT TO BLUE
        twicketSegmentControl.sliderBackgroundColor = createColorFromHex(hex: Colors.PRIMARY_LIGHT)
        
        // SETTING THE BACKGROUND COLOR OF THE OTHER SEGMENTS TO A VERY LIGHT GRAY
        twicketSegmentControl.segmentsBackgroundColor = UIColor.init(red: 238/255.0, green: 238/255.0, blue: 238/255.0, alpha: 1.0)
        
        // SHOWS SEGMENTS
        return twicketSegmentControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = Strings.NEW_PURCHASE_TITLE
        didSelect(0)
        super.viewDidLoad()
        configureViewHierarchy()
        registerToShowWishlistDetailsViewEvent()
        registerToHideWishlistDetailsViewEvent()
        
    }
    func registerToShowWishlistDetailsViewEvent(){
        NotificationCenter.default.addObserver(self, selector: #selector(showHiddenView), name: NSNotification.Name(rawValue: "showHiddenView"), object: nil)
    }
    @objc func showHiddenView(){
        wishlistDetailsView.alpha = 1
    }
    func registerToHideWishlistDetailsViewEvent(){
        NotificationCenter.default.addObserver(self, selector: #selector(hideHiddenView), name: NSNotification.Name(rawValue: "hideHiddenView"), object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        orderItemForPurchase = [OrderItem]()
    }
    @objc func hideHiddenView(){
        wishlistDetailsView.alpha = 0
    }

}
