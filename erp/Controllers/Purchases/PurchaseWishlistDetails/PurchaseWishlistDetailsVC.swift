//
//  PurchaseWishlistDetailsVC.swift
//  erp
//
//  Created by Admin on 30/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class PurchaseWishlistDetailsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var priceField: UITextField!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var darkView: UIView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet var orderView: UIView!
    @IBOutlet weak var seenButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var wishlistCopy = [OrderItem]()
    var selectedProductName = ""
    var selectedProductId = 0
    var selectedProductAmount = 0.0
    var selectedProductPrice = 0.0
    var selectedProductIndexPath = IndexPath()
    let noItemsView = NoItemsFoundView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        intializeComponents()
        intializeAppearanceComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        wishlistCopy = orderItemForPurchase
        showNoItemLogo(insetionView: tableView, noItemsView: noItemsView, itemsNumber: wishlistCopy.count)
    }
    
    func intializeComponents(){
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        let nib = UINib(nibName: "PurchaseProductCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "purchaseProductCell")
        navigationController?.navigationBar.tintColor = UIColor.white
        tableView.rowHeight = 153
    }
    
    func intializeAppearanceComponents(){
        createRoundButton(button: seenButton)
        darkView.isHidden = true
        createRoundCorner(addButton, 10)
        createRoundCorner(cancelButton, 10)
        createRoundCorner(orderView, 10)
        createRoundCorner(amountView, 10)
        createRoundCorner(priceView, 10)
    }
    
    @IBAction func seenButtonTUP(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "hideHiddenView"), object: nil)
    }
    
    @IBAction func addButtonTUP(_ sender: Any) {
        if  !validateInput() {
            return
        }
        if let amount = Double(amountField.text!){
            selectedProductAmount = amount
        }
        if let price = Double(priceField.text!){
            selectedProductPrice = price
        }
        purchaseDetails.append(PurchaseDetail(product_id: Int32(selectedProductId),product_name:selectedProductName, amount: selectedProductAmount, product_price: selectedProductPrice, discount: 0.0, value: selectedProductAmount*selectedProductPrice))
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadPurchaseProducts"), object: nil)
        darkView.isHidden = true
        orderView.removeFromSuperview()
        wishlistCopy.remove(at: selectedProductIndexPath.item)
        tableView.deleteRows(at: [selectedProductIndexPath], with: .automatic)
        emptyFields()
        showNoItemLogo(insetionView: tableView, noItemsView: noItemsView, itemsNumber: wishlistCopy.count)
        
    }
    
    @IBAction func cancelButtonTUP(_ sender: Any) {
        darkView.isHidden = true
        orderView.removeFromSuperview()
    }
    
    func validateInput()->Bool{
        var valid = true
        var displayedMissingFieldsMessage = ""
        
        if amountField.text! == ""{
            valid = false
            displayedMissingFieldsMessage += Strings.MISSING_AMOUNT_DATA
        }
        
        if priceField.text! == ""{
            valid = false
            displayedMissingFieldsMessage += Strings.MISSING_PRICE_DATA
        }
        
        if !valid{
            displayAlert(title: Strings.MISSING_DATA, message: Strings.COMPLETE_EMPTY_FIELDS + "\n \(displayedMissingFieldsMessage)")
        }
        return valid
    }
    func emptyFields(){
        amountField.text = ""
        priceField.text = ""
    }
}
