//
//  PWDSearchBarExt.swift
//  erp
//
//  Created by Admin on 30/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension PurchaseWishlistDetailsVC{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        wishlistCopy = orderItemForPurchase.filter({ wishlist -> Bool in
            guard !searchText.isEmpty else {
                tableView.reloadData()
                return true
            }
            
            return  (wishlist.product_name?.uppercased().contains(searchText.uppercased()))!
        })
        tableView.reloadData()
        showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: self.wishlistCopy.count)
    }
    
}
