//
//  PWDTableViewExt.swift
//  erp
//
//  Created by Admin on 30/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension PurchaseWishlistDetailsVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wishlistCopy.count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete){
            wishlistCopy.remove(at: indexPath.item)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: self.wishlistCopy.count)
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedProductId = Int(wishlistCopy[indexPath.row].product_id!)
        let product = uiRealm.objects(ProductModel.self).filter("id = \(selectedProductId)").first
        selectedProductIndexPath = indexPath
        
        if let productName = product?.name!{
            selectedProductName = productName
        }
        darkView.isHidden = false
        view.addSubview(orderView)
        orderView.center = CGPoint(x: view.frame.width / 2, y: view.frame.height / 2 - 30)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let product = uiRealm.objects(ProductModel.self).filter("id = \(wishlistCopy[indexPath.row].product_id!)").first
        let cell = tableView.dequeueReusableCell(withIdentifier: "purchaseProductCell", for: indexPath) as! PurchaseProductCell
        let currentWishlist = wishlistCopy[indexPath.row]
        
        let amount = currentWishlist.required_quantity ?? 0.0
        cell.requiredAmountLabel.text = "\(String(describing: amount))"
        
        let lastBuyPrice = currentWishlist.last_purchased_price ?? 0.0
        cell.lastBuyPriceLabel.text = "\(String(lastBuyPrice)) lekë"
        
        let productName = product?.name! ?? ""
        cell.productNameLabel.text = productName
        
        //here I change the image of the product using URL
        if let imgUrl = product?.photo!{
            let imageUrl = "\(Urls.BASE_IMAGE_URL)\(imgUrl)"
            cell.productImage.pin_setImage(from: URL(string: imageUrl))
            if cell.productImage.image == nil {
                cell.productImage.image = UIImage(named: "default_product_image") //default product image when no image on server
            }
        }
        
        stylePurchaseProductTable(cell, indexPath)
        
        return cell
    }
}
