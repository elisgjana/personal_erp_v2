//
//  PurchasesJsonExt.swift
//  erp
//
//  Created by Admin on 16/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation

extension TodayPurchasesVC{
    func getPurchasesFromServer(){
        self.activityIndicator.startAnimating()
        getApiClient().fetchApiResults(urlString: Urls.GET_USER_PURCHASES_URL + "?start_date=\(Device.getCurrentDate())", parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<Purchase>)->()
            in
            self.performUIUpdatesOnMain {
                self.activityIndicator.stopAnimating()
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                if response.data.count > 0 {
                    setAllPurchases(purchases: response.data)
                    storePurchasesInRealm()
                }
            }
        })
        
        self.activityIndicator.stopAnimating()
    }
}
