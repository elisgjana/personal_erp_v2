//
//  TPTableViewExt.swift
//  erp
//
//  Created by Admin on 18/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension TodayPurchasesVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return purchases.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "purchaseCell", for: indexPath) as! PurchaseCell
        
        let currentPurchase = purchases[indexPath.row]
        cell.supplierNameLabel.text = currentPurchase.supplier_name ?? ""
        cell.invoiceNumberLabel.text = currentPurchase.invoice_no ?? ""
        
        if let date = currentPurchase.date{
            cell.dateLabel.text = String(describing: date)
        }else{
            cell.dateLabel.text = "-"
        }
        let value = currentPurchase.value.value ?? 0.0
        cell.valueLabel.text = "\(value) lekë"
        
        let status = currentPurchase.status_id.value ?? 0
        cell.statusLabel.text = Constants.STATUSES[status] ?? "0"
        
        let isSync = currentPurchase.is_sync.value ?? true
        if isSync{
            cell.notSyncImage.isHidden = true
        }else{
            cell.notSyncImage.isHidden = false
        }
        
        if currentPurchase.status_id.value == 6{
            cell.statusLabel.textColor = createColorFromHex(hex: Colors.INOVACION_GREEN)
        }else if currentPurchase.status_id.value == 10{
            cell.statusLabel.textColor = createColorFromHex(hex: Colors.INOVACION_YELLOW)
        }else{
            cell.statusLabel.textColor = UIColor.red
        }
        stylePurchaseTable(cell,indexPath)
        
        return cell
    }
}
