//
//  TodayPurchasesVC.swift
//  erp
//
//  Created by Admin on 18/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class TodayPurchasesVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var refresher = UIRefreshControl()
    
    var purchases = [PurchaseModel]()
    var tableRowCount = 0
    var today = convertStringToDate(Device.getCurrentDate())
    var tomorrow = convertStringToDate(Device.getTomorrowDate())
    
    let noItemsView = NoItemsFoundView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeComponents()
        queryPurchases()
        refresher = UIRefreshControl()
        refresher.attributedTitle = NSAttributedString(string: Strings.PULL_TO_REFRESH_TITLE)
        refresher.addTarget(self, action: #selector(pullToRefreshFunc), for: UIControlEvents.valueChanged)
        tableView.addSubview(refresher)
    }
    
    @objc func pullToRefreshFunc(){
        getPurchasesFromServer()
        purchases = [PurchaseModel]()
        queryPurchases()
        refresher.endRefreshing()
        tableView.reloadData()
    }
    
    func queryPurchases(){
        let allItems = uiRealm.objects(PurchaseModel.self).filter("date >= %@ AND date < %@", today, tomorrow)
        for item in allItems{
            purchases.append(item)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        purchases = [PurchaseModel]()
        queryPurchases()
        tableView.reloadData()
        showNoItemLogo(insetionView: tableView, noItemsView: noItemsView, itemsNumber: purchases.count)
    }
    
    func initializeComponents(){
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "PurchaseCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "purchaseCell")
        tableView.rowHeight = 96
    }

}
