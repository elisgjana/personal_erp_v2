//
//  AllTimePurchasesVC.swift
//  erp
//
//  Created by Admin on 19/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class AllTimePurchasesVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var purchases = [PurchaseModel]()
    var isFiltered = false
    var refresher = UIRefreshControl()
    let noItemsView = NoItemsFoundView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeAppearanceComponents()
        initializeComponents()
        refresher = UIRefreshControl()
        refresher.attributedTitle = NSAttributedString(string: Strings.PULL_TO_REFRESH_TITLE)
        refresher.addTarget(self, action: #selector(pullToRefreshFunc), for: UIControlEvents.valueChanged)
        tableView.addSubview(refresher)
    }
    
    @objc func pullToRefreshFunc(){
        queryPurchases()
        refresher.endRefreshing()
        tableView.reloadData()
    }
    
    func initializeComponents(){
        queryPurchases()
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "PurchaseCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "purchaseCell")
        tableView.rowHeight = 96
    }
    
    func queryPurchases(){
        purchases = [PurchaseModel]()
        for each in uiRealm.objects(PurchaseModel.self){
            self.purchases.append(each)
        }
        showNoItemLogo(insetionView: tableView, noItemsView: noItemsView, itemsNumber: purchases.count)
    }
    
    func initializeAppearanceComponents(){
        createRoundButton(button: filterButton)
    }
}
