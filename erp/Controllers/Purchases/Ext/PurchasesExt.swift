//
//  PurchasesExt.swift
//  erp
//
//  Created by Admin on 16/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import TwicketSegmentedControl
import SnapKit

extension PurchasesVC{

    // VIEWS
    func configureViewHierarchy(){
        view.addSubview(segmentControlView)
        segmentControlView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(view)
        }
        
        segmentControlView.addSubview(twicketSegmentControl)
        twicketSegmentControl.delegate = self;
        twicketSegmentControl.snp.makeConstraints { (make) in
            
            // PUTS THE SEGMENT CONTROL 30px (= THE HEIGHT OF THE CONTROL) OVER THE VIEW
            make.top.equalTo(view)
            
            // DEFINE THE BORDERS TO THE SIDES
            make.left.equalTo(segmentControlView).offset(16)
            make.right.equalTo(segmentControlView).offset(-16)
            make.bottom.equalTo(segmentControlView).offset(-10)
            
            // DEFINE THE HEIGHT
            make.height.equalTo(30)
        }
    }
    
    // CHOSING VIEW BASED ON SEGMENT CHOSEN
    func didSelect(_ segmentIndex: Int) {
        
        switch segmentIndex
        {
        case 0:
            // SHOWING THE TODAY VIEW
            todayView.alpha = 1
            allTimeView.alpha = 0
            break;
        case 1:
            // SHOWING THE ALLTIME VIEW
            todayView.alpha = 0
            allTimeView.alpha = 1
            break;
        default:
            break;
        }
    }
}
