//
//  ProductionsJsonExt.swift
//  erp
//
//  Created by Admin on 13/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation

extension ProductionsVC{
    func getProductionsFromServer(){
            CustomActivityIndicator.instance.showCustomActivityIndicator(view)
            let url = Urls.GET_ALL_PRODUCTIONS_URL + String(SavedVariables.getDefaultWarehouseId())
            print(url)
            getApiClient().fetchApiResults(urlString: url, parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<Production>)->()
                in
                self.performUIUpdatesOnMain {
                    if response.status != 0 {
                        self.presentApiErrorPopup(statusCode: response.status!)
                        return
                    }
                    if response.data.count > 0 {
                        setAllProductions(productions: response.data)
                        self.tableView.reloadData()
                        self.productions = getAllProductions()
                        self.tableView.reloadData()
                    }
                    self.apiCallFinished()
                }
            })
    }
    
    @objc func apiCallFinished(){
        self.performUIUpdatesOnMain {
            CustomActivityIndicator.instance.hideCustomActivityIndicator()
            self.refresher.endRefreshing()
            self.showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: self.productions.count)
        }
    }
}
