//
//  ProductionsTableViewExt.swift
//  erp
//
//  Created by Admin on 13/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension ProductionsVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productions.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        warehouseId = Int(getAllProductions()[indexPath.row].warehouse_id!)
        performSegue(withIdentifier: "productionDetails", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var warehouseLabel = ""
        let cell = tableView.dequeueReusableCell(withIdentifier: "rejectionCell", for: indexPath) as! RejectionCell
        let currentProduction = productions[indexPath.row]
        
        let date = currentProduction.date!
        cell.dateLabel.text = String(describing: date)
        
        if let startWarehouse = currentProduction.warehouse_name{
            warehouseLabel = "Nga \(startWarehouse)"
        }
        
        if let endWarehouse = currentProduction.destination_warehouse_name{
            warehouseLabel += " Tek \(endWarehouse)"
        }
        
        cell.warehousesLabel.text = warehouseLabel
        styleRejectionTable(cell, indexPath)
        
        return cell
    }
}
