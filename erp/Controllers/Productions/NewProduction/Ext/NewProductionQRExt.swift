//
//  NewProductionQRExt.swift
//  erp
//
//  Created by Admin on 14/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import AVKit
import AVFoundation
import MTBBarcodeScanner
import UIKit

extension NewProductionVC{
    func QRGenerator(){
        MTBBarcodeScanner.requestCameraPermission(success: { success in
            if success {
                do {
                    try self.scanner?.startScanning(resultBlock: { codes in
                        if let codes = codes {
                            let code = codes[0]
                            let stringValue = code.stringValue!
                            if stringValue != ""{
                                var finalCode: String = ""
                                var productId: Int32 = 0
                                var productWeightInKg: Double = 0.0
                                
                                if (self.getSpecificFirstCharactersInString(stringValue, 2) != "00" && self.getSpecificFirstCharactersInString(stringValue, 2) != "01")  {
                                    finalCode = self.getSpecificFirstCharactersInString(stringValue, 12)
                                    let productScanned = uiRealm.objects(ProductModel.self).filter("external_bar_code == '\(finalCode)' OR internal_bar_code == '\(finalCode)' ").first
                                    if let productName = productScanned?.name!{
                                        self.searchProductLabel.text = productName
                                        self.searchProductLabel.textColor = UIColor.black
                                    }
                                }else{
                                    finalCode = self.getSpecificCharactersInString(stringValue, 2, 5)
                                    if let id = Int32(finalCode) {
                                        productId = id
                                        let productScanned = uiRealm.objects(ProductModel.self).filter("id == \(productId)").first
                                        if let productName = productScanned?.name!{
                                            self.searchProductLabel.text = productName
                                            self.searchProductLabel.textColor = UIColor.black
                                        }
                                    }
                                    if let weightInGram = Int32(self.getSpecificCharactersInString(stringValue, 7, 5)){
                                        productWeightInKg = Double(weightInGram) / 1000.0
                                        self.amountField.text = "\(String(productWeightInKg)) KG"
                                    }
                                }
                                self.scanner?.stopScanning()
                                self.barcodeView.removeFromSuperview()
                            }
                        }
                    })
                } catch {
                    NSLog("Unable to start scanning")
                }
            } else {
                UIAlertView(title: Strings.SCANNING_UNAVAILABLE, message: Strings.SCANNING_UNAVAILABLE_MESSAGE, delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "Ok").show()
            }
        })
    }
    
    //get a number of first characters in a string
    func getSpecificFirstCharactersInString(_ string: String, _ offsetBy: Int) -> String{
        let firstCharacters = string.substring(to:string.index(string.startIndex, offsetBy: offsetBy))
        return firstCharacters
    }
    
    //get a number of n-th position range characters in a string
    func getSpecificCharactersInString(_ string: String, _ startIndex: Int, _ offsetBy: Int) -> String{
        return string[startIndex ..< startIndex + offsetBy]
    }

}
