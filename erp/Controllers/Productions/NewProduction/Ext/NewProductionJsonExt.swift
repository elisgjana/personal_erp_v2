//
//  NewProductionJsonExt.swift
//  erp
//
//  Created by Admin on 14/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension NewProductionVC{
    func createNewProduction(){
        var productionDetail = ProductionDetail()
        productionDetail.user_id = Int32(SavedVariables.getUserId())
        productionDetail.warehouse_id = selectedWarehouseId
        productionDetail.product_id = Int32(selectedProductId)
        productionDetail.quantity = productQuantity
        
        activityIndicator.startAnimating()
        getApiClient().fetchApiResults(urlString: Urls.CREATE_PRODUCTION, parameters: productionDetail , method: "POST", callback: {(response: GenericResponse<Production>)->()
            in
            self.performUIUpdatesOnMain {
                self.activityIndicator.stopAnimating()
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                self.presentSuccessPopup()
                self.emptyFields()
            }
        })
        
        self.activityIndicator.stopAnimating()
    }
}
