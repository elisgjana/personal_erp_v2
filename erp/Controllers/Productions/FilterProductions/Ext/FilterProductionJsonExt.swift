//
//  FilterProductionJsonExt.swift
//  erp
//
//  Created by Admin on 14/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension FilterProductionsVC{
    /**
     This method is used to filter Productions based on start date, end date and warehouses
     -Parameter: startDate : optional parameter
     -Parameter: endDate : optional parameter
     -Parameter: warehouse : optional parameter
     **/
    func getAllProductionsFromServer(_ startDate: String = "", _ endDate: String = "", _ warehouseId: Int = 0, _ shouldClose : Bool = false){
        var url = Urls.GET_ALL_FILTERED_PRODUCTIONS
        if startDate != ""{
            url += "&start_date=\(startDate)"
        }
        if endDate != ""{
            url += "&end_date=\(endDate)"
        }
        if warehouseId != 0{
            url += "&warehouse_id=\(warehouseId)"
        }
        
        getApiClient().fetchApiResults(urlString: url, parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<Production>)->()
            in
            self.performUIUpdatesOnMain {
                self.activityIndicator.stopAnimating()
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                if response.data.count > 0 {
                    setAllProductions(productions: response.data)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.PRODUCTION_ITEMS_UPDATED), object: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        })
        self.activityIndicator.stopAnimating()
    }
}
