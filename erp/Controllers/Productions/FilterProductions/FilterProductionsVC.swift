//
//  FilterProductionsVC.swift
//  erp
//
//  Created by Admin on 14/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class FilterProductionsVC: UIViewController , UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var calendarImage: UIImageView!
    @IBOutlet weak var startDateView: UIView!
    @IBOutlet weak var endDateView: UIView!
    @IBOutlet weak var warehouseView: UIView!
    @IBOutlet weak var warehouseImage: UIImageView!
    @IBOutlet weak var calendar2Image: UIImageView!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var warehouseLabel: UILabel!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    @IBOutlet weak var searchButton: UIButton!
    
    //I declare this variable in order to define which is tapped the start date or the end  date
    var selectedView = ""
    var blackView = UIView()
    var whiteView = UIView()
    var searchBar = UISearchBar()
    var tableView = UITableView()
    var warehouses = [WarehouseModel]()
    var warehousesCopy = [WarehouseModel]()
    var temp = 0
    var startDate = ""
    var endDate = ""
    var selectedWarehouseId = 0
    let calendarLauncher = CalendarLauncher()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeAppearanceComponents()
        initializeComponents()
        queryWarehouses()
        warehousesCopy = warehouses
        //Here I make the startDateView clickable
        let tapStartDate = UITapGestureRecognizer(target: self, action: #selector(FilterProductionsVC.startDateTapped(gesture:)))
        startDateView.addGestureRecognizer(tapStartDate)
        startDateView.isUserInteractionEnabled = true
        
        //Here I make the endDateView clickable
        let tapEndDate = UITapGestureRecognizer(target: self, action: #selector(FilterProductionsVC.endDateTapped(gesture:)))
        endDateView.addGestureRecognizer(tapEndDate)
        endDateView.isUserInteractionEnabled = true
        
        //Here I make the warehouseView clickable
        let tapWarehouse = UITapGestureRecognizer(target: self, action: #selector(FilterProductionsVC.warehouseTapped(gesture:)))
        warehouseView.addGestureRecognizer(tapWarehouse)
        warehouseView.isUserInteractionEnabled = true
    }
    
    func queryWarehouses(){
        for each in uiRealm.objects(WarehouseModel.self){
            self.warehouses.append(each)
        }
    }
    
    @objc func startDateTapped(gesture: UIGestureRecognizer) {
        
        if gesture.view != nil {
            selectedView = "start_date"
            calendarLauncher.showCalendar()
        }
    }
    
    @objc func endDateTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            selectedView = "end_date"
            calendarLauncher.showCalendar()

        }
    }
    
    @objc func warehouseTapped(gesture: UIGestureRecognizer) {
        if gesture.view != nil {
            handleSelectWarehouse()
        }
    }
    
    func initializeAppearanceComponents(){
        self.title = Strings.FILTER_PRODUCTIONS_TITLE
        self.navigationController?.navigationBar.tintColor = UIColor.white
        createRoundCorner(startDateView, 10)
        calendarImage.tintColor = UIColor.gray
        createRoundCorner(endDateView, 10)
        calendar2Image.tintColor = UIColor.gray
        createRoundCorner(warehouseView, 10)
        warehouseImage.tintColor = UIColor.gray
        warehouseLabel.textColor = UIColor.gray
        createRoundCorner(searchButton, 10)
        activityIndicator.color = createColorFromHex(hex: Colors.PRIMARY_DARK)
        activityIndicator.type = .ballPulse
    }
    
    func initializeComponents(){
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        let nib = UINib(nibName: "SearchWarehouseCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "searchWarehouseCell")
        tableView.rowHeight = 50
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.backgroundColor = UIColor.groupTableViewBackground
        let defaultWarehouse = uiRealm.objects(WarehouseModel.self).filter("warehouse_id = \(SavedVariables.getDefaultWarehouseId())").first
        if defaultWarehouse != nil{
            warehouseLabel.text = defaultWarehouse?.name!
            selectedWarehouseId = Int((defaultWarehouse?.warehouse_id.value)!)
        }else{
            warehouseLabel.text = Strings.CHOOSE
        }
    }
    
    @objc func stopActivityIndicatorInRejections(){
        self.activityIndicator.stopAnimating()
    }
    
    //Here I reload the table depending on the date filters
    @IBAction func searchButtonTUP(_ sender: Any) {
        activityIndicator.startAnimating()
        //here I simulate a delay in order to watch the activity indicator animation due to fast network request
        let when = DispatchTime.now() + 0.75 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Code with delay
            if self.startDateLabel.text! != Strings.FILTER_VC_CHOOSE{
                self.startDate = self.startDateLabel.text!
            }
            if self.endDateLabel.text! != Strings.FILTER_VC_CHOOSE{
                self.endDate = self.endDateLabel.text!
            }
            self.getAllProductionsFromServer(self.startDate, self.endDate, self.selectedWarehouseId, true)
        }
    }
    
    //Here I choose the value of start date and end date and then I make the validation
    @IBAction func chooseDateTUP(_ sender: Any) {
        let date = calendarLauncher.getSelectedDate()
        if selectedView == Strings.FILTER_VC_START_DATE{
            startDateLabel.text = date
        }else if selectedView == Strings.FILTER_VC_END_DATE{
            endDateLabel.text = date
        }
        selectedView = ""
        calendarLauncher.handleDismiss()
    }
}
