//
//  ProductionDetailsVC.swift
//  erp
//
//  Created by Admin on 14/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import PINRemoteImage

class ProductionDetailsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    var productionDetails = [ProductionDetail]()
    var refresher = UIRefreshControl()
    var warehouseId:Int = 0
    
    var indexToShow : Int = 0
    var indexToDelete = 0
    var uuid = ""
    let noItemsView = NoItemsFoundView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeAppearanceComponents()
        initializeComponents()
    }
    
    func initializeAppearanceComponents(){
        self.title = Strings.PRODUCTION_DETAILS_TITLE
        self.navigationController?.navigationBar.tintColor = UIColor.white;
    }
    
    func initializeComponents(){
        refresher.attributedTitle = NSAttributedString(string: Strings.PULL_TO_REFRESH_TITLE)
        refresher.addTarget(self, action: #selector(getProductionDetails), for: UIControlEvents.valueChanged)
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "RejectionDetailCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "rejectionDetailCell")
        tableView.rowHeight = 112
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.addSubview(refresher)
        showNoItemLogo(insetionView: tableView, noItemsView: noItemsView, itemsNumber: productionDetails.count)
    }
    
    @objc func getProductionDetails(){
        getProductionDetailsFromServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getProductionDetailsFromServer()
    }
    
    @IBAction func deleteProductionDetail(sender: UIButton){
        indexToDelete = sender.tag
        let id = getAllProductions()[indexToShow].details![sender.tag].id!
        deleteProductionDetailFromServer(id: id)
    }
}
