//
//  PDGetJsonExt.swift
//  erp
//
//  Created by Admin on 14/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation

extension ProductionDetailsVC{
    func getProductionDetailsFromServer(){
        CustomActivityIndicator.instance.showCustomActivityIndicator(view)
        getApiClient().fetchApiResults(urlString: Urls.GET_PRODUCTION_DETAILS + "warehouse_id=\(SavedVariables.getDefaultWarehouseId())", parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<Production>)->()
            in
            self.performUIUpdatesOnMain {
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                
                if response.data.count > 0 {
                    self.productionDetails.removeAll()
                    for item in response.data {
                        if item.details?.count != 0{
                            for detail in item.details!{
                                self.productionDetails.append(detail)
                            }
                        }
                    }
                    self.tableView.reloadData()
                }
                CustomActivityIndicator.instance.hideCustomActivityIndicator()
                self.refresher.endRefreshing()
                self.showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: self.productionDetails.count)
            }
        })
    }
}
