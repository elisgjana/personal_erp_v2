//
//  PDDeleteJsonExt.swift
//  erp
//
//  Created by Admin on 14/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation

extension ProductionDetailsVC{
    func deleteProductionDetailFromServer(id : Int32){
        CustomActivityIndicator.instance.showCustomActivityIndicator(view)
        getApiClient().fetchApiResults(urlString: Urls.DELETE_PRODUCTION_DETAIL + "\(id)", parameters: EmptyParamStruct(), method: "DELETE", callback: {(response: GenericResponse<String>)->()
            in
            self.performUIUpdatesOnMain {
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                
                if response.data.count > 0 {
                    self.productionDetails.remove(at: self.indexToDelete)
                    self.tableView.deleteRows(at: [IndexPath(item:self.indexToDelete, section:0)], with: .automatic)
                    self.tableView.reloadData()
                }
                CustomActivityIndicator.instance.hideCustomActivityIndicator()
                self.showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: self.productionDetails.count)
                self.presentSuccessPopup()
            }
        })
    }
}
