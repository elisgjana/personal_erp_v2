//
//  ProductionsVC.swift
//  erp
//
//  Created by Admin on 12/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class ProductionsVC: UIViewController, UITableViewDataSource, UITableViewDelegate{

    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var wasFiltering = false
    var productions = [Production]()
    var refresher = UIRefreshControl()
    var warehouseId : Int = 0
    let noItemsView = NoItemsFoundView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeAppearanceComponents()
        initializeComponents()
        registerToFilterItemsEvent()
        refresher = UIRefreshControl()
        refresher.attributedTitle = NSAttributedString(string: Strings.PULL_TO_REFRESH_TITLE)
        refresher.addTarget(self, action: #selector(pullToRefreshFunc), for: UIControlEvents.valueChanged)
        tableView.addSubview(refresher)
    }
    
    @objc func pullToRefreshFunc(){
        getProductionsFromServer()
    }
    
    //Create an event in order to make the table reload after data filter
    func registerToFilterItemsEvent(){
        NotificationCenter.default.addObserver(self, selector: #selector(productionItemsUpdated), name: NSNotification.Name(rawValue: Notifications.PRODUCTION_ITEMS_UPDATED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(apiCallFinished), name: NSNotification.Name(rawValue: Constants.REQUEST_RETURNED_ERROR), object: nil)
    }
    
    @objc func productionItemsUpdated(){
        wasFiltering = true
        productions = getAllProductions()
        self.tableView.reloadData()
    }
    
    func initializeAppearanceComponents(){
        navigationController?.navigationBar.tintColor = UIColor.white
        self.title = Strings.PRODUCTION_TITLE
        createRoundButton(button: filterButton)
    }
    
    func initializeComponents(){
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "RejectionCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "rejectionCell")
        tableView.rowHeight = 63
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (!wasFiltering){
            getProductionsFromServer()
        }
        showNoItemLogo(insetionView: tableView, noItemsView: noItemsView, itemsNumber: productions.count)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //when we say as? Create...VC we can access each object of that class
        if let detailsVC = segue.destination as? RejectionDetailsVC{
            if let indexPath = tableView.indexPathForSelectedRow{
                let selectedRow = indexPath.row
                detailsVC.indexToShow = selectedRow
                detailsVC.warehouseId = warehouseId
            }
        }
    }
}
