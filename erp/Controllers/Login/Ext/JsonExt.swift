//
//  LoginControllerExtension.swift
//  erp-ios
//
//  Created by apple on 12/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation
extension LoginVC{
    func constructParametersForLogin()->UserStruct{
        var user = UserStruct()
        user.email = emailField.text!
        user.password = passwordField.text!
        return user
    }
    
    //############################################## API CALL #####################################################
    
    func makeLoginRequest(){
        self.activityIndicator.startAnimating()
        SavedVariables.setUserPassword(password: passwordField.text!)
        let params = constructParametersForLogin()
        getApiClient().fetchApiResults(urlString: Urls.LOGIN_URL, parameters: params, method: "POST", callback: {(response: GenericResponse<UserStruct>)->()
            in
            self.performUIUpdatesOnMain {
                
                if response.status == 1{
                    self.presentWorngCredentialsError()
                    self.activityIndicator.stopAnimating()
                    return
                }
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    self.activityIndicator.stopAnimating()
                    return
                }
                SavedVariables.setUserEmail(userEmail: response.data[0].email!)
                SavedVariables.setUserFirstName(userFirstName: response.data[0].first_name!)
                SavedVariables.setUserLastName(userLastName: response.data[0].last_name!)
                SavedVariables.setUserId(userId: response.data[0].id!)
                SavedVariables.setDefaultWarehouseId(defaultWarehouseId: response.data[0].default_warehouse_id!)
                self.waitForNewAuthorizationKey()
            }
        })
    }
    
    func displayGeneralJSONErrorAndStopAnimating(){
        activityIndicator.stopAnimating()
        self.presentNoNetPopup()
    }
}



