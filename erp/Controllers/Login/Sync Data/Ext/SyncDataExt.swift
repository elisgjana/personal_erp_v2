//
//  SyncDataExt.swift
//  erp
//
//  Created by Admin on 09/08/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension SyncDataVC{
    func syncDownloadData(){
        getAppDelegate().failedModels.removeAll()
        currentProgress = 0
        let queue = DispatchQueue(label: "al.inovacion.erp.syncdata",attributes: .concurrent)
        queue.async {
            if AuthUser.has(PermissionTo.ACCESS_PRODUCTS){
                self.getProducts()
            }
            
            if AuthUser.has(PermissionTo.ACCESS_WISHLISTS){
                self.getWishLists()
            }
            
            if AuthUser.has(PermissionTo.ACCESS_WAREHOUSE_WISHLIST){
                self.getWarehouses()
            }
            
            if AuthUser.has(PermissionTo.ACCESS_PURCHASES){
                self.getPurchases()
            }
            
            if AuthUser.has(PermissionTo.ACCESS_SUPPLIERS){
                self.getSuppliers()
            }
            
            if AuthUser.has(PermissionTo.ACCESS_CLIENTS){
                self.getClients()
            }
        }
    }
    
    func getWishLists(){
        getApiClient().fetchApiResults(urlString: Urls.GET_USER_WISHLISTS, parameters: EmptyParamStruct(), method: "GET", model:"WishList", callback: {(response: GenericResponse<WishList>)->() in
            self.performUIUpdatesOnMain {
                if response.data.count > 0 {
                    setAllWishLists(wishLists:  response.data)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.WISHLISTS_RETRIEVED), object: nil)
                }
                self.updateItemsDownloaded()
            }
        })
    }
    
    func getProducts(){
        getApiClient().fetchApiResults(urlString: Urls.GET_ALL_PRODUCTS, parameters: EmptyParamStruct(), method: "GET", model:"Products", callback: {(response: GenericResponse<Product>)->() in
            self.performUIUpdatesOnMain {
                if response.data.count > 0{
                    deleteProductsFromRealm()
                    setAllProducts(products: response.data)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.PRODUCTS_RETRIEVED), object: nil)
                    storeProductsInRealm()
                }
                self.updateItemsDownloaded()
            }
        })
    }
    
    func getClients(){
        getApiClient().fetchApiResults(urlString: Urls.GET_ALL_CLIENTS, parameters: EmptyParamStruct(), method: "GET", model:"Clients", callback: {(response: GenericResponse<Client>)->() in
            self.performUIUpdatesOnMain {
                if response.data.count > 0{
                    deleteClientsFromRealm()
                    setAllClients(clients: response.data)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.CLIENTS_RETRIEVED), object: nil)
                    storeClientsInRealm()
                }
                self.updateItemsDownloaded()
            }
        })
    }
    
    func getWarehouses(){
        getApiClient().fetchApiResults(urlString: Urls.GET_ALL_USER_WAREHOUSES, parameters: EmptyParamStruct(), method: "GET", model:"Warehouses", callback: {(response: GenericResponse<Warehouse>)->() in
            self.performUIUpdatesOnMain {
                if response.data.count > 0{
                    deleteWarehousesFromRealm()
                    setAllWarehouses(warehouses:response.data)
                    storeWarehousesInRealm()
                }
                self.updateItemsDownloaded()
            }
        })
    }
    
    func getPurchases(){
        getApiClient().fetchApiResults(urlString: Urls.GET_USER_PURCHASES_URL, parameters: EmptyParamStruct(), method: "GET", model:"Purchase", callback: {(response: GenericResponse<Purchase>)->() in
            self.performUIUpdatesOnMain {
                if response.data.count > 0 {
                    deletePurchasesFromRealm()
                    setAllPurchases(purchases: response.data)
                    storePurchasesInRealm()
                }
                self.updateItemsDownloaded()
            }
        })
    }
    
    func getSuppliers(){
        getApiClient().fetchApiResults(urlString: Urls.GET_ALL_SUPPLIERS, parameters: EmptyParamStruct(), method: "GET", model:"Supplier", callback: {(response: GenericResponse<Supplier>)->() in
            self.performUIUpdatesOnMain {
                if response.data.count > 0{
                    deleteSuppliersFromRealm()
                    setAllSuppliers(suppliers: response.data)
                    storeSuppliersInRealm()
                }
                self.updateItemsDownloaded()
            }
        })
    }
    
    func registerToEvents(){
        NotificationCenter.default.addObserver(self, selector: #selector(updateItemsDownloaded), name: NSNotification.Name(rawValue: Constants.REQUEST_RETURNED_ERROR), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenOnSync), name: NSNotification.Name(rawValue: Notifications.REFRESH_TOKEN_ON_SYNC), object: nil)
    }
    
    func unregisterFromEvents(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.REQUEST_RETURNED_ERROR), object: nil)
    }
    
    @objc func updateItemsDownloaded(){
        let totalNumberOfItems = TotalItemsToSync.instance.numberOfItems()
        
        performUIUpdatesOnMain {
            self.currentProgress += 1
            self.calculatedProgress = Double(self.currentProgress) / Double(totalNumberOfItems)
            self.customProgressBar.setProgressValue(currentValue: self.currentProgress, totalValue: totalNumberOfItems)
            
            if self.currentProgress == TotalItemsToSync.instance.numberOfItems() && getFailedModels().count > 0 {
                self.customProgressBar.retryButton.isHidden = false
                self.customProgressBar.titleLabel.text = Strings.ERROR_OCCURED_AT + " :"
                for item in getFailedModels(){
                    self.customProgressBar.titleLabel.text = self.customProgressBar.titleLabel.text! + item + ", "
                }
                self.customProgressBar.retryButton.addTarget(self, action: #selector(self.handleRetryDownloadingData), for: .touchUpInside)
            }
            else if self.currentProgress == totalNumberOfItems && getFailedModels().count == 0 {
                self.customProgressBar.dismissView()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func handleRetryDownloadingData(){

    }
    
    @objc func refreshTokenOnSync(){
        //        refreshButton.isHidden = false
    }
}
