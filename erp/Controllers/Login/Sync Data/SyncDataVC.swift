//
//  SyncDataVC.swift
//  erp
//
//  Created by Akil Rajdho on 14/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import RealmSwift

class SyncDataVC: UIViewController {
    
    let customProgressBar = CustomProgressBar()
    
    
    var calculatedProgress = 0.0
    var currentProgress = 0
    let totalNumberOfItems = TotalItemsToSync.instance.numberOfItems()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customProgressBar.showCustomProgressBar()
        registerToEvents()
        initializeComponents()
        syncDownloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        unregisterFromEvents()
    }
    
    @IBAction func btnDoneTUP(_ sender: Any) {
        if getFailedModels().count == 0 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.HIDE_DARK_VIEW), object: nil)
            self.dismiss(animated: true, completion: nil)
            return
        }
        syncDownloadData()
    
    }
    
    func initializeComponents(){
        SavedVariables.setIsSync(isSync: false)
    }
    
    @IBAction func refreshButtonTUP(_ sender: Any) {
        syncDownloadData()
    }
}
