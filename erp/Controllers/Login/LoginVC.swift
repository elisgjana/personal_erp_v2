//
//  LoginViewController.swift
//  erp-ios
//
//  Created by Elis Gjana on 22/01/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class LoginVC: UIViewController {

    @IBOutlet weak var passwordIcon: UIImageView!
    @IBOutlet weak var emailIcon: UIImageView!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    @IBOutlet weak var passwordField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        registerToEvents()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        unregisterFromEvents()
    }
    
    func registerToEvents(){
        NotificationCenter.default.addObserver(self, selector: #selector(resetUI), name: NSNotification.Name(rawValue: Notifications.REQUEST_RETURNED_ERROR), object: nil)
        
            NotificationCenter.default.addObserver(self, selector: #selector(logIn), name: NSNotification.Name(rawValue: Notifications.PERMISSIONS_RETRIEVED), object: nil)
    }
    
    @objc func logIn(){
        unregisterFromEvents()
        self.performUIUpdatesOnMain {
            SavedVariables.setIsSync(isSync: true)
            self.performSegue(withIdentifier: "login", sender: nil)
        }
    }
    
    func unregisterFromEvents(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Notifications.REQUEST_RETURNED_ERROR), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Notifications.PERMISSIONS_RETRIEVED), object: nil)
    }
    
    @objc func resetUI(){
        performUIUpdatesOnMain {
            self.activityIndicator.stopAnimating()
            self.presentApiErrorPopup()
        }
    }

    func initializeComponents(){
        hideKeyboardOnViewTap()
        
        loginButton.layer.backgroundColor = createColorFromHex(hex: Colors.PRIMARY_LIGHT).cgColor
        containerView.layer.backgroundColor = createColorFromHex(hex: Colors.PRIMARY_DARK).cgColor
        emailField.text = "developers@inovacion.al"
        passwordField.text = "invDevelopers2017&"
        createRoundCorner(formView, 10)
        createRoundCorner(loginButton, 10)
        
        activityIndicator.color = UIColor.white
        activityIndicator.type = .ballSpinFadeLoader
        
        emailField.textColor = UIColor.white
        passwordField.textColor = UIColor.white
        passwordField.isSecureTextEntry = true
        
        emailIcon.tintColor = UIColor.white
        passwordIcon.tintColor = UIColor.white
    }

    @IBAction func loginTUP(_ sender: Any) {
        SavedVariables.setSyncNeeded(syncNeeded: true)
        if !validateInput(){
            presentFieldsMissingPopup()
            return
        }
        makeLoginRequest()
    }
    
    func validateInput()->Bool{
        if emailField.text?.trim().count == 0 || passwordField.text?.trim().count == 0 || !validateEmail(email:emailField.text!){
            displayAlert(title: "ERROR", message: Strings.PLEASE_FILL_LOGIN_DATA)
            return false
        }
        return true
    }
    
}
