//
//  OrdersTableViewExt.swift
//  erp
//
//  Created by Admin on 13/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension OrderItemsVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderItemsCopy.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderItemCell", for: indexPath) as! OrderItemCell
        
        let requiredQuantity = orderItemsCopy[indexPath.row].required_quantity ?? 0.0
        cell.requiredQuantityLabel.text = "\(String(requiredQuantity)) \(String(describing: orderItemsCopy[indexPath.row].measurement_unit!))"
        
        
        let productName = orderItemsCopy[indexPath.row].product_name ?? ""
        cell.productNameLabel.text = "\(String(productName))"
        
        
        let purchasedQuantity = orderItemsCopy[indexPath.row].purchased_quantity ?? 0.0
        cell.purchasedQuantityLabel.text = "\(String(purchasedQuantity)) \(String(describing: orderItemsCopy[indexPath.row].measurement_unit!))"
        
        
        let lastPrice = orderItemsCopy[indexPath.row].last_purchased_price ?? 0.0
        cell.lastPriceLabel.text = "\(String(lastPrice)) ALL"
        
        let recommendedPrice = orderItemsCopy[indexPath.row].recommended_price ?? 0.0
        cell.recommendedPriceLabel.text = "\(String(recommendedPrice)) ALL"
        
        //here I change the image of the product using URL
        if let imgUrl = orderItemsCopy[indexPath.row].product_img_path{
            let imageUrl = "\(Urls.BASE_URL)\(imgUrl)"
            cell.productImage.pin_setImage(from: URL(string: imageUrl))
            if cell.productImage.image == nil {
                cell.productImage.image = UIImage(named: "default_product_image") //default product image when no image on server
            }
        }
        styleOrderItemTable(cell,indexPath)
        
        return cell
    }
}
