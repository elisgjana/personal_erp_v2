//
//  OrderItemsSearchBarExt.swift
//  erp
//
//  Created by Admin on 14/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension OrderItemsVC{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        orderItemsCopy = orderItems.filter({ order -> Bool in
            guard !searchText.isEmpty else {
                tableView.reloadData()
                return true
            }
            return  (order.product_name?.uppercased().contains(searchText.uppercased()))!
        })
        tableView.reloadData()
        self.showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: orderItems.count)
    }
}
