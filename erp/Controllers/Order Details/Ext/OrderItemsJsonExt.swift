//
//  OrderItemsJsonExt.swift
//  erp
//
//  Created by Admin on 13/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension OrderItemsVC{
    func getOrderItemsFromServer(startDate:String, endDate:String, warehouseId:Int32){
        CustomActivityIndicator.instance.showCustomActivityIndicator(view)
        var url = ""
        if warehouseId != -1{
            url = Urls.GET_ORDER_ITEMS + "&start_date=\(startDate)&end_date=\(endDate)&warehouse_id=\(warehouseId)"
        }else{
            url = Urls.GET_ORDER_ITEMS + "&start_date=\(startDate)&end_date=\(endDate)"
        }
        
        getApiClient().fetchApiResults(urlString: url, parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<OrderItem>)->()
            in
            self.performUIUpdatesOnMain {
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                if response.data.count > 0 {
                    setAllOrderItems(orderItems: response.data)
                    self.orderItems = getAllOrderItems()
                    print(warehouseId)
                    self.orderItemsCopy = self.orderItems
                    self.tableView.reloadData()
                }
                CustomActivityIndicator.instance.hideCustomActivityIndicator()
                self.showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: self.orderItems.count)
                self.refresher.endRefreshing()
            }
        })
    }
}
