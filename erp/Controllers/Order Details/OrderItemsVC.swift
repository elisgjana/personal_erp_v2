//
//  OrderItemsVC.swift
//  erp
//
//  Created by Admin on 13/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import PINRemoteImage


class OrderItemsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate{

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var purchaseButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var warehouseId = 0
    var temp = 0
    var orderItems = [OrderItem]()
    var orderItemsCopy = [OrderItem]()
    var refresher = UIRefreshControl()
    
    let noItemsView = NoItemsFoundView()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        initializeAppearanceComponents()
        initializeComponents()
        refresher = UIRefreshControl()
        refresher.attributedTitle = NSAttributedString(string: Strings.PULL_TO_REFRESH_TITLE)
        refresher.addTarget(self, action: #selector(pullToRefreshFunc), for: UIControlEvents.valueChanged)
        tableView.addSubview(refresher)
    }
    
    @objc func pullToRefreshFunc(){
        getOrderItemsFromServer(startDate:filterEndDate, endDate: filterEndDate, warehouseId: Int32(warehouseId))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getOrderItemsFromServer(startDate:filterEndDate, endDate: filterEndDate, warehouseId: Int32(warehouseId))
    }
    
    func initializeAppearanceComponents(){
        self.title = Strings.ORDER_ITEMS_TITLE
        purchaseButton.tintColor = UIColor.white
    }
    
    func initializeComponents(){
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        let nib = UINib(nibName: "OrderItemCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "orderItemCell")
        tableView.rowHeight = 119
    }

    @IBAction func purchaseButtonTUP(_ sender: Any) {
        orderItemForPurchase = orderItems
        let purchasesStoryboard = UIStoryboard(name: "Purchases", bundle: nil)
        let secondViewController = purchasesStoryboard.instantiateViewController(withIdentifier: "NewPurchaseVC") as! NewPurchaseVC
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
}
