//
//  FilterOrdersJsonExt.swift
//  erp
//
//  Created by Admin on 13/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension FilterOrdersVC{
    /**
     This method is used to filter Orders based on start date, end date and warehouse
     -Parameter: startDate : optional parameter
     -Parameter: endDate : optional parameter
     -Parameter: warehouseId : optional parameter
     **/
    
    func getOrdersFromServer(_ startDate: String = "", _ endDate: String = "", _ warehouseId: Int = 0, _ shouldClose : Bool = false){
        var url = Urls.GET_ALL_ORDERS
        if startDate != ""{
            url += "&start_date=\(startDate)"
        }
        if endDate != ""{
            url += "&end_date=\(endDate)"
        }
        if warehouseId != 0{
            url += "&warehouse_id=\(warehouseId)"
        }
        
        getApiClient().fetchApiResults(urlString: url, parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<Order>)->()
            in
            self.performUIUpdatesOnMain {
                self.activityIndicator.stopAnimating()
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                if response.data.count > 0 {
                    setAllOrders(orders: response.data)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.ORDER_ITEMS_FILTERED), object: nil)
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
        })
        self.activityIndicator.stopAnimating()
    }
}
