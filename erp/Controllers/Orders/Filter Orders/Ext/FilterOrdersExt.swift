//
//  FilterOrdersExt.swift
//  erp
//
//  Created by Admin on 13/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension FilterOrdersVC{
    @objc func handleSelectWarehouse(){
        if let window = UIApplication.shared.keyWindow{
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideBlackView)))
            blackView.backgroundColor = UIColor(white:0, alpha:0.5)
            whiteView.backgroundColor = UIColor.white
            
            searchBar.searchBarStyle = .minimal
            searchBar.placeholder = Strings.WAREHOUSE_SEARCHBAR_PLACEHOLDER
            
            window.addSubview(blackView)
            window.addSubview(whiteView)
            
            let height = Int(window.frame.height) - 55
            let y = Int(window.frame.height) - height
            whiteView.frame = CGRect(x: 0, y: Int(window.frame.height), width: Int(window.frame.width), height: height)
            
            whiteView.addSubview(tableView)
            whiteView.addSubview(searchBar)
            
            blackView.frame = window.frame
            blackView.alpha = 0
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping :1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.whiteView.frame = CGRect(x: 0, y: y, width: Int(window.frame.width), height: height)
                self.searchBar.addContstraintsRelatedToSuperview(leading: 0, trailing: 0, top: 5, bottom: Int(self.whiteView.frame.height) - 44)
                self.tableView.addContstraintsRelatedToSuperview(leading: 0, trailing: 0, top: 44, bottom: 0)
                
            },completion : nil)
        }
    }
    @objc func hideBlackView(){
        self.handleTapOutsideDismiss(blackView: self.blackView, whiteView:self.whiteView)
    }
}
