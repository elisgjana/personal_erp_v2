//
//  TableView.swift
//  erp
//
//  Created by Akil Rajdho on 14/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension OrdersVC {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return orders.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "orderItems", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //when we say as? Create...VC we can access each object of that class
        if let orderItemsVC = segue.destination as? OrderItemsVC {
            if let indexPath = tableView.indexPathForSelectedRow{
                let selectedRow = indexPath.row
                orderItemsVC.warehouseId = self.orders[selectedRow].warehouse_id!
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell", for: indexPath) as! OrderCell
        let order = orders[indexPath.row]
        cell.warehouseNameLabel.text = order.warehouse_name ?? "-"
        cell.dateLabel.text = transfromDateToLongDate(date: Device.getCurrentDate() )
        cell.numberOfArticles.text = String(describing: order.total_items ?? 0)
        cell.tintView.alpha = 0
        if orders[indexPath.row].warehouse_id == -1{
            cell.tintView.alpha = 0.4
        }
        styleOrdersTable(cell,indexPath)
        return cell
    }
   
}

