//
//  OrdersJsonExt.swift
//  erp
//
//  Created by Akil Rajdho on 27/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation

extension OrdersVC{
    func getOrdersFromServer(){
        CustomActivityIndicator.instance.showCustomActivityIndicator(view)
        let url = "&start_date=\(Device.getCurrentDate())&end_date=\(Device.getCurrentDate())"
        getApiClient().fetchApiResults(urlString: Urls.GET_ALL_ORDERS+url, parameters: EmptyParamStruct(), method: "GET", callback: {(response: GenericResponse<Order>)->()
            in
            self.performUIUpdatesOnMain {
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                if response.data.count > 0 {
                    setAllOrders(orders:  response.data)
                    self.orders = getAllOrders()
                    self.tableView.reloadData()
                }
                CustomActivityIndicator.instance.hideCustomActivityIndicator()
                self.refresher.endRefreshing()
                self.showNoItemLogo(insetionView: self.tableView, noItemsView: self.noItemsView, itemsNumber: self.orders.count)
            }
        })
    }
}

