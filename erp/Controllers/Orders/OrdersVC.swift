//
//  WishListViewController.swift
//  erp-ios
//
//  Created by apple on 07/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import UIKit
import CoreLocation
class OrdersVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    
    let noItemsView = NoItemsFoundView()
    var orders = [Order]()
    var refresher = UIRefreshControl()
    var wasFiltering = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeAppearanceComponents()
        initializeComponents()
        refresher.attributedTitle = NSAttributedString(string: Strings.PULL_TO_REFRESH_TITLE)
        refresher.addTarget(self, action: #selector(pullToRefreshFunc), for: UIControlEvents.valueChanged)
        tableView.addSubview(refresher)
        registerToFilterItemsEvent()
    }
    @objc func pullToRefreshFunc(){
        wasFiltering = false
        getOrdersFromServer()
    }

    override func viewWillAppear(_ animated: Bool) {
        registerToEvents()
        wasFiltering = false
        getOrdersFromServer()
        showNoItemLogo(insetionView: self.tableView, noItemsView: noItemsView, itemsNumber: orders.count)
    }
    //Create an event in order to make the table reload after data filter
    func registerToFilterItemsEvent(){
        NotificationCenter.default.addObserver(self, selector: #selector(orderItemsFiltered), name: NSNotification.Name(rawValue: Notifications.ORDER_ITEMS_FILTERED), object: nil)
    }
    
    @objc func orderItemsFiltered(){
        wasFiltering = true
        orders = getAllOrders()
        self.tableView.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        unregisterFromEvents()
    }
    @objc func wishListWarehouseItemsUpdated(){
        wasFiltering = true
        orders = getAllOrders()
        self.tableView.reloadData()
    }

    func initializeAppearanceComponents(){
        self.title = Strings.ORDERS_TITLE
        self.navigationController?.navigationBar.tintColor = UIColor.white
        showNoItemLogo(insetionView: self.tableView, noItemsView: noItemsView, itemsNumber: orders.count)
    }
    func initializeComponents(){
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: "OrderCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "orderCell")
        tableView.rowHeight = 66

    }

    func registerToEvents(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(resetUI), name: NSNotification.Name(rawValue: Notifications.REQUEST_RETURNED_ERROR), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(notifyTokkenExpired), name: NSNotification.Name(rawValue: Notifications.TOKEN_EXPIRED), object: nil)
    }
    
    func unregisterFromEvents(){
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Notifications.REQUEST_RETURNED_ERROR), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Notifications.TOKEN_EXPIRED), object: nil)
    }
    
    @objc func resetUI(){
        performUIUpdatesOnMain {
            self.refresher.endRefreshing()
            self.presentApiErrorPopup()
        }
    }
    
    @objc func notifyTokkenExpired(){
        performUIUpdatesOnMain {
            self.refresher.endRefreshing()
            self.presentTokenExpiredPopup()
        }
    }

}

