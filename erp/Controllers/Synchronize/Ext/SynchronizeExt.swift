//
//  SynchronizeExt.swift
//  erp
//
//  Created by Admin on 20/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension SynchronizeVC{
    func syncUploadedData(){
        for purchase in purchases{
            var newPurchase = Purchase()
            newPurchase.supplier_uuid = purchase.supplier_uuid
            newPurchase.supplier_name = purchase.supplier_name
            newPurchase.date = String(describing: purchase.date!)
            newPurchase.user_id = purchase.user_id.value
            newPurchase.uuid = purchase.uuid
            newPurchase.value = purchase.value.value
            newPurchase.warehouse_id = purchase.warehouse_id.value
            newPurchase.invoice_no = purchase.invoice_no
            newPurchase.serial_no = purchase.serial_no
            newPurchase.discount = purchase.discount.value
            newPurchase.delivered = purchase.delivered.value
            newPurchase.latitude = purchase.latitude.value
            newPurchase.longitude = purchase.longitude.value
            newPurchase.debt = 0.0
            newPurchase.notes = purchase.notes
            newPurchase.payment_method = purchase.payment_method
            newPurchase.payment_value = purchase.payment_value.value
            newPurchase.status_id = purchase.status_id.value
            
            var allDetails : [Detail] = []
            for element in purchase.details{
                var purchaseDetail = Detail()
                purchaseDetail.product_id = element.product_id.value
                purchaseDetail.product_name = element.product_name
                purchaseDetail.amount = element.amount.value
                purchaseDetail.product_price = element.product_price.value
                purchaseDetail.discount = element.discount.value
                purchaseDetail.total = element.total.value
                allDetails.append(purchaseDetail)
            }
            
            var allPayments : [Payment] = []
            for element in purchase.payments{
                var purchasePayment = Payment()
                purchasePayment.payment_method = element.payment_method
                purchasePayment.amount = element.amount.value
                purchasePayment.date = element.date
                purchasePayment.user_id = Int32(SavedVariables.getUserId())
                allPayments.append(purchasePayment)
            }
            newPurchase.details = allDetails
            newPurchase.payments = allPayments
            
            getApiClient().fetchApiResults(urlString: Urls.CREATE_PURCHASE_URL, parameters: newPurchase, method: "POST", callback: {(response: GenericResponse<Purchase>)->()
                in
                self.performUIUpdatesOnMain {
                    if response.status != 0 {
                        self.presentApiErrorPopup(statusCode: response.status!)
                        return
                    }
                    if response.data.count > 0 {
                        updatePurchase(object: purchase, value: true, forKey: "is_sync")
                        self.uploadPurchaseImage(purchaseUuid: newPurchase.uuid!)
                    }
                    if !self.isBackgroundSync{
                        self.updateItemsUploaded()
                    }else{
                        self.isBackgroundSync = false
                    }
                }
            })
        }
    }
    
    func uploadPurchaseImage(purchaseUuid: String){
        let currentPurchaseImageModel = uiRealm.objects(PurchaseImageModel.self).filter("uuid = '\(purchaseUuid)'").first
        
        var newPurchaseImage = PurchaseImage()
        newPurchaseImage.photo = currentPurchaseImageModel?.photo
        newPurchaseImage.uuid = currentPurchaseImageModel?.uuid
        
        getApiClient().fetchApiResults(urlString: Urls.CREATE_PURCHASE_IMAGE, parameters: newPurchaseImage, method: "POST", callback: {(response: GenericResponse<PurchaseImage>)->()
            in
            self.performUIUpdatesOnMain {
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                if response.data.count > 0 {
                    print("Fotoja u uplodua me sukses")
                }else{
                    self.presentApiErrorPopup()
                }
            }
        })
    }
    
    func syncDownloadData(){
        let queue = DispatchQueue(label: "al.inovacion.erp.syncdata",attributes: .concurrent)
        queue.async {
            if AuthUser.has(PermissionTo.ACCESS_PRODUCTS){
                self.getProducts()
            }
            
            if AuthUser.has(PermissionTo.ACCESS_WISHLISTS){
                self.getWishLists()
            }
            
            if AuthUser.has(PermissionTo.ACCESS_WAREHOUSE_WISHLIST){
                self.getWarehouses()
            }
            
            if AuthUser.has(PermissionTo.ACCESS_PURCHASES){
                self.getPurchases()
            }
            
            if AuthUser.has(PermissionTo.ACCESS_SUPPLIERS){
                self.getSuppliers()
            }
            
            if AuthUser.has(PermissionTo.ACCESS_CLIENTS){
                self.getClients()
            }
        }
        self.customProgressBar.titleLabel.text = Strings.SYNCRONIZING_DATA
    }
    
    
    func updateItemsUploaded(){
        performUIUpdatesOnMain {
            self.currentProgress += 1
            self.customProgressBar.setProgressValue(currentValue: self.currentProgress, totalValue: self.purchases.count)
            
            if self.currentProgress == self.purchases.count && getFailedModels().count > 0 {
                self.customProgressBar.retryButton.isHidden = false
                self.customProgressBar.titleLabel.text = Strings.ERROR_OCCURED_AT + " :"
                for item in getFailedModels(){
                    self.customProgressBar.titleLabel.text =  self.customProgressBar.titleLabel.text! + item + ", "
                }
            }
            else if self.currentProgress == self.purchases.count && getFailedModels().count == 0 {
                self.customProgressBar.dismissView()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @objc func updateItemsDownloaded(){
        performUIUpdatesOnMain {
            self.currentProgress += 1
            self.customProgressBar.setProgressValue(currentValue: self.currentProgress, totalValue: self.totalNumberOfItems)
            
            if self.currentProgress == self.totalNumberOfItems && getFailedModels().count > 0 {
                self.customProgressBar.retryButton.isHidden = false
                self.customProgressBar.titleLabel.text = Strings.ERROR_OCCURED_AT + " :"
                for item in getFailedModels(){
                    self.customProgressBar.titleLabel.text =  self.customProgressBar.titleLabel.text! + item + ", "
                }
            }
            else if self.currentProgress == self.totalNumberOfItems && getFailedModels().count == 0 {
                self.customProgressBar.dismissView()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func getWishLists(){
        getApiClient().fetchApiResults(urlString: Urls.GET_USER_WISHLISTS, parameters: EmptyParamStruct(), method: "GET", model:"WishList", callback: {(response: GenericResponse<WishList>)->() in
            self.performUIUpdatesOnMain {
                if response.data.count > 0 {
                    setAllWishLists(wishLists:  response.data)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.WISHLISTS_RETRIEVED), object: nil)
                }
                self.updateItemsDownloaded()
            }
        })
    }
    
    func getProducts(){
        getApiClient().fetchApiResults(urlString: Urls.GET_ALL_PRODUCTS, parameters: EmptyParamStruct(), method: "GET", model:"Products", callback: {(response: GenericResponse<Product>)->() in
            self.performUIUpdatesOnMain {
                if response.data.count > 0 {
                    deleteProductsFromRealm()
                    setAllProducts(products: response.data)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.PRODUCTS_RETRIEVED), object: nil)
                    storeProductsInRealm()
                }
                self.updateItemsDownloaded()
            }
        })
    }
    
    func getClients(){
        getApiClient().fetchApiResults(urlString: Urls.GET_ALL_CLIENTS, parameters: EmptyParamStruct(), method: "GET", model:"Clients", callback: {(response: GenericResponse<Client>)->() in
            self.performUIUpdatesOnMain {
                if response.data.count > 0 {
                    deleteClientsFromRealm()
                    setAllClients(clients: response.data)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.CLIENTS_RETRIEVED), object: nil)
                    storeClientsInRealm()
                }
                self.updateItemsDownloaded()
            }
        })
    }
    
    func getWarehouses(){
        getApiClient().fetchApiResults(urlString: Urls.GET_ALL_USER_WAREHOUSES, parameters: EmptyParamStruct(), method: "GET", model:"Warehouses", callback: {(response: GenericResponse<Warehouse>)->() in
            self.performUIUpdatesOnMain {
                if response.data.count > 0 {
                    deleteWarehousesFromRealm()
                    setAllWarehouses(warehouses:response.data)
                    storeWarehousesInRealm()
                }
                self.updateItemsDownloaded()
            }
        })
    }
    
    func getPurchases(){
        getApiClient().fetchApiResults(urlString: Urls.GET_USER_PURCHASES_URL, parameters: EmptyParamStruct(), method: "GET", model:"Purchase", callback: {(response: GenericResponse<Purchase>)->() in
            self.performUIUpdatesOnMain {
                if response.data.count > 0 {
                    deletePurchasesFromRealm()
                    setAllPurchases(purchases: response.data)
                    storePurchasesInRealm()
                }
                self.updateItemsDownloaded()
            }
        })
    }
    
    func getSuppliers(){
        getApiClient().fetchApiResults(urlString: Urls.GET_ALL_SUPPLIERS, parameters: EmptyParamStruct(), method: "GET", model:"Supplier", callback: {(response: GenericResponse<Supplier>)->() in
            self.performUIUpdatesOnMain {
                if response.data.count > 0 {
                    deleteSuppliersFromRealm()
                    setAllSuppliers(suppliers: response.data)
                    storeSuppliersInRealm()
                }
                self.updateItemsDownloaded()
            }
        })
    }
}
