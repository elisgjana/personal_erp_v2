//
//  SynchronizeVC.swift
//  erp
//
//  Created by Admin on 17/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class SynchronizeVC: UIViewController {

    var date: Date?
    var purchases = [PurchaseModel]()
    var calculatedProgress = 0.0
    var currentProgress = 0
    var isBackgroundSync = false
    let totalNumberOfItems = TotalItemsToSync.instance.numberOfItems()
    let customProgressBar = CustomProgressBar()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerToEvents()
    }
    
    func queryPurchases(){
        let items = uiRealm.objects(PurchaseModel.self).filter("is_sync = false")
        for item in items{
            self.purchases.append(item)
        }
    }
    
    func registerToEvents(){
        NotificationCenter.default.addObserver(self, selector: #selector(backgroundSynchronizeData), name: NSNotification.Name("color"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTokenOnSync), name: NSNotification.Name(rawValue: Notifications.REFRESH_TOKEN_ON_SYNC), object: nil)
    }
    
    @objc  func backgroundSynchronizeData(){
        //Code For synchronization
        isBackgroundSync = true
        syncData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        customProgressBar.showCustomProgressBar()
        queryPurchases()
        syncData()
    }
    
    func syncData(){
        getAppDelegate().failedModels.removeAll()
        currentProgress = 0
        
        if purchases.count != 0{
            syncUploadedData()
        }else{
            syncDownloadData()
        }
    }
    
  
    @IBAction func btnDoneTUP(_ sender: Any) {
        self.queryPurchases()
        let purchases = uiRealm.objects(PurchaseModel.self).filter("is_sync = false")
        
        if purchases.count == 0 {
            navigationController?.popViewController(animated: true)
            return
        }
    }
    
    @objc func refreshTokenOnSync(){
        self.customProgressBar.retryButton.isHidden = false
        self.customProgressBar.retryButton.addTarget(self, action: #selector(handleRefresh), for: .touchUpInside)
    }
    
    @objc func handleRefresh(){
        if purchases.count != 0{
            syncUploadedData()
        }else{
            syncDownloadData()
        }
    }
}
