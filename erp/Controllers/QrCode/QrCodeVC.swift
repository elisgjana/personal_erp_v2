//
//  QrCodeVC.swift
//  erp
//
//  Created by Admin on 30/06/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class QrCodeVC: UIViewController {
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var qrCodeBox: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Strings.USER_CODE
        initializeComponents()
    }
    
    func initializeComponents(){
        fullNameLabel.text = "\(SavedVariables.getUserFirstName())"
        let myQRimage = createQRFromString("client=\(SavedVariables.getUserUuid())",size: qrCodeBox.frame.size) 
        qrCodeBox.image = myQRimage
    }
}
