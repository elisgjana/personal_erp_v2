//
//  ClientsTableViewExt.swift
//  erp
//
//  Created by Admin on 15/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

extension ClientsVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clientsCopy.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "clientCell", for: indexPath) as! ClientCell
        
        let currentContact = clientsCopy[indexPath.row]
        
        var subjectName = currentContact.name ?? "-"
        if subjectName == ""{
            subjectName = "-"
        }
        cell.subjectNameLabel.text = subjectName
        
        var contactName = currentContact.contact_name ?? "-"
        if contactName == ""{
            contactName = "-"
        }
        cell.contactNameLabel.text = contactName
        
        var nuis = currentContact.nuis ?? "-"
        if nuis == ""{
            nuis = "-"
        }
        cell.nuisLabel.text = nuis
        
        var city = currentContact.city ?? "="
        if city == ""{
            city = "-"
        }
        cell.cityLabel.text = city
        
        var tel = currentContact.mobile_phone ?? "-"
        if tel == ""{
            tel = "-"
        }
        cell.telLabel.text = tel
        
        styleClientDetailTable(cell, indexPath)
        
        cell.callButton.addTarget(self, action: #selector(callFunc), for: UIControlEvents.touchUpInside)
        cell.callButton.tag = indexPath.row
        cell.directionButton.addTarget(self, action: #selector(getDirections), for: UIControlEvents.touchUpInside)
        cell.directionButton.tag = indexPath.row
        
        return cell
    }
}
