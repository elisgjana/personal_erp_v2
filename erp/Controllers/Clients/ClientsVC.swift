//
//  ClientsVC.swift
//  erp
//
//  Created by Admin on 14/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit
import RealmSwift
import CoreLocation


class ClientsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var temp = 0
    var allClients = [ClientModel]()
    var clientsCopy = [ClientModel]()
    var currentLatitude = 0.0
    var currentLongitude = 0.0
    var currentLocation: CLLocation!
    var locManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeComponents()
        query_clients()
        clientsCopy = allClients
        locManager.requestWhenInUseAuthorization()
        if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            currentLocation = locManager.location
            currentLongitude = currentLocation.coordinate.longitude
            currentLatitude = currentLocation.coordinate.latitude
        }
    }
    
    func query_clients(){
        for each in uiRealm.objects(ClientModel.self){
            self.allClients.append(each)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.title = Strings.CLIENTS_TITLE
        clientsCopy = allClients
    }

    func initializeComponents(){
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        let nib = UINib(nibName: "ClientCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "clientCell")
        tableView.rowHeight = 161
    }
    
    @IBAction func callFunc(sender: UIButton){
        if let telNumber = clientsCopy[sender.tag].mobile_phone{
            if telNumber != ""{
                makeAPhoneCall(number: telNumber)
            }else{
                presentWrongNumber()
            }
        }else{
            presentWrongNumber()
        }
    }
    
    func makeAPhoneCall(number: String)  {
        let nr =  number.components(separatedBy: .whitespaces).joined()
        if let url = URL(string: "tel://\(nr)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func getDirections(sender: UIButton){
        openGoogleMapsNavigation(sLatitude: currentLatitude, sLongitude: currentLongitude, eLatitude: clientsCopy[sender.tag].latitude.value!, eLongitude: clientsCopy[sender.tag].longitude.value!)
    }

    func openGoogleMapsNavigation(sLatitude:Double,sLongitude:Double,eLatitude:Double,eLongitude:Double){
        //Working in Swift new versions.
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
        {
            let url = URL(string: "comgooglemaps://?saddr=\(sLatitude),\(sLongitude)&daddr=\(eLatitude),\(eLongitude)&directionsmode=driving")
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        }
        else
        {
            displayAlert(title: "Google Maps", message: "Ju lutem instaloni programin Google Maps ")
        }
    }
    
}
