//
//  OrdersStruct.swift
//  erp
//
//  Created by Akil Rajdho on 03/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation

struct Order: Codable{
    var warehouse_id: Int? = 0
    var warehouse_name: String? = ""
    var total_items : Int? = 0
    var total_users : Int? = 0
    
    init(){
    }
    
    init (json: [String:Any]){
        warehouse_id = json["warehouse_id"] as? Int ?? 0
        warehouse_name = json["warehouse_name"] as? String ?? ""
        total_items = json["total_items"] as? Int ?? 0
        total_users = json["total_users"] as? Int ?? 1
    }
}

struct OrderItem: Codable{
    var product_id: Int32? = 0
    var product_name: String? = ""
    var product_img_path: String? = ""
    var required_quantity: Double? = 0.0
    var measurement_unit: String? = ""
    var purchased_quantity: Double? = 0.0
    var last_purchased_price: Double? = 0.0
    var recommended_price: Double? = 0.0
    
    init(){
    }
    
    init (json: [String:Any]){
        product_id = json["product_id"] as? Int32 ?? 0
        product_name = json["product_name"] as? String ?? ""
        product_img_path = json["product_img_path"] as? String ?? ""
        required_quantity = json["required_quantity"] as? Double ?? 0.0
        measurement_unit = json["measurement_unit"] as? String ?? ""
        purchased_quantity = json["purchased_quantity"] as? Double ?? 0.0
        last_purchased_price = json["last_purchased_price"] as? Double ?? 0.0
        recommended_price = json["recommended_price"] as? Double ?? 0.0
    }
}
