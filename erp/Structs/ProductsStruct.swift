//
//  AllProducts.swift
//  erp-ios
//
//  Created by apple on 27/01/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation
struct Product: Codable{
    var id: Int32?
    var code: String?
    var uuid: String?
    var name: String?
    var description: String?
    var external_bar_code: String?
    var internal_bar_code: String?
    var measurement_unit: String?
    var sale_price: Double?
    var buy_price: Double?
    var total_sales: Double?
    var total_wasted: Double?
    var product_category_id: Int32?
    var product_type_id: Int32?
    var product_type: String?
    var price_change_days: Double?
    var last_price_changed_date: String?
    var last_price_changed_by: String?
    var is_active: Bool?
    var photo: String?
    
    init(){
        id =  0
        code = ""
        uuid = ""
        name = ""
        description = ""
        external_bar_code = ""
        internal_bar_code = ""
        measurement_unit = ""
        sale_price = 0.00
        buy_price = 0.00
        total_sales = 0.00
        total_wasted = 0.00
        product_category_id = 0
        product_type_id = 0
        product_type = ""
        price_change_days = 0.00
        last_price_changed_date = ""
        last_price_changed_by = ""
        is_active = true
        photo = ""
        
    }
    
    init (json: [String:Any]){
        id = json["id"] as? Int32 ?? 0
        code = json["code"] as? String ?? ""
        uuid = json["uuid"] as? String ?? ""
        name = json["name"] as? String ?? ""
        description = json["description"] as? String ?? ""
        external_bar_code = json["external_bar_code"] as? String ?? ""
        internal_bar_code = json["internal_bar_code"] as? String ?? ""
        measurement_unit = json["measurement_unit"] as? String ?? ""
        sale_price = json["sale_price"] as? Double ?? 0.0
        buy_price = json["buy_price"] as? Double ?? 0.0
        total_sales = json["total_sales"] as? Double ?? 0.0
        total_wasted = json["total_wasted"] as? Double ?? 0.0
        product_category_id = json["product_category_id"] as? Int32 ?? 0
        product_type_id = json["product_type_id"] as? Int32 ?? 0
        product_type = json["product_type"] as? String ?? ""
        price_change_days = json["price_change_days"] as? Double ?? 0.0
        last_price_changed_date = json["last_price_changed_date"] as? String ?? ""
        last_price_changed_by = json["last_price_changed_by"] as? String ?? ""
        photo = json["photo"] as? String ?? ""
        is_active = json["is_active"] as? Bool ?? true
    }
    
    
}



