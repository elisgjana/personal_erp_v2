//
//  ClientsStruct.swift
//  erp
//
//  Created by Admin on 15/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation

struct Client: Codable{
    var id: Int32? = 0
    var nuis: String? = ""
    var uuid: String? = ""
    var name: String? = ""
    var owner: String? = ""
    var city_id: Int32? = 0
    var city: String? = ""
    var zone: String? = ""
    var phone: String? = ""
    var mobile_phone: String? = ""
    var email: String? = ""
    var website: String? = ""
    var contact_name: String? = ""
    var latitude: Double? = 0.0
    var longitude: Double? = 0.0
    var notes: String? = ""
    var is_active: Bool? = true
    var status_id: Int32? = 0
    var status: String? = ""
    var industry_type: String? = ""
    var business_type: String? = ""
    var business_type_id: Int32? = 0
    var industry_type_id: Int32? = 0
    
    init(){
    }
}
