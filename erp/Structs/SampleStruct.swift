//
//  SampleStruct.swift
//  StarterProject
//
//  Created by Akil Rajdho on yyyy-mm-dd.
//  Copyright © 2018 Inovacion. All rights reserved.
//

//sample struct that is used to take JSON as a parameter to initialize data
//please remove this struct in your project as it is used only as a demo
import Foundation
struct SampleStruct : Decodable {
    
    let firsName : String
    let lastName : String
    let status :  String
  
    init (json: [String:Any]){
        firsName = json["firsName"] as? String ?? ""
        lastName = json["lastName"] as? String ?? ""
        status   = json["status"] as? String ?? ""
    }
}
