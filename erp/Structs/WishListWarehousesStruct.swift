//
//  WishListWarehousesStruct.swift
//  erp-ios
//
//  Created by Admin on 08/03/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation

struct WishListWarehouse: Decodable{
    var warehouse_id : Int32
    var warehouse_name : String
    var total_users : Int32
    var total_items : Int32
    
    init (json: [String:Any]){
        warehouse_id = json["warehouse_id"] as? Int32 ?? 0
        warehouse_name = json["warehouse_name"] as? String ?? ""
        total_users = json["total_users"] as? Int32 ?? 0
        total_items = json["total_items"] as? Int32 ?? 0
    }
}

