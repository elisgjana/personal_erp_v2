//
//  AuthorizationKeyStruct.swift
//  erp-ios
//
//  Created by Admin on 26/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation
struct AuthorizationKeyStruct : Codable {
    
    let token_type : String
    let expires_in : Int32
    let access_token :  String
    let refresh_token :  String
    
    init (json: [String:Any]){
        token_type = json["token_type"] as? String ?? ""
        expires_in = json["expires_in"] as? Int32 ?? 0
        access_token = json["access_token"] as? String ?? ""
        refresh_token   = json["refresh_token"] as? String ?? ""
    }
}
