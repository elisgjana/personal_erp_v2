//
//  PurchasePaymentStruct.swift
//  erp
//
//  Created by Admin on 30/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
struct PurchasePayment: Codable{
    var payment_method: String?
    var amount: Double?
    var date: String?
    var user_id: Int32?
    
    init(){
        payment_method =  ""
        amount = 0.0
        date = ""
        user_id = 0
    }
    
    init(payment_method: String, amount: Double, date: String, user_id: Int32){
        self.payment_method =  payment_method
        self.amount = amount
        self.date = date
        self.user_id = user_id
    }
    
    init (json: [String:Any]){
        payment_method = json["payment_method"] as? String ?? ""
        amount = json["amount"] as? Double ?? 0.0
        date = json["date"] as? String ?? ""
        user_id = json["user_id"] as? Int32 ?? 0  
    }
}
