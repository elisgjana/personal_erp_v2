//
//  ErrorStruct.swift
//  erp
//
//  Created by Akil Rajdho on 24/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
struct ErrorResponseStruct : Codable {
    var detailed_message : String? = ""
    init (json: [String:Any]){
        detailed_message = json["detailed_message"] as? String ?? ""
    }
    
    init(){
        
    }
}
