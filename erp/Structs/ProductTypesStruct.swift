//
//  AllProductTypes.swift
//  erp-ios
//
//  Created by apple on 10/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation

struct ProductType: Decodable{
    var id: Int32?
    var name: String?
    var description: String?
    var is_active: Bool?
    
    
    
    init(){
        id =  0
        name = ""
        description = ""
        is_active = true
    }
    
    init(id: Int32, name:String, description:String, is_active:Bool ){
        
        self.id =  id
        self.name = name
        self.description = description
        self.is_active = is_active
        
    }
    
    init (json: [String:Any]){
        id = json["id"] as? Int32 ?? 0
        name = json["name"] as? String ?? ""
        description = json["description"] as? String ?? ""
        is_active = json["is_active"] as? Bool ?? true
    }
    
    
}
