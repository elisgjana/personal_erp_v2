//
//  CurrentMapData.swift
//  erp-ios
//
//  Created by apple on 24/01/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation
struct CurrentMapData{
    var zoom : Float = 12
    var latitude : Double = 41.3273996
    var longitude : Double = 19.81766
}
