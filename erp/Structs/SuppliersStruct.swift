//
//  AllSuppliers.swift
//  erp-ios
//
//  Created by Admin on 27/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//


import Foundation

struct Supplier: Codable{
    var id: Int32?
    var nuis: String?
    var uuid: String?
    var name: String?
    var owner: String?
    var city_id: Int32?
    var city: String?
    var zone: String?
    var phone: String?
    var mobile_phone: String?
    var email: String?
    var website: String?
    var contact_name: String?
    var latitude: Double?
    var longitude: Double?
    var notes: String?
    var is_active: Bool?
    var status_id: Int32?
    var status: String?
    var industry_type: String?
    var business_type: String?
    var business_type_id: Int32?
    var industry_type_id: Int32?
    
    init(){
        id   = 0
        nuis = ""
        uuid = ""
        name = ""
        owner = ""
        city_id = 0
        city = ""
        zone = ""
        phone = ""
        mobile_phone = ""
        email = ""
        website = ""
        contact_name = ""
        latitude = 0.0
        longitude = 0.0
        notes = ""
        is_active = true
        status_id = 0
        status = ""
        industry_type = ""
        business_type = ""
        business_type_id = 0
        industry_type_id = 0
    }
    
    init(id: Int32, nuis: String, uuid:String, name:String, owner:String, city_id:Int32, city:String, zone:String, phone:String, mobile_phone:String, email:String, website:String, contact_name:String, latitude:Double, longitude:Double, notes:String, is_active:Bool, status_id:Int32, status:String, industry_type:String, business_type:String, business_type_id:Int32, industry_type_id:Int32  ){
        self.id   = id
        self.nuis = nuis
        self.uuid = uuid
        self.name = name
        self.owner = owner
        self.city_id = city_id
        self.city = city
        self.zone = zone
        self.phone = phone
        self.mobile_phone = mobile_phone
        self.email = email
        self.website = website
        self.contact_name = contact_name
        self.latitude = latitude
        self.longitude = longitude
        self.notes = notes
        self.is_active = true
        self.status_id = status_id
        self.status = status
        self.industry_type = industry_type
        self.business_type = business_type
        self.business_type_id = business_type_id
        self.industry_type_id = industry_type_id
    }
    
    init (json: [String:Any]){
        id = json["id"] as? Int32 ?? 0
        nuis = json["nuis"] as? String ?? ""
        uuid = json["uuid"] as? String ?? ""
        name = json["name"] as? String ?? ""
        owner = json["owner"] as? String ?? ""
        city_id = json["city_id"] as? Int32 ?? 0
        city = json["city"] as? String ?? ""
        zone = json["zone"] as? String ?? ""
        phone = json["phone"] as? String ?? ""
        mobile_phone = json["mobile_phone"] as? String ?? ""
        email = json["email"] as? String ?? ""
        website = json["website"] as? String ?? ""
        contact_name = json["contact_name"] as? String ?? ""
        latitude = json["latitude"] as? Double ?? 0
        longitude = json["longitude"] as? Double ?? 0
        notes = json["notes"] as? String ?? ""
        is_active = json["is_active"] as? Bool ?? true
        status_id = json["status_id"] as? Int32 ?? 0
        status = json["status"] as? String ?? ""
        industry_type = json["industry_type"] as? String ?? ""
        business_type = json["business_type"] as? String ?? ""
        business_type_id = json["business_type_id"] as? Int32 ?? 0
        industry_type_id = json["industry_type_id"] as? Int32 ?? 0
    }
}



