//
//  UserPermissionsStruct.swift
//  erp
//
//  Created by Akil Rajdho on 15/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
struct UserPermissions : Codable {
    var user_id: Int32?
    var permissions: [String]?
    
    init (json: [String:Any]){
        user_id = json["user_id"] as? Int32 ?? 0
        permissions = json["permissions"] as? [String] ?? [""]
    }
}
