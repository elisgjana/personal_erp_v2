//
//  DeleteResponseStruct.swift
//  erp-ios
//
//  Created by Admin on 28/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation
struct DeleteResponse: Decodable {
    var data: [String]
    var message: String?
    var status: Int32?
    
    init(){
        data = [String]()
        message = ""
        status = 0
    }
    init (json: [String:Any]){
        data = json["data"] as? [String] ?? [String]()
        message = json["message"] as? String ?? ""
        status = json["status"] as? Int32 ?? 0
    }
}
