//
//  DetailResponseStruct.swift
//  erp-ios
//
//  Created by Admin on 06/03/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation
struct GenericResponse<T:Codable>:Codable {
    var data: [T]
    var message: String?
    var status: Int?
    
    init(){
        data = [T]()
        message = ""
        status = 0
    }
    init (json: [String:Any]){
        data = json["data"] as? [T] ?? [T]()
        message = json["message"] as? String ?? ""
        status = json["status"] as? Int ?? 0
    }
}
