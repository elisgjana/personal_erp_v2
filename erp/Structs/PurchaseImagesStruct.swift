//
//  PurchaseImagesStruct.swift
//  erp
//
//  Created by Admin on 06/06/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
struct PurchaseImage: Codable{
    var photo: String? = ""
    var uuid: String? = ""

    init(){
    }
    
    init (json: [String:Any]){
        photo = json["photo"] as? String ?? ""
        uuid = json["uuid"] as? String ?? ""
    }
    
}




