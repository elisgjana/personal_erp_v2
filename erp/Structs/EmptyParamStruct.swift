//
//  EmptyParamStruct.swift
//  erp
//
//  Created by Akil Rajdho on 20/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
struct EmptyParamStruct : Codable {
    var user_id :  Int32? = 0
    init (json: [String:Any]){
        user_id = json["user_id"] as? Int32 ?? 0
    }
    init (){
    }
}
