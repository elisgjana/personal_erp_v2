//
//  RejectionsStruct.swift
//  erp
//
//  Created by Admin on 01/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation

struct Rejection: Codable{
    var type: String?
    var date: String?
    var user_id: Int32?
    var warehouse_id: Int32?
    var warehouse_name: String?
    var destination_warehouse_id: Int32?
    var destination_warehouse_name: String?
    var note: String?
    var total_items: Double?
    var details: [RejectionDetail]?
    
    init(){
        type =  ""
        date = ""
        user_id = 0
        warehouse_id = 0
        warehouse_name = ""
        destination_warehouse_id = 0
        destination_warehouse_name = ""
        note = ""
        total_items = 0.0
        details = [RejectionDetail]()
    }
    
    init(type: String, date:String,warehouse_id:Int32,  warehouse_name: String, user_id: Int32, destination_warehouse_id:Int32, destination_warehouse_name:String, note:String, total_items: Double){
        self.type =  type
        self.date =  date
        self.warehouse_name = warehouse_name
        self.warehouse_id = warehouse_id
        self.user_id =  user_id
        self.destination_warehouse_id = destination_warehouse_id
        self.destination_warehouse_name = destination_warehouse_name
        self.note = note
        self.total_items = total_items
    }
    
    init (json: [String:Any]){
        type = json["type"] as? String ?? ""
        date = json["date"] as? String ?? ""
        user_id = json["user_id"] as? Int32 ?? 0
        warehouse_id = json["warehouse_id"] as? Int32 ?? 0
        warehouse_name = json["warehouse_name"] as? String ?? ""
        destination_warehouse_id = json["destination_warehouse_id"] as? Int32 ?? 0
        details = json["details"] as? [RejectionDetail] ?? [RejectionDetail]()
        destination_warehouse_name = json["destination_warehouse_name"] as? String ?? ""
        note = json["note"] as? String ?? ""
        total_items = json["total_items"] as? Double ?? 0.0
    }
}

struct RejectionDetail: Codable{
    var id: Int32? = 0
    var product_id: Int32?
    var user_id: Int32?
    var quantity: Double?
    var latitude: Double?
    var longitude: Double?
    var status_id: Int32?
    var product_name: String?
    var product_measurement_unit: String?
    var product_thumbnail: String?
    var warehouse_id: Int32
    
    
    private enum CodingKeys: String, CodingKey {
        case id
        case product_id
        case user_id
        case quantity
        case latitude
        case longitude
        case status_id
        case product_name
        case product_measurement_unit
        case product_thumbnail
        case warehouse_id
    }
    
    //when we get data from server
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int32.self, forKey: .id)
        product_id = try container.decode(Int32.self, forKey: .product_id)
        user_id = try container.decode(Int32.self, forKey: .user_id)
        quantity = try container.decode(Double.self, forKey: .quantity)
        latitude = try container.decode(Double.self, forKey: .latitude)
        status_id = try container.decode(Int32.self, forKey: .status_id)
        product_name = try container.decode(String.self, forKey: .product_name)
        product_measurement_unit = try container.decode(String.self, forKey: .product_measurement_unit)
        product_thumbnail = try container.decode(String.self, forKey: .product_thumbnail)
        warehouse_id = 0
    }
    
    //when we send data to server
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(user_id, forKey: .user_id)
        try container.encode(warehouse_id, forKey: .warehouse_id)
        try container.encode(product_id, forKey: .product_id)
        try container.encode(quantity, forKey: .quantity)
    }
    init(){
        id =  0
        product_id = 0
        user_id = 0
        quantity = 0.0
        latitude = 0.0
        longitude = 0.0
        status_id = 0
        product_name = ""
        product_measurement_unit = ""
        product_thumbnail = ""
        warehouse_id = 0
    }
    
    init(id: Int32, product_id: Int32, user_id: Int32, quantity:Double, latitude:Double, longitude:Double, status_id: Int32, product_name: String, product_measurement_unit:String,product_thumbnail:String, warehouse_id: Int32){
        
        self.id =  id
        self.product_id = product_id
        self.user_id = user_id
        self.quantity = quantity
        self.latitude = latitude
        self.longitude = longitude
        self.status_id = status_id
        self.product_name = product_name
        self.product_measurement_unit = product_measurement_unit
        self.product_thumbnail = product_thumbnail
        self.warehouse_id = warehouse_id
    }
    
    init (json: [String:Any]){
        id = json["id"] as? Int32 ?? 0
        product_id = json["product_id"] as? Int32 ?? 0
        user_id = json["user_id"] as? Int32 ?? 0
        product_name = json["product_name"] as? String ?? ""
        quantity = json["quantity"] as? Double ?? 0.0
        latitude = json["latitude"] as? Double ?? 0.0
        longitude = json["longitude"] as? Double ?? 0.0
        product_measurement_unit = json["product_measurement_unit"] as? String ?? ""
        status_id = json["status_id"] as? Int32 ?? 0
        product_thumbnail = json["product_thumbnail"] as? String ?? ""
        warehouse_id = 0
    }
    
}

