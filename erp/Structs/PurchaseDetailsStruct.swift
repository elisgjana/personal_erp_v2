//
//  PurchaseDetailsStruct.swift
//  erp
//
//  Created by Admin on 23/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
struct PurchaseDetail: Codable{
    var product_id: Int32?
    var product_name: String?
    var amount: Double?
    var product_price: Double?
    var discount: Double?
    var value: Double?
    
    init(){
        product_id =  0
        product_name = ""
        amount = 0.0
        product_price = 0.0
        discount = 0.0
        value = 0.0
    }
    init(product_id:Int32, product_name:String, amount:Double, product_price:Double, discount:Double, value:Double){
        self.product_id = product_id
        self.product_name = product_name
        self.amount = amount
        self.product_price = product_price
        self.discount = discount
        self.value = value
    }
    
    init (json: [String:Any]){
        product_id = json["product_id"] as? Int32 ?? 0
        product_name = json["product_name"] as? String ?? ""
        amount = json["amount"] as? Double ?? 0.0
        product_price = json["product_price"] as? Double ?? 0.0
        discount = json["discount"] as? Double ?? 0.0
        value = json["value"] as? Double ?? 0.0
    }
}
