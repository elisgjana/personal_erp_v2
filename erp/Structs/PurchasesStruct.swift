//
//  PurchasesStruct.swift
//  erp
//
//  Created by Admin on 16/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation

struct Purchase: Codable{
    var id: Int32?
    var uuid: String?
    var date: String?
    var invoice_no: String?
    var serial_no: String?
    var user_id: Int32?
    var supplier_uuid: String?
    var supplier_name: String?
    var delivered: Bool?
    var notes: String?
    var payment_method: String?
    var payment_value: Double?
    var discount: Double?
    var value: Double?
    var debt: Double?
    var latitude: Double?
    var longitude: Double?
    var status_id: Int32?
    var sort_date: String?
    var is_sync: Bool?
    var warehouse_id: Int32?
    var warehouse_name: String?
    var payments: [Payment]?
    var details: [Detail]?
    init(){
        
    }
    init (json: [String:Any]){
        id = json["id"] as? Int32 ?? 0
        uuid = json["uuid"] as? String ?? ""
        date = json["date"] as? String ?? ""
        invoice_no = json["invoice_no"] as? String ?? ""
        serial_no = json["serial_no"] as? String ?? ""
        user_id = json["user_id"] as? Int32 ?? 0
        supplier_uuid = json["supplier_uuid"] as? String ?? ""
        warehouse_id = json["warehouse_id"] as? Int32 ?? 0
        warehouse_name = json["warehouse_name"] as? String ?? ""
        supplier_name = json["supplier_name"] as? String ?? ""
        delivered = json["delivered"] as? Bool ?? true
        supplier_uuid = json["supplier_uuid"] as? String ?? ""
        notes = json["notes"] as? String ?? ""
        payment_method = json["payment_method"] as? String ?? ""
        payment_value = json["payment_value"] as? Double ?? 0.0
        discount = json["discount"] as? Double ?? 0.0
        value = json["value"] as? Double ?? 0.0
        debt = json["debt"] as? Double ?? 0.0
        latitude = json["latitude"] as? Double ?? 0.0
        longitude = json["longitude"] as? Double ?? 0.0
        status_id = json["status_id"] as? Int32 ?? 0
        sort_date = json["sort_date"] as? String ?? ""
        is_sync = json["is_sync"] as? Bool ?? true
        payments = json["payments"] as? [Payment] ?? [Payment]()
        details = json["details"] as? [Detail] ?? [Detail]()
    }
}

struct Payment: Codable{
    var purchase_uuid: String?
    var amount: Double?
    var payment_method: String?
    var notes: String?
    var date: String?
    var user_id: Int32?
    var user_full_name: String?
    
    init (){
        purchase_uuid = ""
        amount = 0.00
        payment_method = ""
        notes = ""
        date = ""
        user_id =  0
        user_full_name = ""
    }
    
    init (json: [String:Any]){
        purchase_uuid = json["purchase_uuid"] as? String ?? ""
        amount = json["amount"] as? Double ?? 0.0
        payment_method = json["payment_method"] as? String ?? ""
        notes = json["notes"] as? String ?? ""
        date = json["date"] as? String ?? ""
        user_id = json["user_id"] as? Int32 ?? 0
        user_full_name = json["user_full_name"] as? String ?? ""
    }
}
struct  Detail: Codable{
    var purchase_uuid: String?
    var product_id: Int32?
    var product_name: String?
    var image_url: String?
    var measurement_unit: String?
    var amount: Double?
    var product_price: Double?
    var last_buy_price: Double?
    var discount: Double?
    var total: Double?
    
    init (json: [String:Any]){
        purchase_uuid = json["purchase_uuid"] as? String ?? ""
        product_id = json["product_id"] as? Int32 ?? 0
        product_name = json["product_name"] as? String ?? ""
        image_url = json["image_url"] as? String ?? ""
        measurement_unit = json["measurement_unit"] as? String ?? ""
        amount = json["amount"] as? Double ?? 0.0
        product_price = json["product_price"] as? Double ?? 0.0
        discount = json["discount"] as? Double ?? 0.0
        total = json["total"] as? Double ?? 0.0
    }
    init (){
        purchase_uuid = ""
        product_id = 0
        product_name = ""
        image_url = ""
        measurement_unit = ""
        amount = 0.0
        product_price = 0.0
        discount = 0.0
        total = 0.0
    }
}


