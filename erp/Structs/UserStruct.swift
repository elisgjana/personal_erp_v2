//
//  LoginResponse.swift
//  erp
//
//  Created by Akil Rajdho on 12/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation

struct UserStruct : Codable{
    var id : Int? = 0
    var first_name : String? = ""
    var last_name : String? = ""
    var FullName : String? = ""
    var email : String? = ""
    var is_confirmed : Bool? = true
    var is_active : Bool? = true
    var default_warehouse_id : Int? = 0
    var password : String? = ""
    
    init(){
        
    }
    
    init (json: [String:Any]){
        id = json["id"] as? Int ?? 0
        first_name = json["first_name"] as? String ?? ""
        last_name = json["last_name"] as? String ?? ""
        FullName = json["FullName"] as? String ?? ""
        email = json["email"] as? String ?? ""
        is_confirmed = json["is_confirmed"] as? Bool ?? false
        is_active = json["is_active"] as? Bool ?? false
        id = json["id"] as? Int ?? -1
        password = ""
    }
}
