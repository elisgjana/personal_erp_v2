//
//  AllProducts.swift
//  erp-ios
//
//  Created by apple on 27/01/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation

struct WishList: Codable{
    var id: Int? 
    var date: String?
    var uuid: String?
    var user_id: Int32?
    var warehouse_id: Int?
    var warehouse_name: String?
    var status_id: Int32?
    var details: [WishListItem]?
    var sort_date: String?
    var is_sync: Bool?
    var is_active: Bool?

    init(){
        id =  0
        uuid = ""
        date = ""
        user_id = 0
        warehouse_id = 0
        warehouse_name = ""
        status_id = 0
        sort_date = ""
        details = [WishListItem]()
        is_active = true
        is_sync = true
    }

    init(id: Int, date:String,warehouse_id:Int,  uuid: String, user_id: Int32, status_id: Int32,sort_date:String, is_sync:Bool ){

        self.id =  id
        self.date =  date
        self.uuid = uuid
        self.warehouse_id = warehouse_id
        self.user_id =  user_id
        self.status_id = status_id
        self.sort_date = sort_date
        self.is_sync = is_sync
    }

    init (json: [String:Any]){
        id = json["id"] as? Int ?? 0
        date = json["date"] as? String ?? ""
        uuid = json["uuid"] as? String ?? ""
        user_id = json["user_id"] as? Int32 ?? 0
        warehouse_id = json["warehouse_id"] as? Int ?? 0
        warehouse_name = json["warehouse_name"] as? String ?? ""
        status_id = json["status_id"] as? Int32 ?? 0
        details = json["details"] as? [WishListItem] ?? [WishListItem]()
        sort_date = json["sort_date"] as? String ?? ""
        is_sync = json["is_sync"] as? Bool ?? true
        is_active = json["is_active"] as? Bool ?? true
    }


}

struct WishListItem: Codable{
    var id: Int? = 0
    var uuid: String? = ""
    var wishlist_uuid: String? = ""
    var product_id: Int? = 0
    var user_id: Int? = 0
    var warehouse_id: Int? = 0
    var quantity: Double? = 0.0
    var latitude: Double? = 0.0
    var longitude: Double? = 0.0
    var is_active: Bool? = true
    var status_id: Int? = 0
    var is_sync: Bool? = true
    var date: String? = convertDateToString(date: Date())
    
    init(){
    }
    
    init (json: [String:Any]){
        id = json["id"] as? Int ?? 0
        uuid = json["uuid"] as? String ?? ""
        wishlist_uuid = json["wishlist_uuid"] as? String ?? ""
        product_id = json["product_id"] as? Int ?? 0
        user_id = json["user_id"] as? Int ?? 0
        warehouse_id = json["warehouse_id"] as? Int ?? 0
        quantity = json["quantity"] as? Double ?? 0.0
        latitude = json["latitude"] as? Double ?? 0.0
        longitude = json["longitude"] as? Double ?? 0.0
        is_active = json["is_active"] as? Bool ?? true
        status_id = json["status_id"] as? Int ?? 0
        is_sync = json["is_sync"] as? Bool ?? true
        date = json["date"] as? String ?? convertDateToString(date: Date())
    }
}





