//
//  AllWarehouses.swift
//  erp-ios
//
//  Created by Admin on 27/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation

struct Warehouse: Codable{
    var id: Int32? = 0
    var code: String? = ""
    var barcode: String? = ""
    var name: String? = ""
    var type: String? = ""
    var is_active: Bool? = true
    
    init(){
        
    }
}

