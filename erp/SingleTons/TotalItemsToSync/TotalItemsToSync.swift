//
//  TotalItemsToSync.swift
//  erp
//
//  Created by Admin on 09/08/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit


open class TotalItemsToSync{
    
    open class var instance: TotalItemsToSync {
        struct Static {
            static let instance: TotalItemsToSync = TotalItemsToSync()
        }
        return Static.instance
    }
    
    open func numberOfItems() -> Int {
        var count = 0
        
        if AuthUser.has(PermissionTo.ACCESS_PRODUCTS){
            count += 1
        }
        
        if AuthUser.has(PermissionTo.ACCESS_WISHLISTS){
            count += 1
        }
        
        if AuthUser.has(PermissionTo.ACCESS_WAREHOUSE_WISHLIST){
            count += 1
        }
        
        if AuthUser.has(PermissionTo.ACCESS_PURCHASES){
            count += 1
        }
        
        if AuthUser.has(PermissionTo.ACCESS_SUPPLIERS){
            count += 1
        }
        
        if AuthUser.has(PermissionTo.ACCESS_CLIENTS){
            count += 1
        }
        
        return count
    }
}

