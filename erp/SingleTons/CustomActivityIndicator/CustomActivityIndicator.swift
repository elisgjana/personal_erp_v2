//
//  CustomActivityIndicator.swift
//  erp
//
//  Created by Admin on 07/09/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

open class CustomActivityIndicator{
    
    var backgroundTransparentView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.85)
        view.layer.cornerRadius = 10
        return view
    }()
    var activityIndicator: UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.activityIndicatorViewStyle = .whiteLarge
        return ai
    }()
    var textLabel: UILabel = {
        let label = UILabel()
        label.text = "Ju lutem prisni"
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()

    open class var instance: CustomActivityIndicator {
        struct Static {
            static let instance: CustomActivityIndicator = CustomActivityIndicator()
        }
        return Static.instance
    }
    
    /**
     This function generates a custom activity indicator
     - Parameter view : the view where the activity indicator will be added
     - Parameter text : The text that you want to display to the user
     */
    open func showCustomActivityIndicator(_ view : UIView, text : String = "Ju Lutem Prisni") {
        setupComponents()
        activityIndicator.startAnimating()
    }
    
    fileprivate func setupComponents(){
        if let window = UIApplication.shared.keyWindow{
            
            //setup background TransparentView
            window.addSubview(backgroundTransparentView)
            backgroundTransparentView.anchor(top: window.topAnchor, paddingTop: 0, left: window.leftAnchor, paddingLeft: 0, bottom: window.bottomAnchor, paddingBottom: 0, right: window.rightAnchor, paddingRight: 0, width: 0, height: 0)
            
            //setup containerView
            backgroundTransparentView.addSubview(containerView)
            containerView.anchor(top: nil, paddingTop: 0, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 160, height: 100)
            containerView.centerYAnchor.constraint(equalTo: backgroundTransparentView.centerYAnchor).isActive = true
            containerView.centerXAnchor.constraint(equalTo: backgroundTransparentView.centerXAnchor).isActive = true
            
            //setup activity indicator
            containerView.addSubview(activityIndicator)
            activityIndicator.anchor(top: containerView.topAnchor, paddingTop: 5, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 60, height: 60)
            activityIndicator.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
            
            //setup text label
            containerView.addSubview(textLabel)
            textLabel.anchor(top: activityIndicator.bottomAnchor, paddingTop: 0, left: containerView.leftAnchor, paddingLeft: 8, bottom: nil, paddingBottom: 0, right: containerView.rightAnchor, paddingRight: 8, width: 0, height: 0)
        }
    }
    
    open func hideCustomActivityIndicator() {
        performUIUpdatesOnMain {
            self.activityIndicator.stopAnimating()
            self.backgroundTransparentView.removeFromSuperview()
        }
    }
    
    /**
     This method returns to the main que and performs all operations on that queue
     - Parameter updates : the updates that you want to perform on main
     */
    func performUIUpdatesOnMain(_ updates: @escaping () -> Void) {
        DispatchQueue.main.async {
            updates()
        }
    }
}
