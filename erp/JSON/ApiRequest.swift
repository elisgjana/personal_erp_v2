//
//  ApiRequest.swift
//  StarterProject
//
//  Created by Elis Gjana on yyyy-mm-dd.
//  Copyright © 2018 Inovacion. All rights reserved.
//

import Foundation

class ApiRequest {
    
    /**
     This function creates a URL request
     -Parameter url : the url of the request
     -Parameter parameters :  a dictionary containing paramaters as string any pairs
     -Returns URLRequest
     */
    
    class func prepareUrlRequest<P:Codable>(urlString url:String,parameters: P, method:String)->URLRequest{
        
        let requestURL = URL(string: url)!
        //now create the NSMutableRequest object using the url object
        var request = URLRequest(url: requestURL)
        request.httpMethod = method //set http method
        
        do {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let body = try encoder.encode(parameters)
            request.httpBody = body
        } catch let error {
            print(error.localizedDescription)
        }
        
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let authorizationKey = "Bearer \(SavedVariables.getAccessToken())"
        request.addValue(authorizationKey, forHTTPHeaderField: "Authorization")
        
        return request
    }
    
    class func constructOAuthParameters()->AuthorizationKeyRequestStruct{
        //check if a refresh token is needed
        var authKeyStruct = AuthorizationKeyRequestStruct()
        authKeyStruct.client_id = Constants.CLIENT_ID
        authKeyStruct.client_secret = Constants.CLIENT_SECRET
        authKeyStruct.grant_type = Constants.GRANT_TYPE_PASSWORD
        authKeyStruct.refresh_token = ""
        authKeyStruct.username = SavedVariables.getUserEmail()
        authKeyStruct.scope = Constants.SCOPE
        authKeyStruct.password = SavedVariables.getUserPassword()
        return authKeyStruct
    }
    
    class func constructRefreshTokenParameters()->AuthorizationKeyRequestStruct{
        var authKeyStruct = AuthorizationKeyRequestStruct()
        authKeyStruct.client_id = Constants.CLIENT_ID
        authKeyStruct.client_secret = Constants.CLIENT_SECRET
        authKeyStruct.grant_type = Constants.GRANT_TYPE_PASSWORD
        authKeyStruct.refresh_token = SavedVariables.getRefreshToken()
        authKeyStruct.scope = Constants.SCOPE
        authKeyStruct.username = SavedVariables.getUserEmail()
        authKeyStruct.password = SavedVariables.getUserPassword()
        return authKeyStruct
    }
    
}
