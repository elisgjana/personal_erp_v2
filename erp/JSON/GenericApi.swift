//
//  ClientsApi.swift
//  erp-ios
//
//  Created by apple on 24/01/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation
import UIKit
import BRYXBanner

class GenericsApi: UIViewController{
    /**
     This functions i used to make an api call to a specific url and get back the results
     -Parameter urlString : the string representation of the url that you want to make a request to
     -Parameter paramters : a struct confirming to the codable protocol
     -Parameter callback :  returns the results of the api call to its initial caller function
     -Parameter method : the type of request we want to perform (GET, POST, PUT, DELETE, ETC)
     -Parameter event : optional parameter that should be filled if we want to listen for a specific notification event with that name
     -Return callback : return a generic type
     */
    
    public func fetchApiResults<T:Codable, P:Codable>(urlString: String, parameters: P, method: String, model:String = "", callback:@escaping (T)->()){
        if Device.hasNetConnection(){
            let request = ApiRequest.prepareUrlRequest(urlString: urlString, parameters: parameters, method: method)
            let session = URLSession.shared
            //create dataTask using the session object to send data to the server
            
            let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
                if let httpResponse = response as? HTTPURLResponse{
                    if httpResponse.statusCode == 401{
                        //enter logic to recall authorization key if the operations fail
                        //TODO : LOGOUT THE USER IF THIS ACTION FAILS 10 TIMES.
                        let queue = DispatchQueue(label: "al.inovacion.erp.getRrefreshToken",attributes: .concurrent)
                        queue.async {
                            self.waitForRefreshTokenKey()
                        }
                        self.registerTokenExpiredEvent()
                        CustomActivityIndicator.instance.hideCustomActivityIndicator()
                        return
                    }
                }
                guard error == nil else {
                    if model != "" {
                        setFailedModels(item: model)
                    }
                    self.registerApiErrorEvent()
                    CustomActivityIndicator.instance.hideCustomActivityIndicator()
                    return
                }
                guard let data = data else {
                    self.registerApiErrorEvent()
                    CustomActivityIndicator.instance.hideCustomActivityIndicator()
                    return
                }
                
                do {
                    try print(JSONSerialization.jsonObject(with: data, options: .mutableContainers))
                    
                    let json = try JSONDecoder().decode(T.self, from: data)
                    callback(json)
                    
                } catch {
                    if model != "" {
                        setFailedModels(item: model)
                        
                    }
                    self.registerApiErrorEvent( error: error.localizedDescription)
                    CustomActivityIndicator.instance.hideCustomActivityIndicator()
                }
            }
            
            task.resume()
        }else{
            presentNoInternetPopup()
            CustomActivityIndicator.instance.hideCustomActivityIndicator()
        }
    }
    
    public func waitForRefreshTokenKey() {
        if Device.hasNetConnection(){
            let parameters = ApiRequest.constructRefreshTokenParameters()
            self.fetchApiResults(urlString: Urls.GET_REFRESH_TOKEN, parameters: parameters, method: "POST", callback: {(response : AuthorizationKeyStruct)->()
                in
                SavedVariables.setTokenType(tokenType: response.token_type)
                SavedVariables.setExpiresIn(expiresIn: response.expires_in)
                SavedVariables.setAccessToken(access_token: response.access_token)
                SavedVariables.setRefreshToken(refresh_token: response.refresh_token)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.REFRESH_TOKEN_ON_SYNC), object: nil)
            })
        }
    }
    
    /**
     This method presents a popup that notifies the user that the token expired and registers an event to notify observers for this event
     **/
    public func registerTokenExpiredEvent(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.TOKEN_EXPIRED), object: nil)
        print("Token error")
    }
    
    /**
     This method presents a popup that notifies the user that the api returned an error and registers an event to notify observers for this event
     **/
    public func registerApiErrorEvent(error : String = "General Error"){
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.REQUEST_RETURNED_ERROR), object: nil)
        print("Api Error \(error)")
    }
    
    /**
     This method presents a popup that notifies the user that he is not connected to the internet
     **/
    public func presentNoInternetPopup(){
        presentTopNotification(title:"Error", subtitle: Strings.NO_NET_CONNECTION, image: "warning2",color: Colors.INOVACION_RED, duration: Constants.TOP_NOTIFICATION_DURATION )
    }
}

