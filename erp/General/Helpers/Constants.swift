//
//  Constatnts.swift
//  StarterProject
//
//  Created by Elis Gjana on yyyy-mm-dd.
//  Copyright © 2018 Inovacion. All rights reserved.
//

import Foundation
import CoreGraphics
struct Constants {
    
    static let STATUSES:[Int32: String] = [6: "E paguar", 10: "Paguar pjesërisht", 11: "E papaguar" ]
    static let TOP_NOTIFICATION_DURATION = 5.0
    //Auth Token Constants
    static let GRANT_TYPE_PASSWORD = "password"
    static let GRANT_TYPE_REFRESH = "refresh_token"
//    static let CLIENT_ID = "3"
//    //live client secret
//    static let CLIENT_SECRET = "VeQswxQnCJqkIPR7LMPP6Pt5sU6BRZMfexMK1abE"
    
    
    //staging client secret
//    static let CLIENT_SECRET = "QvjXWFPwa66KwajCAlAwZuACBfLKWMA8byOELurx"

    
    //e-commerce credintials
    static let CLIENT_ID =  "4"
    static let CLIENT_SECRET = "HNigejlkSEMxFqIfqqquEPfPI7ouHGcnRb2dTCRa"
    
    
    static let SCOPE = "*"
    static let AUTHORIZIATION_KEY = ""
    static var ITEMS_TO_SYNC: Int = 0
    //is used as a raw value for a notification observer to get a notification that a json request returned with error
    static let REQUEST_RETURNED_ERROR = "requestReturnedError"
    static let MONTHS = ["JANAR","SHKURT","MARS","PRILL","MAJ","QERSHOR","KORRIK","GUSHT","SHTATOR","TETOR","NENTOR","DHJETOR"]
    static let MENUS:[String: String] = ["QRCode":"qrCode","Sinkronizo": "synchronize", "Dil": "logout"]
    static let BASE64_COMPRESSION_RATE = CGFloat(0.5)
    
    
    

    
    
}
