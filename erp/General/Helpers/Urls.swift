//
//  Urls.swift
//  erp
//
//  Created by Akil Rajdho on 13/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
struct Urls{
    static let TOP_NOTIFICATION_DURATION = 5.0
    
    //staging url
//    static let BASE_URL = "https://erp-staging.inovacion.al/"
//    static let BASE_API_URL = "https://erp-staging.inovacion.al/api/"
//    static let BASE_IMAGE_URL = "https://erp-staging.inovacion.al"

    //live url
//    static let BASE_URL = "https://spar.inovacion.al/"
//    static let BASE_API_URL = "https://spar.inovacion.al/api/"
//    static let BASE_IMAGE_URL = "https://spar.inovacion.al"
    
    //staging e-commerce
    static let BASE_URL = "https://ecommerce-staging.inovacion.al/"
    static let BASE_API_URL = BASE_URL + "api/"
    static let BASE_IMAGE_URL = "https://ecommerce-staging.inovacion.al"
    
    static let LOGIN_URL = BASE_API_URL + "login"
    static let GET_OAUTH_KEY = BASE_URL + "oauth/token"
    static let GET_REFRESH_TOKEN = BASE_API_URL + "oauth/token"
    
    static let GET_ALL_CLIENTS = BASE_URL + "api/clients"
    static let GET_ALL_SUPPLIERS = BASE_URL + "api/suppliers"
    static let GET_ALL_WAREHOUSES = BASE_URL + "api/warehouses"
    static let GET_ALL_USER_WAREHOUSES = BASE_API_URL + "warehouses/?user_id=\(SavedVariables.getUserId())"
    static let GET_ALL_PURCHASES = BASE_URL + "api/purchases"
    static let GET_ALL_WISHLISTS = BASE_URL + "api/wishlists"
    static let GET_USER_WISHLISTS = BASE_API_URL + "wishlists?user_id=" + "\(SavedVariables.getUserId())"
    static let GET_USER_PURCHASES_URL = BASE_URL + "api/purchases/?user_id="+"\(SavedVariables.getUserId())"
    static let GET_ALL_PRODUCTS = BASE_API_URL + "products"
    static let GET_ALL_PRODUCT_TYPES = BASE_URL + "api/product_types"
    static let GET_ALL_INDUSTRY_TYPES = BASE_URL + "api/industry_types"
    static let GET_ALL_BUSINESS_TYPES = BASE_URL + "api/business_types"

    static let DELETE_WISHLISTS = BASE_URL + "api/wishlists/"
    static let DELETE_WISHLISTS_DETAIL = BASE_URL + "api/wishlist-details/"
    static let CREATE_WISHLIST_DETAIL = BASE_URL + "api/wishlists"

    static let GET_ALL_FILTERED_WISHLISTS = BASE_API_URL + "wishlists?user_id=\(SavedVariables.getUserId())"
    static let GET_USER_PERMISSIONS = BASE_API_URL + "user-permissions/\(SavedVariables.getUserId())"
    static let GET_WISHLIST_ITEMS = BASE_API_URL + "wishlists?user_id=\(SavedVariables.getUserId())"
    static let GET_REJECTION_DETAILS = BASE_API_URL + "rejections?user_id=\(SavedVariables.getUserId())&"
    static let GET_PRODUCTION_DETAILS = BASE_API_URL + "productions?user_id=\(SavedVariables.getUserId())&"
    static let GET_ORDER_ITEMS = BASE_API_URL + "wishlists_grouped?user_id=\(SavedVariables.getUserId())"
    static let GET_ALL_ORDERS = BASE_URL + "api/wishlist_warehouses?user_id=\(SavedVariables.getUserId())"
    static let CREATE_PURCHASE_URL = BASE_URL + "api/purchases"
    static let GET_ALL_REJECTIONS_URL = BASE_URL + "api/rejections?user_id=\(SavedVariables.getUserId())"
    static let GET_ALL_PRODUCTIONS_URL = BASE_URL + "api/productions?user_id=\(SavedVariables.getUserId())&warehouse_id="
    static let CREATE_REJECTION = BASE_URL + "api/rejections"
    static let CREATE_PRODUCTION = BASE_URL + "api/productions"
    static let GET_ALL_FILTERED_REJECTIONS = BASE_API_URL + "rejections?user_id=\(SavedVariables.getUserId())"
    static let GET_ALL_FILTERED_PRODUCTIONS = BASE_API_URL + "productions?user_id=\(SavedVariables.getUserId())"
    static let DELETE_REJECTION_DETAIL = BASE_URL + "api/rejections/"
    static let DELETE_PRODUCTION_DETAIL = BASE_URL + "api/productions/"
    static let GET_MRPRODUCT_URL = BASE_URL + "api/wishlist-reports/most-required-product-analysis"
    static let CREATE_PURCHASE_IMAGE = BASE_URL + "api/purchases/add-photo"
}
