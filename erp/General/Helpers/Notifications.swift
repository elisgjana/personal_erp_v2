//
//  Notifications.swift
//  erp
//
//  Created by Akil Rajdho on 14/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
struct Notifications {
    static let WISHLISTS_RETRIEVED = "wishListsRetrieved"
    static let ORDERS_RETRIEVED = "ordersRetrieved"
    static let CLIENTS_RETRIEVED = "clientsRetrieved"
    static let PRODUCTS_RETRIEVED = "productsRetrieved"
    static let PERMISSIONS_RETRIEVED = "productsRetrieved"
    static let WISHLIST_ITEMS_UPDATED = "wishListItemsUpdated"
    static let REQUEST_RETURNED_ERROR = "requestReturnedError"
    static let TOKEN_EXPIRED = "tokenExpired"
    static let HIDE_DARK_VIEW = "hideDarkView"
    static let WISHLIST_ITEMS_FILTERED = "wishListItemsFiltered"
    static let ORDER_ITEMS_FILTERED = "orderItemsFiltered"
    static let REJECTION_ITEMS_UPDATED = "rejectionItemsUpdated"
    static let PRODUCTION_ITEMS_UPDATED = "productionItemsUpdated"
    static let REFRESH_TOKEN_ON_SYNC = "refreshTokenOnSync"
    static let BACKGROUND_SYNC = "backgroundSync"
    static let LOGOUT_SYNC_DATA = "logoutSyncData"
    static let PERFORM_LOGOUT = "performLogout"
    static let FETCH_BARCHART_DATA = "fetchBarchartData"
}
