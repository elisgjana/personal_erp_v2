//
//  Colors.swift
//  StarterProject
//
//  Created by Elis Gjana on yyyy-mm-dd.
//  Copyright © 2018 Inovacion. All rights reserved.
//

import Foundation
struct Colors{
    static let randomColors = ["ba68dd", "8b5de2", "e25d7e", "5694f7", "3fdbac", "bedb3f", "dbc83f", "db923f", "db563f"]
    static let PRIMARY_DARK = "1b5691"
    static let PRIMARY_LIGHT = "2196f3"
    static let ACCENT_DARK = "8e2647"
    static let ACCENT_LIGHT = "b5345d"
    static let SIDEBAR_TINT = "62666d"
    static let PS = "4A0D48"
    static let INOVACION_GREEN = "2F9C7F"
    static let INOVACION_RED = "A22821"
    static let GRAY_SEPARATOR = "DCDCDC"
    static let INOVACION_RED_SECONDARY = "C53D27"
    static let INOVACION_BLUE = "349AD5"
    static let INOVACION_YELLOW = "FFBF61"
    static let INOVACION_DARK_GREY = "C53D27"
    static let INOVACION_GREEN_SECONDARY = "38B19D"
    static let CELL_LIGHT_BLUE = "03A9F4"
    static let WHITE = "FFFFFF"
    static let PIE_CHART_COLORS = ["3d0926", "6d1747", "a53373", "99527a", "bc6f9a", "dba0c1", "e50683"]
    static let BAR_CHART = "a36688"
}
