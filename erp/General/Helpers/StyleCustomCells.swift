//
//  StyleCustomCells.swift
//  erp
//
//  Created by Akil Rajdho on 21/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController{
    
    /**
     This function applies styles to a product table view cell
     -Params : cell the cell you want to style
     -Params : indexPath the index path of the cell you want to style
    */
    func styleProductsTable(_ cell: ProductCell, _ indexPath : IndexPath){
        let randomColor = getRandomColor(indexPath.row)
        //seting titles
        cell.salePriceTitle.text = Strings.SALE_PRICE_TITLE
        cell.buyPriceTitel.text = Strings.BUY_PRICE_TITLE
        cell.idTitle.text = Strings.ID_TITLE
        cell.itemCodeTitle.text = Strings.PRODUCT_CODE_TITLE
        
        //create the 2 little circles connected with dashes
        
        createRoundView(cell.circleView1)
        cell.circleView1.layer.borderColor = randomColor.cgColor
        cell.circleView1.layer.borderWidth = 1
        cell.circleView1.backgroundColor = UIColor.clear
        createRoundView(cell.circle1SubView)
        cell.circle1SubView.backgroundColor = randomColor
        cell.circle1SubView.layer.borderWidth = 1
        cell.circle1SubView.layer.borderColor = randomColor.cgColor
        
        createRoundView(cell.circleView2)
        cell.circleView2.layer.borderColor = randomColor.cgColor
        cell.circleView2.layer.borderWidth = 1
        cell.circleView2.backgroundColor = UIColor.clear
        createRoundView(cell.circle2Subview)
        cell.circle2Subview.backgroundColor = randomColor
        cell.circle2Subview.layer.borderWidth = 1
        cell.circle2Subview.layer.borderColor = randomColor.cgColor
        
        //bug when trying to draw a dottet path
//        var point1 = cell.circleView1.center
//        point1.y = point1.y + 15
//        point1.x = point1.x + 15
//
//        var point2 = cell.circleView2.center
//        point2.y = point2.y - 5
//        point2.x = point2.x + 15
//        cell.layer.addSublayer(drawDottedLine(start: point1, end: point2, color: randomColor))
        cell.colorView.backgroundColor = randomColor
        cell.connectorView.backgroundColor = randomColor
        //cell.colorView.roundCorners([.topLeft], radius: 10)
        //cell.colorView.roundCorners([.bottomLeft], radius: 10)
        
        //style each cell
        createRoundCorner(cell.containerView, 10)
        cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.containerView.layer.shadowRadius = 1
        cell.containerView.layer.shadowOpacity = 0.1
        
        //style the product image
        createRoundCorner(cell.productImage, 10)
        cell.productImage.contentMode = .scaleAspectFill
        cell.productImage.clipsToBounds = true
        cell.productImage.layer.borderWidth = 1
        cell.productImage.layer.borderColor = getRandomColor(indexPath.row).cgColor
        cell.productNameBar.layer.backgroundColor = getRandomColor(indexPath.row).cgColor
    }
    
    /**
     This function applies styles to a wishlist item table view cell
     -Params : cell the cell you want to style
     -Params : indexPath the index path of the cell you want to style
     */
    func styleWishlistItemTable(_ cell : WishlistItemCell, _ indexPath : IndexPath){
        //style the view
        createRoundCorner(cell.containerView, 10)
        cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.containerView.layer.shadowRadius = 1
        cell.containerView.layer.shadowOpacity = 0.1
        
        //style the product image
        createRoundCorner(cell.productImage, 10)
        cell.productImage.layer.borderWidth = 1
        cell.productImage.layer.borderColor = UIColor.gray.cgColor
        cell.productImage.layer.shadowColor = UIColor.black.cgColor;
        cell.productImage.layer.shadowOffset = CGSize(width: 10, height: 10);
        cell.productImage.layer.shadowOpacity = 0.5;
        cell.productImage.layer.shadowRadius = 1.0;
        
        //style the delete button
        createRoundCorner(cell.deleteView, 10)
        cell.deleteView.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.deleteView.layer.shadowRadius = 1
        cell.deleteView.layer.shadowOpacity = 0.1
    }
    
    /**
     This function applies styles to a order item table view cell
     -Params : cell the cell you want to style
     -Params : indexPath the index path of the cell you want to style
     */
    func styleOrderItemTable(_ cell : OrderItemCell, _ indexPath : IndexPath){
        createRound(imageView: cell.productImage, borderColor: getRandomColor(indexPath.row))
        createRoundCorner(cell.containerView, 10)
        cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.containerView.layer.shadowRadius = 1
        cell.containerView.layer.shadowOpacity = 0.5
        cell.moneyImage.tintColor = createColorFromHex(hex: Colors.INOVACION_GREEN)
        cell.recommendedPriceLabel.textColor = createColorFromHex(hex: Colors.INOVACION_GREEN)
        cell.priceImage.tintColor = UIColor.gray
        cell.scaleImage.tintColor = UIColor.gray
        cell.wishlistImage.tintColor = UIColor.gray
    }
    
    /**
     This function applies styles to a wishlist item table view cell
     -Params : cell the cell you want to style
     -Params : indexPath the index path of the cell you want to style
     */
    
    func styleWishlistTable(_ cell: WishListCell, _ indexPath : IndexPath){
        createRoundCorner(cell.containerView, 10)
        cell.dateLabel.textColor = getRandomColor(indexPath.row)
        cell.dateLabel.adjustsFontSizeToFitWidth = true
        cell.warehouseNameLabel.adjustsFontSizeToFitWidth = true
        cell.calendarImage.tintColor = getRandomColor(indexPath.row)
        cell.nrOfItemsImage.tintColor = getRandomColor(indexPath.row)
        cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.containerView.layer.shadowRadius = 1
        cell.containerView.layer.shadowOpacity = 0.1
    }
    
    /**
     This function applies styles to a wishlist item table view cell
     -Params : cell the cell you want to style
     -Params : indexPath the index path of the cell you want to style
     */
    
    func styleOrdersTable(_ cell: OrderCell, _ indexPath : IndexPath){
        createRoundCorner(cell.containerView, 10)
        cell.dateLabel.textColor = getRandomColor(indexPath.row)
        cell.dateLabel.adjustsFontSizeToFitWidth = true
        cell.warehouseNameLabel.adjustsFontSizeToFitWidth = true
        cell.colorView.backgroundColor = getRandomColor(indexPath.row)
        cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.colorView.roundCorners([.topLeft, .bottomLeft], radius: 10)
        cell.tintView.roundCorners([.topRight, .bottomRight], radius: 10)
        cell.containerView.layer.shadowRadius = 1
        cell.containerView.layer.shadowOpacity = 0.1
    }
    /**
     This function applies styles to a purchase item table view cell
     -Params : cell the cell you want to style
     -Params : indexPath the index path of the cell you want to style
     */
    
    func stylePurchaseTable(_ cell: PurchaseCell, _ indexPath : IndexPath){
        createRoundCorner(cell.containerView, 10)
        cell.colorView.layer.backgroundColor = getRandomColor(indexPath.row).cgColor
        cell.colorView.roundCorners([.topLeft, .bottomLeft], radius: 10)
        createRoundCorner(cell.containerView, 10)
        cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.containerView.layer.shadowRadius = 1
        cell.containerView.layer.shadowOpacity = 0.5
    }
    /**
     This function applies styles to search supplier table view cell
     -Params : cell the cell you want to style
     -Params : indexPath the index path of the cell you want to style
     */
    
    func styleSearchSuppliersTable(_ cell: SearchSupplierCell, _ indexPath : IndexPath){
        createRoundCorner(cell.containerView, 10)
        cell.colorView.layer.backgroundColor = getRandomColor(indexPath.row).cgColor
        cell.colorView.roundCorners([.topLeft, .bottomLeft], radius: 10)
        cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.containerView.layer.shadowRadius = 1
        cell.containerView.layer.shadowOpacity = 0.1
    }
    /**
     This function applies styles to search warehouse table view cell
     -Params : cell the cell you want to style
     -Params : indexPath the index path of the cell you want to style
     */
    
    func styleSearchWarehousesTable(_ cell: SearchWarehouseCell, _ indexPath : IndexPath){
        createRoundCorner(cell.containerView, 10)
        cell.colorView.layer.backgroundColor = getRandomColor(indexPath.row).cgColor
        cell.colorView.roundCorners([.topLeft, .bottomLeft], radius: 10)
    }
    
    /**
     This function applies styles to purchase product table view cell
     -Params : cell the cell you want to style
     -Params : indexPath the index path of the cell you want to style
     */
    
    func stylePurchaseProductTable(_ cell: PurchaseProductCell, _ indexPath : IndexPath){
        createRoundCorner(cell.containerView, 10)
        cell.scaleImage.tintColor = UIColor.gray
        cell.discountImage.tintColor = UIColor.gray
        cell.wishlistImage.tintColor = UIColor.gray
        cell.productPriceField.isUserInteractionEnabled = false
        cell.productAmountField.isUserInteractionEnabled = false
        cell.productPriceField.textColor = UIColor.black
        cell.productAmountField.textColor = UIColor.black
        createRound(imageView: cell.productImage, borderColor: getRandomColor(indexPath.row))
        cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.containerView.layer.shadowRadius = 1
        cell.containerView.layer.shadowOpacity = 0.1
    }
    
    /**
     This function applies styles to menu item table view cell
     -Params : cell the cell you want to style
     -Params : indexPath the index path of the cell you want to style
     */
    
    func styleMoreItemTable(_ cell: MoreItemCell, _ indexPath : IndexPath){
        cell.menuNameLabel.textColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        cell.menuImage.tintColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
    }
    
    /**
     This function applies styles rejection table view cell
     -Params : cell the cell you want to style
     -Params : indexPath the index path of the cell you want to style
     */
    
    func styleRejectionTable(_ cell: RejectionCell, _ indexPath : IndexPath){
        createRoundCorner(cell.containerView, 10)
        cell.calendarImage.tintColor = getRandomColor(indexPath.row)
        cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.containerView.layer.shadowRadius = 1
        cell.containerView.layer.shadowOpacity = 0.1
    }
    
    /**
     This function applies styles rejection detail table view cell
     -Params : cell the cell you want to style
     -Params : indexPath the index path of the cell you want to style
     */
    
    func styleRejectionDetailTable(_ cell: RejectionDetailCell, _ indexPath : IndexPath){
        createRoundCorner(cell.containerView, 10)
        cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.containerView.layer.shadowRadius = 1
        cell.containerView.layer.shadowOpacity = 0.1
        createRoundCorner(cell.productImage, 10)
        cell.productImage.layer.borderWidth = 1
        cell.productImage.layer.borderColor = UIColor.gray.cgColor
        cell.productImage.layer.shadowColor = UIColor.black.cgColor;
        cell.productImage.layer.shadowOffset = CGSize(width: 10, height: 10);
        cell.productImage.layer.shadowOpacity = 0.5;
        cell.productImage.layer.shadowRadius = 1.0;
        createRoundCorner(cell.deleteView, 10)
        cell.deleteView.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.deleteView.layer.shadowRadius = 1
        cell.deleteView.layer.shadowOpacity = 0.1
    }
    
    /**
     This function applies styles client table view cell
     -Params : cell the cell you want to style
     -Params : indexPath the index path of the cell you want to style
     */
    
    func styleClientDetailTable(_ cell: ClientCell, _ indexPath : IndexPath){
        createRoundCorner(cell.allClientsContainer, 10)
        cell.allClientsContainer.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.allClientsContainer.layer.shadowRadius = 1
        cell.allClientsContainer.layer.shadowOpacity = 0.1
    }
    
}
