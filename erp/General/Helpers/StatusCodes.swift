
//
//  StatusCodes.swift
//  erp
//
//  Created by Akil Rajdho on 14/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
struct StatusCodes{
    static let COULD_NOT_GET_TOKEN = -2
    static let NO_RESULTS = -3
    static let SUCCESS = 0;
    static let USER_AUTHENTICATION_FAILED = 1
    static let VALIDATION_FAILED = 2
    static let RESOURCE_NOT_FOUND = 3
    static let RESOURCE_EXISTS = 4
    static let GENERAL_ERROR = 5
    static let RESOURCE_INACTIVE = 6
    static let RESOURCE_CAN_NOT_BE_MODIFIED = 5
    static let WAREHOUSE_NOT_ACCESSABLE = 7
    static let TOKKEN_EXPIRED = 401
}
