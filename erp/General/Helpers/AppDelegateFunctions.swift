//
//  AppDelegateFunctions.swift
//  StarterProject
//
//  Created by Elis Gjana on yyyy-mm-dd.
//  Copyright © 2018 Inovacion. All rights reserved.
//

import Foundation
import UIKit

func getAppDelegate()->AppDelegate{
    let object = UIApplication.shared.delegate
    return object as! AppDelegate
}

func getAllWishLists()->[WishList]{
    return getAppDelegate().wishLists
}

func setAllWishLists(wishLists:[WishList]){
    return getAppDelegate().wishLists = wishLists
}

func getWishlistDetailWarehouse()->String{
    return getAppDelegate().wishlistDetailWarehouse
}

func setWishlistDetailWarehouse(warehouse:String){
    return getAppDelegate().wishlistDetailWarehouse = warehouse
}

func getWishlistDetailWarehouseId()->Int32{
    return getAppDelegate().wishlistDetailWarehouseId
}

func setWishlistDetailWarehouseId(warehouseId:Int32){
    return getAppDelegate().wishlistDetailWarehouseId = warehouseId
}

func getAllOrders()->[Order]{
    return getAppDelegate().orders
}

func setAllOrders(orders:[Order]){
    return getAppDelegate().orders = orders
}

func getAllPurchaseImages()->[PurchaseImage]{
    return getAppDelegate().purchaseImages
}

func setAllPurchaseImages(purchaseImages:[PurchaseImage]){
    return getAppDelegate().purchaseImages = purchaseImages
}

func getAllPurchases()->[Purchase]{
    return getAppDelegate().purchases
}

func setAllPurchases(purchases:[Purchase]){
    return getAppDelegate().purchases = purchases
}

func getAllSuppliers()->[Supplier]{
    return getAppDelegate().suppliers
}

func setAllSuppliers(suppliers:[Supplier]){
    return getAppDelegate().suppliers = suppliers
}

func getAllClients()->[Client]{
    return getAppDelegate().clients
}

func setAllClients(clients:[Client]){
    return getAppDelegate().clients = clients
}

func getAllRejections()->[Rejection]{
    return getAppDelegate().rejections
}

func setAllRejections(rejections:[Rejection]){
    return getAppDelegate().rejections = rejections
}

func getAllProductions()->[Production]{
    return getAppDelegate().productions
}

func setAllProductions(productions:[Production]){
    return getAppDelegate().productions = productions
}

func getAllOrderItems()->[OrderItem]{
    return getAppDelegate().orderItems
}

func setAllOrderItems(orderItems:[OrderItem]){
    return getAppDelegate().orderItems = orderItems
}

func getAllProducts()->[Product]{
    return getAppDelegate().products
}

func setAllProducts(products:[Product]){
    return getAppDelegate().products = products
}

func getAllWarehouses()->[Warehouse]{
    return getAppDelegate().warehouses
}

func setAllWarehouses(warehouses:[Warehouse]){
    return getAppDelegate().warehouses = warehouses
}

func getFailedModels()->[String]{
    return getAppDelegate().failedModels
}

func setFailedModels(item:String){
    return getAppDelegate().failedModels.append(item)
}

func getPurchaseInvoiceTotal()->Double{
    return getAppDelegate().purchaseInvoiceTotal
}

func setPurchaseInvoiceTotal(total:Double){
    return getAppDelegate().purchaseInvoiceTotal = total
}

func getProductId() -> Int32{
    return getAppDelegate().productId
}

func setProductId(id:Int32){
    return getAppDelegate().productId = id
}

/**
 This Method changes the color of the Nav bar
 -Parameter color : String reperesenting the hexadecimal code of a specific color
 -Parameter tintColor : String reperesenting the hexadecimal code of a specific color
 */
func changeNavBarColor(color:String, tintColor: String){
    // setup navBar.....
    UINavigationBar.appearance().barTintColor = createColorFromHex(hex: color)
    UINavigationBar.appearance().tintColor = createColorFromHex(hex: tintColor)
    UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    UINavigationBar.appearance().isTranslucent = false
}

/**
 This Method changes the color of the tab bar
 -Parameter color : String reperesenting the hexadecimal code of a specific color
 -Parameter tintColor : String reperesenting the hexadecimal code of a specific color
 -Parameter titleColor : String reperesenting the hexadecimal code of a specific color
 */
func changeTabBarColor(color:String, tintColor: String, titleColor: String = "FFFFFF"){
    UITabBar.appearance().barTintColor = createColorFromHex(hex: color)
    UITabBar.appearance().tintColor = createColorFromHex(hex: tintColor)
    UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: createColorFromHex(hex: titleColor)], for: .normal)
}

/**
 This method is used in New Purchase Poducts View Controller to calculate the invoice total
 **/
func calculatePurchaseInvoiceTotal(){
    setPurchaseInvoiceTotal(total: 0.0)
    for i in 0...purchaseDetails.count-1{
        var currentPrice = purchaseDetails[i].product_price! * purchaseDetails[i].amount!
        currentPrice = currentPrice - purchaseDetails[i].discount! * 0.01 * currentPrice
        setPurchaseInvoiceTotal(total: getPurchaseInvoiceTotal() + currentPrice)
    }
}

func getApiClient()->GenericsApi{
    return getAppDelegate().GenericsApiClient
}
