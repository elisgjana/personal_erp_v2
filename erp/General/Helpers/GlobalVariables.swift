//
//  GlobalVariables.swift
//  erp-ios
//
//  Created by apple on 24/01/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation

var currentLatitude = 0.0
var currentLongitude = 0.0
var filterStartDate = Device.getYesterdayDate()
var filterEndDate = Device.getCurrentDate()

//variables used for creating new purchase
var supplier_uuid = ""
var infoIsConfirmed = false
var date = Device.getCurrentDate()
var supplier_name_search = ""
var uuid = ""
var selected_product_id = 0
var invoice_value = 0.0
var warehouse_id = 0
var invoice_no = ""
var serial_no = ""
var delivered = false
var discount = 0.0
var latitude = 0.0
var longitude = 0.0
var status_id: Int32 = 0
var notes = ""
var payment_method = ""
var payment_value = 0.0
var purchaseDetails = [PurchaseDetail]()
var payments = [PurchasePayment]()
var orderItemForPurchase = [OrderItem]()
var purchaseImage = ""

//More Menu - Array of Menus depending on user permissions
var menuItemsNameToSegue = [String: String]()
var menuItemsIndexToName = [Int: String]()


