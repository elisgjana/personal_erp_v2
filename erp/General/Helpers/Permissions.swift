//
//  Permissions.swift
//  erp
//
//  Created by Akil Rajdho on 15/03/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
class AuthUser {
    //Will be used to determine the permissions for a specific module or view
    class func has(_ permission: String)->Bool{
        return SavedVariables.getUserPermissions().contains(permission)
    }
}

struct PermissionTo{
    static let ACCESS_WISHLISTS = "mobile_menu_wishlist"
    static let ACCESS_SALES = "mobile_menu_sales"
    static let ACCESS_PURCHASES = "mobile_menu_purchases"
    static let ACCESS_CLIENTS = "mobile_menu_clients"
    static let ACCESS_SUPPLIERS = "mobile_menu_suppliers"
    static let ACCESS_PRODUCTS = "mobile_menu_products"
    static let ACCESS_ORDERS = "mobile_menu_manager_wishlist"
    static let ACCESS_PRODUCTIONS = "mobile_menu_productions"
    static let ACCESS_REJECTIONS = "mobile_menu_rejections"
    static let ACCESS_REPORTS = "access_graphs_report"
    
    static let ACCESS_WAREHOUSE_WISHLIST = "mobile_menu_warehouse_wishlist"
    static let SEE_PRODUCT_SALE_PRICE = "mobile_option_see_products_sale_price"
    static let SEE_PRODUCT_BUY_PRICE = "mobile_option_see_products_buy_price"
    static let SEE_PRODUCT_CODE = "mobile_option_see_products_item_code"
    static let DELETE_WISHLIST_ITEM = "mobile_option_delete_wishlist_item"
    static let DELETE_REJECTION_ITEM = "mobile_option_delete_rejection_item"
    static let DELETE_PRODUCTION_ITEM = "mobile_option_delete_production_item"
    
}
