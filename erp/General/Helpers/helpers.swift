//
//  helpers.swift
//  StarterProject
//
//  Created by Elis Gjana on yyyy-mm-dd.
//  Copyright © 2018 Inovacion. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift


/**
 This function creates a ui color
 -Parameter hex: the hexadecimal string
 -Returns UIColor representing the hexadecimal String
 */
func createColorFromHex (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func convertDateToString(date:Date)->String{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    dateFormatter.timeZone = NSTimeZone(name: "UTC+1") as TimeZone?
    let convertedDate = dateFormatter.string(from: date)
    return convertedDate
}

func convertStringToDate(date : String)->Date{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"//this your string date format
    dateFormatter.timeZone = NSTimeZone(name: "UTC+1") as TimeZone?
    let convertedDate = dateFormatter.date(from: date)
    
    guard dateFormatter.date(from: date) != nil else {
        assert(false, "no date from string")
        return Date()
    }
    return convertedDate!
}

func transfromDateToLongDate(date: String)->String{
    
    let year = date.substring(fromIndex: 0, toIndex: 3)
    let month = date.substring(fromIndex: 5, toIndex: 6)
    let day = date.substring(fromIndex: 8, toIndex: 9)
    
    let monthString = Constants.MONTHS[Int(month)! - 1]
    
    return "\(day) \(monthString) \(year)"
}
func convertStringToDate(_ mydate : String)->Date{
    let fullDate = "\(mydate) 0\(getTimeZoneOffset()):00:00"
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return dateformatter.date(from: fullDate)!
}

func transfromDateToLongDateRange(startDate: String, endDate: String)->String{
    
    let startYear = startDate.substring(fromIndex: 0, toIndex: 3)
    let startMonth = startDate.substring(fromIndex: 5, toIndex: 6)
    let startDay = startDate.substring(fromIndex: 8, toIndex: 9)
    
    let endYear = endDate.substring(fromIndex: 0, toIndex: 3)
    let endMonth = endDate.substring(fromIndex: 5, toIndex: 6)
    let endDay = endDate.substring(fromIndex: 8, toIndex: 9)
    
    let startMonthString = Constants.MONTHS[Int(startMonth)! - 1]
    let endMonthString = Constants.MONTHS[Int(endMonth)!-1]
    if startDay == endDay && startMonth == endMonth {
        return "\(startDay) \(startMonthString) \(startYear)"
    }
    return "\(startDay) \(startMonthString) - \(endDay) \(endMonthString) \(endYear)"
}

func snc(){
    UserDefaults.standard.synchronize()
}

func prepareQueryUrlForWishlist(_ startDate: String = "", _ endDate: String = "")->String{
    var url = ""
    if startDate != ""{
        if endDate != ""{
            url = Urls.GET_ALL_FILTERED_WISHLISTS + "start_date=\(startDate)&end_date=\(endDate)"
        }else{
            url = Urls.GET_ALL_FILTERED_WISHLISTS + "start_date=\(startDate)"
        }
    }
    else{
        if endDate != ""{
            url = Urls.GET_ALL_FILTERED_WISHLISTS + "end_date=\(startDate)"
        }else{
            url = Urls.GET_ALL_FILTERED_WISHLISTS + "start_date=\(Device.getCurrentDate())&end_date=\(Device.getCurrentDate())"
        }
    }
    
    return url
}

func printFormatedJson<T:Codable>(structure: T){
    let encoder = JSONEncoder()
    encoder.outputFormatting = .prettyPrinted
    let body = try! encoder.encode(structure)
    try!  print(JSONSerialization.jsonObject(with: body, options: .mutableContainers))
}
func getTimeZoneOffset() -> Int{
    let seconds = TimeZone.current.secondsFromGMT()
    let hours = seconds/3600
    return hours
}






