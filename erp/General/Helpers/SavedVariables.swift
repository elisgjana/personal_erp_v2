//
//  SavedVariables.swift
//  erp-ios
//
//  Created by apple on 12/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation
class SavedVariables{
    class func getUserEmail()-> String{
        if UserDefaults.standard.value(forKey: "userEmail") != nil{
            return UserDefaults.standard.value(forKey: "userEmail") as! String
        }else{
            return ""
        }
    }
    class func setUserEmail(userEmail: String){
        UserDefaults.standard.set(userEmail, forKey: "userEmail")
        sync()
    }
    class func getUserFirstName()-> String{
        if UserDefaults.standard.value(forKey: "userFirstName") != nil{
            return UserDefaults.standard.value(forKey: "userFirstName") as! String
        }else{
            return ""
        }
    }
    class func setUserFirstName(userFirstName: String){
        UserDefaults.standard.set(userFirstName, forKey: "userFirstName")
        sync()
    }
    class func getUserId()->Int {
        return UserDefaults.standard.value(forKey: "userId") as! Int
    }
    class func setUserId(userId:Int){
        UserDefaults.standard.setValue(userId, forKey: "userId")
        sync()
    }
    
    class func getUserUuid()->String {
        if UserDefaults.standard.value(forKey: "userUuid") != nil{
            return UserDefaults.standard.value(forKey: "userUuid") as! String
        }else{
            return ""
        }
    }
    
    class func setUserUuid(userUuid:Int){
        UserDefaults.standard.setValue(userUuid, forKey: "userUuid")
        sync()
    }
    
    class func getDefaultWarehouseId()->Int {
        return UserDefaults.standard.value(forKey: "defaultWarehouseId") as! Int
    }
    class func setDefaultWarehouseId(defaultWarehouseId:Int){
        UserDefaults.standard.setValue(defaultWarehouseId, forKey: "defaultWarehouseId")
        sync()
    }
    
    class func getUserLastName()->String {
        if UserDefaults.standard.value(forKey: "userLastName") != nil{
            return UserDefaults.standard.value(forKey: "userLastName") as! String
        }else
        {
            return ""
        }
    }
    class func setUserLastName(userLastName:String){
        UserDefaults.standard.setValue(userLastName, forKey: "userLastName")
        sync()
    }
    
    class func setTokenType(tokenType : String){
        UserDefaults.standard.setValue(tokenType, forKey: "tokenType")
        sync()
    }
    
    class func getTokenType()->String{
        return UserDefaults.standard.value(forKey: "tokenType") as! String
    }
    
    class func setExpiresIn(expiresIn : Int32){
        UserDefaults.standard.setValue(expiresIn, forKey: "expiresIn")
        sync()
    }
    
    class func getExpiresIn()->Int32{
        return UserDefaults.standard.value(forKey: "expiresIn") as! Int32
    }
    class func setAccessToken(access_token : String){
        UserDefaults.standard.setValue(access_token, forKeyPath: "access_token")
        sync()
    }
    
    class func getAccessToken()->String{
        if let currentToken = UserDefaults.standard.value(forKey: "access_token") as? String{
            return currentToken
        } else{
            return ""
        }
    }
    class func setRefreshToken(refresh_token : String){
        UserDefaults.standard.setValue(refresh_token, forKeyPath: "refresh_token")
        sync()
    }
    
    class func getRefreshToken()->String{
        if UserDefaults.standard.value(forKey: "refresh_token") != nil {
            return UserDefaults.standard.value(forKey: "refresh_token") as! String
        } else {
            return ""
        }
    }
    
    class func errorRequiringRefreshToken()->Bool{
        return UserDefaults.standard.value(forKey: "errorRequiringRefreshToken") as! Bool
    }
    
    class func setErrorRequiringRefreshToken(state: Bool){
        UserDefaults.standard.set(state, forKey: "errorRequiringRefreshToken")
        sync()
    }
    
    class func getUserPassword()->String{
        return UserDefaults.standard.value(forKey: "userPassword") as! String
    }
    
    class func setUserPassword(password: String){
        UserDefaults.standard.set(password,forKey: "userPassword")
        sync()
    }
    
    class func getSyncNeeded()->Bool{
        return UserDefaults.standard.value(forKey: "syncNeeded") as! Bool
    }
    
    class func setSyncNeeded(syncNeeded: Bool){
        UserDefaults.standard.set(syncNeeded,forKey: "syncNeeded")
        sync()
    }
    
    class func getIsSync()->Bool{
        return UserDefaults.standard.value(forKey: "isSync") as! Bool
    }
    
    class func setIsSync(isSync: Bool){
        UserDefaults.standard.set(isSync,forKey: "isSync")
        sync()
    }
    
    class func getUserPermissions()->[String]{
        return UserDefaults.standard.value(forKey: "userPermissions") as! [String]
    }
    
    class func setUserPermissions(permissions: [String]){
        UserDefaults.standard.set(permissions, forKey: "userPermissions")
        sync()
    }
    
    class func setShoudlChangeInterface(value : Bool){
        UserDefaults.standard.set(value, forKey: "shoudChangeInterface")
        sync()
    }
    
    class func getShouldSetInterface()->Bool{
        return UserDefaults.standard.value(forKey: "shoudChangeInterface") as! Bool
    }
}
