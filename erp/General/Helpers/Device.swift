//
//  Device.swift
//  StarterProject
//
//  Created by Elis Gjana on yyyy-mm-dd.
//  Copyright © 2018 Inovacion. All rights reserved.
//

import Foundation
import SystemConfiguration
public class Device {
    
    /**
     This function is used to check if there is a net connection available
     -Returns true if there is a net connection and false otherwise
    */
    class func hasNetConnection() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    /**
        -This Function Returns The Current Date in YYYY-MM-DD string format
    */
    class func getCurrentDate() -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
    }
    
    /**
     -This Function Returns The Current Date
     */
    class func getTodayDate() -> Date{
        let date = Date()
        return date
    }
    
    /**
     -This Function Returns The Yesterday Date in YYYY-MM-DD string format
     */
    class func getYesterdayDate() -> String{
        let date = Date().yesterday
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
    }
    
    /**
     -This Function Returns The Tomorrow Date in YYYY-MM-DD string format
     */
    class func getTomorrowDate() -> String{
        let date = Date().tomorrow
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
    }
}
