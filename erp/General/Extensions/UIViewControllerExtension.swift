//
//  ViewControllerExtension.swift
//  FastEnergy
//
//  Created by Akil Rajdho on yyyy-mm-dd.
//  Copyright © 2017 FastEnergy. All rights reserved.
//

import Foundation
import UIKit
import BRYXBanner
import CoreLocation

extension UIViewController{
    
    //registers a view controller to keyboard events (show, hide events of virtual keyboard)
    func registerToKeyBoardEvents(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //trigered when the virtual keyboard is shown
    @objc func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -70 // Move view 150 points upward
    }
    
    //trigered when the virtual keyboard is hidden
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0 // Move view to original position
    }
    
    //unregisters from keyboard events
    func unregisterFromKeyBoardEvents(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    /**
     This is a method that animates a round checkbox button.
     
     - Parameter button: The button to animate.
     - Parameter state: The state of animation (true -> button is checked, false -> button not checked).
     - Parameter backgroundColor : The background you want to set to the button
     - Parameter image: The image you want to add to the button
     - Parameter alpha: The alpha value of the buttons color, default value is 0.7
     */
    
    func animateRoundButton(button:UIButton, state:Bool, backgroundColor:UIColor = UIColor.white, image: UIImage, alpha: Float = 0.7){
        if state {
            button.setImage(image, for: .normal)
            button.backgroundColor = backgroundColor
            button.alpha = 0.7
        }
        else{
            button.setImage(image, for: .normal)
            button.backgroundColor = backgroundColor
        }
    }
    
    /**
     This is a method that creates a round button.
     - Parameter button: The button to round.
     */
    func createRoundButton(button:UIButton){
        button.layer.cornerRadius = button.bounds.size.height / 2
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 0.0
        button.clipsToBounds = true
        button.contentMode = .scaleToFill
    }
    
    /**
     This is a method that creates a round button.
     - Parameter imageView: The imageView to round.
     - Parameter borderColor: The color of the border around the round image
     */
    func createRound(imageView: UIImageView, borderColor:UIColor ){
        imageView.layer.borderWidth = 2
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = borderColor.cgColor
        imageView.layer.cornerRadius = imageView.frame.height/2
        imageView.clipsToBounds = true
    }
    
    func createRoundView(_ view: UIView){
        view.layer.cornerRadius = view.frame.height/2
        view.clipsToBounds = true
    }
    
    /**
     This is a method that animates checkbuttons in a button group.
     
     - Parameter tag: The tag of the button to check.
     - Parameter maxTags: The maximum tag of the buttons in the group.
     - Parameter checkedBckgColor : The background you want to set to the checked button
     - Parameter uncheckedBckgColor : The background you want to set to the unchecked button
     - Parameter checkedImage: The image you want to add to the checked button
     - Parameter uncheckedImage: The image you want to add to the unchecked button
     - Parameter alpha: The alpha value of the buttons color, default value is 1
     */
    func toogleButtonWith(tag: Int, maxTags:Int, checkedBckgColor:String, uncheckedBckgColor: String ,checkedImage : UIImage, uncheckedImage:UIImage, alpha: Float = 1.0){
        
        if let button = self.view.viewWithTag(tag) as? UIButton{
            button.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            button.setImage(checkedImage, for: .normal)
            button.backgroundColor = createColorFromHex(hex: checkedBckgColor)
            button.alpha = 1
            
            
            UIView.animate(withDuration: 2.0,delay: 0,usingSpringWithDamping: 0.2, initialSpringVelocity: 6.0, options: .allowUserInteraction,animations: { [weak self] in
                button.transform = .identity
                }, completion: nil)
        }
        
        for otherTag in 1...maxTags {
            if otherTag != tag {
                if let otherButton = self.view.viewWithTag(otherTag) as? UIButton{
                    otherButton.setImage(uncheckedImage, for: .normal)
                    otherButton.backgroundColor = createColorFromHex(hex: uncheckedBckgColor)
                    otherButton.alpha = 1
                }
            }
        }
    }
    
    
    /**
     This is a method that animates an activity indicator in a view.
     
     - Parameter state: The state of the animation. True (default value)-> start animating, false -> hide animation.
     - Parameter activityIndicator: The activity indicator to animate.
     */
    func animate(_ state: Bool = true, activityIndicator: UIActivityIndicatorView){
        if state {
            activityIndicator.startAnimating()
            view.isUserInteractionEnabled = false
        } else {
            activityIndicator.stopAnimating()
            view.isUserInteractionEnabled = true
        }
    }
    
    /**
     This is a method that animates an activity indicator in a view.
     
     - Parameter email: The string that you want to validate
     - Returns the result of the validation
     */
    func validateEmail(email:String)->Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    /**
     This is a method that shares an image with other applications .
     
     - Parameter image: The image you want to share
     - Parameter test: The text that you want to share
     */
    func share(image: UIImage, text:String){
        let imageToShare = image
        let textToShare = text
        let activityViewController = UIActivityViewController(activityItems: [textToShare, imageToShare], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    /**
     This is a method that creates a button with round corners .
     
     - Parameter button: The button that you want to have round corners
     - Parameter cornerRadius: The value of the corners (sugested value is 5)
     */
    func createRoundCorner(_ button:UIButton, _ cornerRadius:CGFloat){
        button.layer.cornerRadius = cornerRadius
    }
    
    /**
     This is a method that creates an image with round corners .
     
     - Parameter image: The image that you want to have round corners
     - Parameter cornerRadius: The value of the corners (sugested value is 5)
     */
    func createRoundCorner(_ image:UIImageView, _ cornerRadius:CGFloat){
        image.layer.cornerRadius = cornerRadius
    }
    
    /**
     This is a method that creates a view with round corners .
     
     - Parameter view: The view that you want to have round corners
     - Parameter cornerRadius: The value of the corners (sugested value is 5)
     */
    func createRoundCorner(_ view:UIView, _ cornerRadius:CGFloat){
        view.layer.cornerRadius = cornerRadius
    }
    
    /**
     This is a method that adds a bottom border to a view .
     
     - Parameter custView: The view that you want to add the bottom border
     - Parameter color: The color of the border
     - Parameter borderWidth: The widht of the border
     */
    
    func addBootomBorderTo(custView:UIView,color:String, borderWidth: CGFloat = 2){
        let col = createColorFromHex(hex: color)
        let border = CALayer()
        
        let screenBounds = UIScreen.main.bounds
        let screenHeight = screenBounds.height
        
        let x = custView.frame.origin.x
        
        border.borderColor = col.cgColor
        border.frame = CGRect(x: x, y: 57 , width:  screenHeight * 3, height: 1)
        
        border.borderWidth = borderWidth
        custView.layer.addSublayer(border)
        custView.layer.masksToBounds = true
    }
    
    /**
     This is a method that adds a bottom border to a button .
     
     - Parameter button: The button that you want to add the bottom border
     - Parameter color: The color of the border
     - Parameter borderWidth: The widht of the border
     */
    func addBootomBorderTo(button:UIButton, color:String, borderWidth: CGFloat = 2){
        let col = createColorFromHex(hex: color)
        let border = CALayer()
        
        let screenBounds = UIScreen.main.bounds
        let screenHeight = screenBounds.height
        
        let x = button.frame.origin.x
        
        
        border.borderColor = col.cgColor
        border.frame = CGRect(x: x, y: 57 , width:  screenHeight * 3, height: 1)
        
        border.borderWidth = borderWidth
        button.layer.addSublayer(border)
        button.layer.masksToBounds = true
    }
    
    /**
     This is a method that adds a bottom border to a textField .
     
     - Parameter textField: The textfield that you want to add the bottom border
     - Parameter color: The color of the border
     - Parameter borderWidth: The widht of the border
     */
    func addBootomBorderTo(textField: UITextField,color:String, borderWidth: CGFloat = 2){
        let col = createColorFromHex(hex: color)
        let border = CALayer()
        
        let screenBounds = UIScreen.main.bounds
        let screenHeight = screenBounds.height
        
        let x = textField.frame.origin.x
        
        
        border.borderColor = col.cgColor
        border.frame = CGRect(x: x, y: 57 , width:  screenHeight * 3, height: 1)
        
        border.borderWidth = borderWidth
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
    }
    
    
    /**
     This is a method that creates a simmple alert .
     
     - Parameter title: The title of the alert
     - Parameter message: The message of the alert
     */
    
    func createSimpleAlert(title:String,message:String)->UIAlertController{
        //Creating the alert controller
        //It takes the title and the alert message and prefferred style
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        //then we create a default action for the alert...
        //It is actually a button and we have given the button text style and handler
        //currently handler is nil as we are not specifying any handler
        let defaultAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        //now we are adding the default action to our alertcontroller
        alertController.addAction(defaultAction)
        return alertController
        //and finally presenting our alert using this method
    }
    
    /**
     This is a method that displays an alert.
     - Parameter title:String  The title of the animation
     - Parameter message:String The message that will be displayed to the user.
     */
    func displayAlert(title:String,message:String){
        let alert = createSimpleAlert(title: title, message: message)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    /**
     This is a method that displays an network alert and allows the user to access the settings afterwards.
     - Parameter title:String  The title of the animation
     - Parameter message:String The message that will be displayed to the user.
     */
    
    func presentNetworkAlert(title:String, message: String) {
        let alertController = UIAlertController (title: title, message: message, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            UIApplication.shared.open(URL(string:"App-Prefs:root=General")!,options:[:], completionHandler:nil)
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    /**
     This is a method that displays an network alert and allows the user to access the settings afterwards.
     - Parameter title:String  The title of the animation
     - Parameter message:String The message that will be displayed to the user.
     */
    func presentGpsAlert(title:String, message: String) {
        let alertController = UIAlertController (title: title, message: message, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            UIApplication.shared.open(URL(string:"App-Prefs:root=General")!,options:[:], completionHandler:nil)
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    /**
     This is a method that creates an image or icon with a specified tint color.
     - Parameter tint: the color that we want to convert the image in
     - Parameter imageName: The name of the image we are trying to change the color of.
     - Parameter imageView: The imageview where we are trying to set the new image with the new color
     */
    func createImageWith(tint:UIColor, imageName:String, imageView:UIImageView){
        let image = (UIImage(named:imageName)?.withRenderingMode(.alwaysTemplate))!
        imageView.tintColor = tint
        imageView.image = image
    }
    
    /**
     This is a method prepares the image for changing its tint
     - Parameter imageName: The name of the image we are trying to change the color of.
     */
    func prepareImageForTint(imageName:String)->UIImage{
        return (UIImage(named:imageName)?.withRenderingMode(.alwaysTemplate))!
    }
    
    /**
     This is a method hides the keyboard when the user taps anywhere in the view
     */
    
    func hideKeyboardOnViewTap()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    
    /**
     This method dismisses the keyboard from the view
     */
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    /**
     This method creates a round table cell view
     -Parameter: view : the view we want to round
     -Parameter: cornerRadius : the radius of the round corner( default value is 5)
     */
    
    func generateRoundTableCellView(view: UIView, cornerRadius: CGFloat = 5){
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = false
    }
    
    /**
     This method adds a shadow to a view
     
     -Parameter: view : the view we want to round
     -Parameter: color : the color of the shadow
     -Parameter: opacity : the opacity of the shadow
     -Parameter: shadowRadius : the shadowRadius
     */
    func addShadowTo(view:UIView, color:String, opacity: Float, shadowRadius: CGFloat){
        let col = createColorFromHex(hex:color)
        view.layer.shadowColor = col.cgColor
        view.layer.shadowOpacity = opacity
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = shadowRadius
        view.layer.masksToBounds = false
    }
    
    /**
     This method returns to the main que and performs all operations on that queue
     
     -Parameter: updates : the updates that you want to perform on main
     */
    func performUIUpdatesOnMain(_ updates: @escaping () -> Void) {
        DispatchQueue.main.async {
            updates()
        }
    }
    
    /**
     This method hides the navigation bar
     */
    
    func hideNavigationBar(){
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    /**
     This method shows the navigation bar
     */
    func showNavigationBar() {
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    /**
     This method hides the tool bar
     */
    func hideToolBar(){
        // Hide the navigation bar on the this view controller
        self.navigationController?.setToolbarHidden(true, animated: true)
    }
    
    /**
     This method shows the navigation bar
     */
    func showToolBar() {
        // Show the navigation bar on other view controllers
        self.navigationController?.setToolbarHidden(false, animated: true)
    }
    /**
     This method hides the tab bar
     */
    func hideTabBar(){
        // Hide the navigation bar on the this view controller
        self.tabBarController?.tabBar.isHidden = true
    }
    
    /**
     This method shows the tab bar
     */
    func showTabBar() {
        // Show the navigation bar on other view controllers
        self.tabBarController?.tabBar.isHidden = false
    }
    
    /**
     This method presents a top notification. It requires the isntallation of import BRYXBanner pod
     
     -Parameter: vititleew : the title of the notification
     -Parameter: subtitle : the subtitle of the notification
     -Parameter: image : the name of the image that we want to add to the notification
     -Parameter: color : the bacground color that we want to add to the notification
     -Parameter: duration : the duration of the notification
     */
    
    func presentTopNotification(title: String, subtitle: String, image: String, color: String, duration: TimeInterval){
        
        let backgroundColor = createColorFromHex(hex: color)
        let banner = Banner(title: title, subtitle: subtitle, image: UIImage(named: image), backgroundColor: backgroundColor)
        banner.dismissesOnTap = true
        banner.show(duration: duration)
    }
    
    /**
     This method is used to filter GroupedWishlists based on start date and end date
     -Parameter: startDate : optional parameter
     -Parameter: endDate : optional parameter
     -Parameter: warehouseId : the id of the warehouse of which we want to get the wishlists
     */
    
    
    /**
     This method is used to filter WishlistWarehouses by date and warehouse_id
     */
    
    
    /**
     This method is used to get the authorization token after the login 
     */
    //############################################## API CALL #####################################################
    
    func waitForNewAuthorizationKey() {
        let semaphore = DispatchSemaphore(value: 0)
        if Device.hasNetConnection(){
            let parameters = ApiRequest.constructOAuthParameters()
            getApiClient().fetchApiResults(urlString: Urls.GET_OAUTH_KEY, parameters: parameters, method: "POST", callback: {(results : AuthorizationKeyStruct)->()
                in
                SavedVariables.setTokenType(tokenType: results.token_type)
                SavedVariables.setExpiresIn(expiresIn: results.expires_in)
                SavedVariables.setAccessToken(access_token: results.access_token)
                SavedVariables.setRefreshToken(refresh_token: results.refresh_token)
                semaphore.signal()
                self.getUserPermissions()
            })
        }
        else{
            presentNoNetPopup()
        }
        
        let timeout = DispatchTime.now() + DispatchTimeInterval.seconds(15)
        if semaphore.wait(timeout:timeout) == DispatchTimeoutResult.timedOut{
            presentApiErrorPopup(statusCode: StatusCodes.COULD_NOT_GET_TOKEN)
        }
    }
    
    /**
        this function gets the user permissions from the server
    */
    func getUserPermissions(){
        getApiClient().fetchApiResults(urlString: Urls.GET_USER_PERMISSIONS, parameters:EmptyParamStruct() , method: "GET", model:"UserPermissions", callback: {(response: GenericResponse<UserPermissions>)->()
            in
            self.performUIUpdatesOnMain {
                if response.status != 0 {
                    self.presentApiErrorPopup(statusCode: response.status!)
                    return
                }
                if response.data.count > 0 {
                    SavedVariables.setUserPermissions(permissions:response.data[0].permissions!)
                    //TODO : UNCOMENT AFTER FINISHIN TESTIN
                    //storeWishListsInRealm()
                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notifications.PERMISSIONS_RETRIEVED), object: nil)
                }
            }
        })
    }

    /**
     This function dismisses the popup view when the user clickes upon a black view programatically created
     */
    @objc func handleTapOutsideDismiss(blackView: UIView, whiteView: UIView){
        UIView.animate(withDuration: 0.5, animations: {
            blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow{
                whiteView.frame = CGRect(x: 0, y: window.frame.height, width: whiteView.frame.width, height: whiteView.frame.height)
            }
        })
    }
    
    /**
     This method presents a popup that notifies the user that a general server error has occured
     **/
    func presentGeneralServerErrorPopup(){
        self.presentTopNotification(title:"Error", subtitle: Strings.GENERAL_HI_SERVER_ERROR, image: "warning2",color: Colors.INOVACION_RED, duration: Constants.TOP_NOTIFICATION_DURATION )
    }
    
    /**
     This method presents a popup that notifies the user that a token has expired
     **/
    func presentTokenExpiredPopup(){
        self.presentTopNotification(title:"Error", subtitle: Strings.TOKEN_EXPIRED, image: "error",color: Colors.INOVACION_RED, duration: Constants.TOP_NOTIFICATION_DURATION )
    }
    
    /**
     This method presents a popup that notifies the user that he is not connected to the internet
     **/
    func presentNoNetPopup(){
        presentTopNotification(title:"Error", subtitle: Strings.NO_NET_CONNECTION, image: "warning2",color: Colors.INOVACION_RED, duration: Constants.TOP_NOTIFICATION_DURATION )
    }
    
    /**
     This method presents a popup that notifies the user that the number that you have called is incorrect
     **/
    func presentWrongNumber(){
        presentTopNotification(title:"Error", subtitle: Strings.NO_TELEPHONE_NUMBER, image: "warning2",color: Colors.INOVACION_RED, duration: Constants.TOP_NOTIFICATION_DURATION )
    }
    
    /**
     This method presents a popup that notifies the user that he has entered wrong credentials
     **/
    func presentWorngCredentialsError(){
        self.presentTopNotification(title: "Error", subtitle: Strings.WRONG_CREDINTIALS, image: "warning", color: Colors.INOVACION_RED, duration: Constants.TOP_NOTIFICATION_DURATION)
    }
    
    /**
     This method presents a popup that notifies the user that he has successfully completed a post request
     **/
    func presentSuccessPopup(){
        self.presentTopNotification(title: Strings.SUCCESS_TITLE, subtitle: Strings.SUCCESS_REGISTERED_REQUEST, image: "success", color: Colors.INOVACION_GREEN, duration: Constants.TOP_NOTIFICATION_DURATION)
    }
    
    /**
     This method presents a popup that notifies the user that he has not completed all required fields
     **/
    func presentFieldsMissingPopup(){
        self.presentTopNotification(title: Strings.FILL_ALL_THE_DATA, subtitle: Strings.FILL_ALL_THE_DATA, image: "error", color: Colors.INOVACION_RED, duration: Constants.TOP_NOTIFICATION_DURATION)
    }
    
    /**
     This method presents a popup that notifies the user a knwon error has occured
     -Parameter statusCode : the status code from the server response
     **/
    func presentApiErrorPopup(statusCode : Int = StatusCodes.GENERAL_ERROR){
        switch statusCode{
        case StatusCodes.COULD_NOT_GET_TOKEN :
            presentTopNotification(title: "Error", subtitle: Strings.ERROR_COULD_NOT_GET_TOKEN, image: "error", color: Colors.INOVACION_RED, duration: Constants.TOP_NOTIFICATION_DURATION)
            
        case StatusCodes.GENERAL_ERROR :
            self.presentTopNotification(title:"Error", subtitle: Strings.GENERAL_HI_SERVER_ERROR, image: "warning2",color: Colors.INOVACION_RED, duration: Constants.TOP_NOTIFICATION_DURATION )
            
        case StatusCodes.NO_RESULTS :
            self.presentTopNotification(title:"Info", subtitle: Strings.GENERAL_NO_RESULTS_MESSAGE, image: "warning",color: Colors.INOVACION_YELLOW, duration: Constants.TOP_NOTIFICATION_DURATION )
        case StatusCodes.WAREHOUSE_NOT_ACCESSABLE:
            self.presentTopNotification(title:"Error", subtitle: Strings.WAREHOUSE_NOT_ACCESSIBLE, image: "warning",color: Colors.INOVACION_RED, duration: Constants.TOP_NOTIFICATION_DURATION )
        default :
            self.presentTopNotification(title:"Error", subtitle: Strings.GENERAL_HI_SERVER_ERROR, image: "warning2",color: Colors.INOVACION_RED, duration: Constants.TOP_NOTIFICATION_DURATION )
        }
    }
    
    /**
     This Function Is Used To Request Gps Coordinates
     */
    
    //    func requestGpsCordinates(){
    //        locManager.requestWhenInUseAuthorization()
    //        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
    //            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
    //            currentLocation = locManager.location
    //            currentLatitude = currentLocation.coordinate.latitude
    //            currentLongitude = currentLocation.coordinate.longitude
    //        }
    //    }
    
    /**
     This function is used to return a random color for tableview cells
     -Returns : CGCOLOR representin a random color from the array of colors
     */
    func getRandomColor(_ index : Int)->UIColor{
        return createColorFromHex(hex: Colors.randomColors[(index % 9)])
    }
    
    /**
     This function draws a doted line between two points
     -Params : start the starting point of the dotted line you want to draw
     -Params : end the end point of the dotted line you want to draw
     */
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, color:UIColor)->CAShapeLayer {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineDashPattern = [1, 1] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        shapeLayer.fillColor = color.cgColor
        return shapeLayer
    }
    
    /**
     - This function converts an image to a base64 string
     - Parameter : image the image you want to convert to base 64
     - Returns : string repsresenting the base 64 value of the image
     */
    
    func convertToBase64(image: UIImage)->String{
        
        let imageData = UIImageJPEGRepresentation(image, Constants.BASE64_COMPRESSION_RATE)! as Data
        let base64String:String = imageData.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        let imageStr:String = base64String.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        //return "data:image/jpeg;base64,\(imageStr)"
        return imageStr
    }
    
    /**
     -This function creates a qr code in the image format from a string
     - Parameter : textToEncode represents the text you want to encode in qr format
     - Parameter : size the size of the qr code you want to display
     */
    func createQRFromString(_ textToEncode: String, size: CGSize) -> UIImage {
        let stringData = textToEncode.data(using: .utf8)
        
        let qrFilter = CIFilter(name: "CIQRCodeGenerator")!
        qrFilter.setValue(stringData, forKey: "inputMessage")
        qrFilter.setValue("H", forKey: "inputCorrectionLevel")
        
        let minimalQRimage = qrFilter.outputImage!
        let minimalSideLength = minimalQRimage.extent.width
        
        let smallestOutputExtent = (size.width < size.height) ? size.width : size.height
        let scaleFactor = smallestOutputExtent / minimalSideLength
        let scaledImage = minimalQRimage.transformed(
            by: CGAffineTransform(scaleX: scaleFactor, y: scaleFactor))
        
        return UIImage(ciImage: scaledImage,
                       scale: UIScreen.main.scale,
                       orientation: .up)
    }
    
    /**
     -This function displays an image that shows "no results"
     - Parameter : insetionView represents the view in which will be inserted the "no result image"
     - Parameter : noItemsView represents the view that will show the message
     - Parameter : itemsNumber represents the number of objects that we are counting
     */
    
    func showNoItemLogo(insetionView: UIView, noItemsView: UIView, itemsNumber: Int){
            //here I check if no item in order to display the sad face image with no articles label
            if itemsNumber == 0 {
                noItemsView.isHidden = false
                insetionView.addSubview(noItemsView)
                noItemsView.anchor(top: nil, paddingTop: 0, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 200, height: 100)
                noItemsView.centerXAnchor.constraint(equalTo: insetionView.centerXAnchor).isActive = true
                noItemsView.centerYAnchor.constraint(equalTo: insetionView.centerYAnchor).isActive = true
                return
            }
            noItemsView.isHidden = true
            noItemsView.removeFromSuperview()
    }
    
}
