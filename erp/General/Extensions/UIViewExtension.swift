//
//  UIViewExtension.swift
//  StarterProject
//
//  Created by Akil Rajdho on yyyy-mm-dd.
//  Copyright © 2018 Inovacion. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    

    /**
        Adds constraints to this `UIView` instances `superview` object to make sure this always has the same size as the superview. Please note that this has no effect if its `superview` is `nil` – add this `UIView` instance as a subview before calling this.
    */
    
    func bindFrameToSuperviewBounds() {
        guard let superview = self.superview else {
            print("Error! `superview` was nil – call `addSubview(view: UIView)` before calling `bindFrameToSuperviewBounds()` to fix this.")
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
    }
    
    /**
    This function is used to add constraints of an element in respect to its super view
     */
    
    func addContstraintsRelatedToSuperview(leading: Int, trailing: Int, top: Int, bottom: Int) {
        guard let superview = self.superview else {
            print("Error! `superview` was nil – call `addSubview(view: UIView)` before calling `bindFrameToSuperviewBounds()` to fix this.")
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-\(leading)-[subview]-\(trailing)-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(top)-[subview]-\(bottom)-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
    }

    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    /**
     This function is used to anchor a view component related to other elements in the view hierarchy
     - Parameter top : the top anchor related to another element
     - Parameter paddingTop : the top space between the current element and the top anchor of the other element
     - Parameter left : the left anchor related to another element
     - Parameter paddingLeft : the space between the left side of the element of the current element and the left anchor of the other element
     - Parameter bottom : the bottom anchor of the current element
     - Parameter paddingBottom the space between the bottom of the current element and the bottom anchor of the other element
     - Parameter right : the right anchor related to the other element
     - Parameter paddingRight : the space between the right of the current element and the right anchor of the other element
     - Height width : the height of the current element
     - Parameter width : the height of the current element
     
     * Use the function like this: someElement.anchor(top: someOtherElement.bottomAnchor, paddingTop: 10, left: someOtherElement.leftAnchor,paddingLeft: 10, bottom: nil, paddingBottom: 0, right: someOtherElement.rightAnchor, paddingRight: 40, width: 40, height: 40)
     */
    
    func anchor(top: NSLayoutYAxisAnchor?, paddingTop: CGFloat, left: NSLayoutXAxisAnchor?,paddingLeft: CGFloat, bottom: NSLayoutYAxisAnchor?, paddingBottom: CGFloat, right: NSLayoutXAxisAnchor?, paddingRight: CGFloat, width: CGFloat, height: CGFloat) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    
}
