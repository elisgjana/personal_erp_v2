//
//  WishListProductTableViewCell.swift
//  erp-ios
//
//  Created by apple on 07/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import UIKit

class OrderItemCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var scaleImage: UIImageView!
    @IBOutlet weak var priceImage: UIImageView!
    @IBOutlet weak var moneyImage: UIImageView!
    @IBOutlet weak var requiredQuantityLabel: UILabel!
    @IBOutlet weak var wishlistImage: UIImageView!
    @IBOutlet weak var purchasedQuantityLabel: UILabel!
    @IBOutlet weak var lastPriceLabel: UILabel!
    @IBOutlet weak var recommendedPriceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

