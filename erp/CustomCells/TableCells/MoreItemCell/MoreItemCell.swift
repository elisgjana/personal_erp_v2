//
//  MoreItemCell.swift
//  erp
//
//  Created by Admin on 30/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class MoreItemCell: UITableViewCell {

    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var menuNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
