//
//  SearchProductTableViewCell.swift
//  erp-ios
//
//  Created by apple on 02/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import UIKit

class SearchProductCell: UITableViewCell {

    @IBOutlet weak var productNameView: UIView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var barcodeImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var firstLetterLabel: UILabel!
    @IBOutlet weak var barcodeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
