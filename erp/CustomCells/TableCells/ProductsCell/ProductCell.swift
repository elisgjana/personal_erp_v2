//
//  AllProductsTableViewCell.swift
//  erp-ios
//
//  Created by apple on 27/01/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {
    
    @IBOutlet weak var priceSeparatorView: UIView!
    @IBOutlet weak var circle2Subview: UIView!
    @IBOutlet weak var circle1SubView: UIView!
    @IBOutlet weak var connectorView: UIView!
    @IBOutlet weak var salePriceTitle: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var idTitle: UILabel!
    @IBOutlet weak var buyPriceTitel: UILabel!
    @IBOutlet weak var itemCodeLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var buyPirceLabel: UILabel!
    @IBOutlet weak var circleView2: UIView!
    @IBOutlet weak var circleView1: UIView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var salePriceLabel: UILabel!
    @IBOutlet weak var prizeView: UIView!
    @IBOutlet weak var productNameLabel: UILabel!

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productNameBar: UIView!
    
    @IBOutlet weak var itemCodeTitle: UILabel!
    
 
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
