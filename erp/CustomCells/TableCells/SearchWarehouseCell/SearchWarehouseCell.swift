//
//  SearchWarehouseTableViewCell.swift
//  erp-ios
//
//  Created by Admin on 02/03/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import UIKit

class SearchWarehouseCell: UITableViewCell {

    @IBOutlet weak var warehouseNameLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
