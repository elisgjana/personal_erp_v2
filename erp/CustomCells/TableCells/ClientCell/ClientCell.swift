//
//  ClientCell.swift
//  erp
//
//  Created by Admin on 15/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class ClientCell: UITableViewCell {

    @IBOutlet weak var nuisImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var celImage: UIImageView!
    @IBOutlet weak var addressImage: UIImageView!
    @IBOutlet weak var subjectNameLabel: UILabel!
    @IBOutlet weak var contactNameLabel: UILabel!
    @IBOutlet weak var nuisLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var allClientsContainer: UIView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var directionButton: UIButton!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var telLabel: UILabel!
    @IBOutlet weak var actionView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
