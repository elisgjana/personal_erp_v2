//
//  PurchaseProductCell.swift
//  erp
//
//  Created by Admin on 22/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class PurchaseProductCell: UITableViewCell {
    @IBOutlet weak var requiredAmountLabel: UILabel!
    @IBOutlet weak var lastBuyPriceLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var scaleImage: UIImageView!
    @IBOutlet weak var productPriceField: UITextField!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var discountImage: UIImageView!
    @IBOutlet weak var productAmountField: UITextField!
    @IBOutlet weak var totalProductPriceLabel: UILabel!
    @IBOutlet weak var wishlistImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
