//
//  WishListTableViewCell.swift
//  erp-ios
//
//  Created by apple on 07/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import UIKit

class WishListCell: UITableViewCell {

    
    @IBOutlet weak var calendarImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var totalNumberOfProductsLabel: UILabel!
    @IBOutlet weak var warehouseNameLabel: UILabel!
    @IBOutlet weak var nrOfItemsImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
