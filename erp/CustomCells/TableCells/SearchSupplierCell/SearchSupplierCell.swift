//
//  SearchSupplierCell.swift
//  erp
//
//  Created by Admin on 20/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class SearchSupplierCell: UITableViewCell {
    
    @IBOutlet weak var supplierLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
