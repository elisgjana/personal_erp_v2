//
//  PurchaseCell.swift
//  erp
//
//  Created by Admin on 18/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class PurchaseCell: UITableViewCell {
    @IBOutlet weak var notSyncImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var supplierNameLabel: UILabel!
    @IBOutlet weak var invoiceNumberLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
