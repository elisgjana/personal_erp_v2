//
//  RejectionCell.swift
//  erp
//
//  Created by Admin on 01/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import UIKit

class RejectionCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var warehousesLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var calendarImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
