//
//  CustomProgressBar.swift
//  erp
//
//  Created by Admin on 29/08/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

class CustomProgressBar: NSObject {
    
    var shapeLayer = CAShapeLayer()
    var pulsatingLayer = CAShapeLayer()
    var trackLayer = CAShapeLayer()
    
    let blackView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.9)
        return view
    }()
    
    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 1, alpha: 0.9)
        view.layer.cornerRadius = 10
        
        //shadow
        view.layer.shadowColor = UIColor(white: 0.4, alpha: 0.4).cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 4) //makes the shadows drop to the bottom
        return view
    }()
    
    let progressLabel : UILabel = {
        let label = UILabel()
        label.text = "0.0 %"
        label.textColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        label.font = UIFont.init(name: "Palatino-Roman", size: 30)
        label.textAlignment = .center
        return label
    }()
    
    var titleLabel : UILabel = {
        let label = UILabel()
        label.text = "Duke sinkronizuar te dhënat"
        label.textColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        
        label.font = UIFont.init(name: "Palatino-Roman", size: 17)
        label.textAlignment = .center
        return label
    }()
    
    let waitingLabel : UILabel = {
        let label = UILabel()
        label.text = "Ju lutem prisni..."
        label.textColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        
        label.font = UIFont.init(name: "Palatino-Roman", size: 17)
        label.textAlignment = .center
        return label
    }()
    
    lazy var retryButton : UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "icon-retry"), for: .normal)
        btn.tintColor = .white
        btn.backgroundColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.contentEdgeInsets = UIEdgeInsetsMake(5,5,5,5)
        btn.layer.cornerRadius = 30 / 2
        return btn
    }()
    
    override init() {
        super.init()
    }
    
    func showCustomProgressBar(){
        if let window = UIApplication.shared.keyWindow{
            window.addSubview(blackView)
            blackView.anchor(top: window.topAnchor, paddingTop: 0, left: window.leftAnchor, paddingLeft: 0, bottom: window.bottomAnchor, paddingBottom: 0, right: window.rightAnchor, paddingRight: 0, width: 0, height: 0)
            
            //setup container View
            blackView.addSubview(containerView)
            containerView.anchor(top: nil, paddingTop: 0, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 280, height: 280)
            containerView.centerXAnchor.constraint(equalTo: blackView.centerXAnchor).isActive = true
            containerView.centerYAnchor.constraint(equalTo: blackView.centerYAnchor).isActive = true
            
            //setup title Label
            containerView.addSubview(titleLabel)
            titleLabel.anchor(top: containerView.topAnchor, paddingTop: 10, left: containerView.leftAnchor, paddingLeft: 8, bottom: nil, paddingBottom: 0, right: containerView.rightAnchor, paddingRight: 8, width: 0, height: 0)
            
            //setup waiting label
            containerView.addSubview(waitingLabel)
            waitingLabel.anchor(top: nil, paddingTop: 0, left: containerView.leftAnchor, paddingLeft: 8, bottom: containerView.bottomAnchor, paddingBottom: 10, right: containerView.rightAnchor, paddingRight: 8, width: 0, height: 0)
            
            //setup custom progress bar
            setupCircleLayers(view: window)
            
            //setup Progress Label
            window.addSubview(progressLabel)
            progressLabel.anchor(top: nil, paddingTop: 0, left: window.leftAnchor, paddingLeft: 8, bottom: nil, paddingBottom: 0, right: window.rightAnchor, paddingRight: 8, width: 0, height: 0)
            progressLabel.centerYAnchor.constraint(equalTo: window.centerYAnchor).isActive = true
            
            //setup Cancel Button
            containerView.addSubview(retryButton)
            retryButton.anchor(top: containerView.topAnchor, paddingTop: -15, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: containerView.rightAnchor, paddingRight: -15, width: 30, height: 30)
            retryButton.isHidden = true
        }
    }
    
    fileprivate func setupCircleLayers(view: UIView){
        //the pulsating layer
        pulsatingLayer = createCircularLayer(strokeColor: .clear, fillColor: createColorFromHex(hex: Colors.PRIMARY_DARK), view: view)
        view.layer.addSublayer(pulsatingLayer)
        animatePulsatingLayer()
        
        //the track layer
        trackLayer = createCircularLayer(strokeColor: .lightGray, fillColor: UIColor(white: 1, alpha: 0.9), view: view)
        view.layer.addSublayer(trackLayer)
        
        //the progress layer
        shapeLayer = createCircularLayer(strokeColor: createColorFromHex(hex: "33bcbf"), fillColor: .clear, view: view)
        shapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1) //rotating around z-axis
        view.layer.addSublayer(shapeLayer)
    }
    
     func animateCircle(value: Double) {
        //sets the animation from 0 to value
        shapeLayer.strokeEnd = CGFloat(value)
    }
    
    fileprivate func animatePulsatingLayer(){
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.4
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        pulsatingLayer.add(animation, forKey: "pulsing")
    }
    
    
    fileprivate func createCircularLayer(strokeColor: UIColor, fillColor: UIColor, view: UIView) -> CAShapeLayer {
        let layer = CAShapeLayer()
        
        //lets start by drawing a circle somehow
        let circularPath = UIBezierPath(arcCenter: .zero, radius: 73, startAngle: 0, endAngle: 2 * CGFloat.pi
            , clockwise: true)
        layer.path = circularPath.cgPath
        layer.position = view.center
        layer.strokeColor = strokeColor.cgColor
        layer.fillColor = fillColor.cgColor
        layer.lineCap = kCALineCapRound
        layer.lineWidth = 17
        return layer
    }
    
    func setProgressValue(currentValue: Int, totalValue: Int){
        let value = Double(currentValue) / Double(totalValue)
        let percentageValue = value * 100
        progressLabel.text = "\(percentageValue)%"
        animateCircle(value: value)
    }
    
    func dismissView(){
        blackView.removeFromSuperview()
        containerView.removeFromSuperview()
        shapeLayer.removeFromSuperlayer()
        trackLayer.removeFromSuperlayer()
        pulsatingLayer.removeFromSuperlayer()
        progressLabel.removeFromSuperview()
    }
}
