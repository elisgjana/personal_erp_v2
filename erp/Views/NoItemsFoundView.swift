//
//  NoItemsFoundView.swift
//  erp
//
//  Created by Admin on 28/08/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

class NoItemsFoundView: UIView {
    
    let sadFaceImage : UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "sad_face")
        iv.tintColor = .gray
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    let noArticlesLabel : UILabel = {
        let label = UILabel()
        label.text = Strings.NO_RESULT_FOUND
        label.textColor = .gray
        label.textAlignment = .center
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sadFaceImage.anchor(top: nil, paddingTop: 0, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 50, height: 50)
        noArticlesLabel.anchor(top: nil, paddingTop: 0, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 200, height: 40)
        let stackView = UIStackView(arrangedSubviews: [sadFaceImage, noArticlesLabel])
        stackView.distribution = .fillProportionally
        stackView.axis = .vertical
        stackView.alignment = .center

        addSubview(stackView)

        stackView.anchor(top: nil, paddingTop: 0, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 0, height: 0)
        stackView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
