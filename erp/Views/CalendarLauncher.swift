//
//  CalendarLauncher.swift
//  erp
//
//  Created by Admin on 30/08/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import UIKit

class CalendarLauncher: NSObject {
    
    let blackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.9)
        return view
    }()
    
    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 5
        return view
    }()
    
    lazy var cancelButton : UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "icon-reject"), for: .normal)
        btn.tintColor = .white
        btn.backgroundColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.contentEdgeInsets = UIEdgeInsetsMake(5,5,5,5)
        btn.layer.cornerRadius = 30 / 2
        btn.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        return btn
    }()
    
    let titleView: UIView = {
        let view = UIView()
        view.backgroundColor = createColorFromHex(hex: Colors.PRIMARY_LIGHT)
        view.layer.cornerRadius = 5
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = Strings.CALENDAR_TITLE
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = .white
        return label
    }()
    
    let datePicker: UIDatePicker = {
        let dp = UIDatePicker()
        dp.datePickerMode = UIDatePickerMode.date
        let dateFormatter = DateFormatter() //this dateformater is used to sent date in server for filter
        dateFormatter.dateFormat = Strings.DATE_FORMATER_YYYY_MM_DD
//        let selectedDateForFilter = dateFormatter.string(from: dp.date)
//        string = selectedDateForFilter
        return dp
    }()
    
    let chooseButton: UIButton = {
        let btn = UIButton()
        btn.setTitle(Strings.CHOOSE, for: .normal)
        btn.backgroundColor = createColorFromHex(hex: Colors.PRIMARY_DARK)
        btn.layer.cornerRadius = 5
        btn.titleLabel?.textColor = .white
        return btn
    }()
    
    override init() {
        super.init()
    }
    
    func showCalendar(){
        if let window = UIApplication.shared.keyWindow{
            window.addSubview(blackView)
            blackView.anchor(top: window.topAnchor, paddingTop: 0, left: window.leftAnchor, paddingLeft: 0, bottom: window.bottomAnchor, paddingBottom: 0, right: window.rightAnchor, paddingRight: 0, width: 0, height: 0)
            
            //setup container View
            blackView.addSubview(containerView)
            containerView.anchor(top: nil, paddingTop: 0, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 285, height: 260)
            containerView.centerXAnchor.constraint(equalTo: blackView.centerXAnchor).isActive = true
            containerView.centerYAnchor.constraint(equalTo: blackView.centerYAnchor).isActive = true
            
            containerView.alpha = 0
            containerView.layer.transform = CATransform3DMakeScale(0, 0, 0)
            
            //setup title view
            containerView.addSubview(titleView)
            titleView.anchor(top: containerView.topAnchor, paddingTop: 0, left: containerView.leftAnchor, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: containerView.rightAnchor, paddingRight: 0, width: 0, height: 50)
            
            //setup title label
            titleView.addSubview(titleLabel)
            titleLabel.anchor(top: nil, paddingTop: 0, left: titleView.leftAnchor, paddingLeft: 8, bottom: nil, paddingBottom: 0, right: titleView.rightAnchor, paddingRight: 8, width: 0, height: 0)
            titleLabel.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
            
            //setup Cancel Button
            containerView.addSubview(cancelButton)
            cancelButton.anchor(top: containerView.topAnchor, paddingTop: -15, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: containerView.rightAnchor, paddingRight: -15, width: 30, height: 30)
            
            //setup DatePicker
            containerView.addSubview(datePicker)
            datePicker.anchor(top: titleView.bottomAnchor, paddingTop: 0, left: containerView.leftAnchor, paddingLeft: 12, bottom: nil, paddingBottom: 0, right: containerView.rightAnchor, paddingRight: 12, width: 0, height: 165)
            
            //setup choose button
            containerView.addSubview(chooseButton)
            chooseButton.anchor(top: datePicker.bottomAnchor, paddingTop: 5, left: nil, paddingLeft: 0, bottom: nil, paddingBottom: 0, right: nil, paddingRight: 0, width: 150, height: 30)
            chooseButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.containerView.alpha = 1
                self.containerView.layer.transform = CATransform3DMakeScale(1, 1, 1)
                
            }, completion: { (completed) in
            })
        }
    }
    
    @objc func handleDismiss(){
        UIView.animate(withDuration: 0.5, animations:{
          self.blackView.removeFromSuperview()
        })
    }
    
    func getSelectedDate() -> String{
        let dateFormatter = DateFormatter() //this dateformater is used to sent date in server for filter
        dateFormatter.dateFormat = Strings.DATE_FORMATER_YYYY_MM_DD
        let date = dateFormatter.string(from: datePicker.date)
        return date
    }
}
