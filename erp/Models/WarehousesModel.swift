//
//  AllWarehousesModel.swift
//  erp-ios
//
//  Created by Admin on 27/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class WarehouseModel: Object{
    var warehouse_id = RealmOptional<Int32>()
    dynamic var code: String? = ""
    dynamic var barcode: String? = ""
    dynamic var name: String? = ""
    dynamic var type: String? = ""
    dynamic var is_active: Bool? = true
    
    override static func primaryKey() -> String? {
        return "warehouse_id"
    }
}

extension WarehouseModel{
    func writeToRealm(){
        try! uiRealm.write{
            uiRealm.add(self, update: true)
        }
    }
}

//get data stored from api and then store them in Realm Database
func storeWarehousesInRealm(){
    for warehouse in getAllWarehouses(){
        let warehouseToAdd = WarehouseModel()
        warehouseToAdd.warehouse_id.value = warehouse.id
        warehouseToAdd.code = warehouse.code
        warehouseToAdd.barcode = warehouse.barcode
        warehouseToAdd.name = warehouse.name
        warehouseToAdd.type = warehouse.type
        warehouseToAdd.is_active = warehouse.is_active
        warehouseToAdd.writeToRealm()
    }
}

func deleteWarehousesFromRealm(){
    let results = uiRealm.objects(WarehouseModel.self)
    if results.count > 0{
        try! uiRealm.write {
            uiRealm.delete(results)
        }
    }
}


