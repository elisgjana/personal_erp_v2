//
//  SuppliersModel.swift
//  erp
//
//  Created by Admin on 20/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class SupplierModel: Object{
    dynamic var nuis: String? = ""
    dynamic var uuid: String? = ""
    dynamic var name: String? = ""
    dynamic var owner: String? = ""
    var city_id = RealmOptional<Int32>()
    dynamic var city: String? = ""
    dynamic var zone1: String? = ""
    dynamic var phone: String? = ""
    dynamic var mobile_phone: String? = ""
    dynamic var email: String? = ""
    dynamic var website: String? = ""
    dynamic var contact_name: String? = ""
    var latitude = RealmOptional<Double>()
    var longitude = RealmOptional<Double>()
    var notes: String? = ""
    var is_active = RealmOptional<Bool>()
    var status_id = RealmOptional<Int32>()
    var status: String? = ""
    var industry_type: String? = ""
    var business_type: String? = ""
    var business_type_id = RealmOptional<Int32>()
    var industry_type_id = RealmOptional<Int32>()
    
    override static func primaryKey() -> String? {
        return "uuid"
    }
    
}
extension SupplierModel{
    func writeToRealm(){
        try! uiRealm.write{
            uiRealm.add(self, update: true)
        }
    }
}

//get data stored from api and then store them in Realm Database
func storeSuppliersInRealm(){
    for suppliers in getAllSuppliers(){
        let supplierToAdd = SupplierModel()
        supplierToAdd.name = suppliers.name
        supplierToAdd.nuis = suppliers.nuis
        supplierToAdd.uuid = suppliers.uuid
        supplierToAdd.owner = suppliers.owner
        supplierToAdd.city_id.value = suppliers.city_id
        supplierToAdd.city = suppliers.city
        supplierToAdd.zone1 = suppliers.zone
        supplierToAdd.phone = suppliers.phone
        supplierToAdd.mobile_phone = suppliers.mobile_phone
        supplierToAdd.email = suppliers.email
        supplierToAdd.website = suppliers.email
        supplierToAdd.contact_name = suppliers.contact_name
        supplierToAdd.latitude.value = suppliers.latitude
        supplierToAdd.longitude.value = suppliers.longitude
        supplierToAdd.notes = suppliers.notes
        supplierToAdd.is_active.value = suppliers.is_active
        supplierToAdd.status_id.value = suppliers.status_id
        supplierToAdd.status = suppliers.status
        supplierToAdd.industry_type = suppliers.industry_type
        supplierToAdd.business_type = suppliers.business_type
        supplierToAdd.business_type_id.value = suppliers.business_type_id
        supplierToAdd.industry_type_id.value = suppliers.industry_type_id
        supplierToAdd.writeToRealm()
    }
}

func deleteSuppliersFromRealm(){
    let results = uiRealm.objects(SupplierModel.self)
    if results.count > 0{
        try! uiRealm.write {
            uiRealm.delete(results)
        }
    }
}

