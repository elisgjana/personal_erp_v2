//
//  PurchaseImagesModel.swift
//  erp
//
//  Created by Admin on 09/06/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class PurchaseImageModel: Object{
    
    dynamic var photo: String? = ""
    dynamic var uuid: String? = ""
    
    override static func primaryKey() -> String? {
        return "uuid"
    }
}

extension PurchaseImageModel{
    func writeToRealm(){
        try! uiRealm.write{
            uiRealm.add(self, update: true)
        }
    }
}

//get data stored from api and then store them in Realm Database
func storePurchaseImagesInRealm(){
    for images in getAllPurchaseImages(){
        let imageToAdd = PurchaseImageModel()
        imageToAdd.photo = images.photo
        imageToAdd.uuid = images.uuid
        imageToAdd.writeToRealm()
    }
}

func deletePurchaseImagesFromRealm(){
    let results = uiRealm.objects(PurchaseImageModel.self)
    if results.count > 0{
        try! uiRealm.write {
            uiRealm.delete(results)
        }
    }
}

func storeIndividualPurchaseImageToRealm(purchaseImage: PurchaseImage){
    let imageToAdd = PurchaseImageModel()
    imageToAdd.uuid = purchaseImage.uuid
    imageToAdd.photo = purchaseImage.photo
    imageToAdd.writeToRealm()
}
