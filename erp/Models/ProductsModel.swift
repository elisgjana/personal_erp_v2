//
//  AllProductsModel.swift
//  erp-ios
//
//  Created by apple on 10/02/2018.
//  Copyright © 2018 inovacion. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class ProductModel: Object{
    var id = RealmOptional<Int32>()
    dynamic var code: String? = ""
    dynamic var uuid: String? = ""
    dynamic var name: String? = ""
    dynamic var description1: String? = ""
    dynamic var external_bar_code: String? = ""
    dynamic var internal_bar_code: String? = ""
    dynamic var measurement_unit: String? = ""
    var sale_price = RealmOptional<Double>()
    var buy_price = RealmOptional<Double>()
    var total_sales = RealmOptional<Double>()
    var total_wasted = RealmOptional<Double>()
    var product_category_id = RealmOptional<Int32>()
    var product_type_id = RealmOptional<Int32>()
    dynamic var product_type: String? = ""
    var price_change_days = RealmOptional<Double>()
    dynamic var last_price_changed_date: String? = ""
    dynamic var last_price_changed_by: String? = ""
    var is_active: Bool? = true
    dynamic var photo: String? = ""
    
    override static func primaryKey() -> String? {
        return "uuid"
    }
}

extension ProductModel{
    func writeToRealm(){
        try! uiRealm.write{
            uiRealm.add(self, update: true)
        }
        
    }
}

//get data stored from api and then store them in Realm Database
func storeProductsInRealm(){
    for products in getAllProducts(){
        let productToAdd = ProductModel()
        productToAdd.id.value = products.id
        productToAdd.name = products.name
        productToAdd.code = products.code
        productToAdd.uuid = products.uuid
        productToAdd.description1 = products.description
        productToAdd.external_bar_code = products.external_bar_code
        productToAdd.internal_bar_code = products.internal_bar_code
        productToAdd.measurement_unit = products.measurement_unit
        productToAdd.total_sales.value = products.total_sales
        productToAdd.sale_price.value = products.sale_price
        productToAdd.buy_price.value = products.buy_price
        productToAdd.total_wasted.value = products.total_wasted
        productToAdd.product_category_id.value = products.product_category_id
        productToAdd.product_type_id.value = products.product_type_id
        productToAdd.product_type = products.product_type
        productToAdd.price_change_days.value = products.price_change_days
        productToAdd.last_price_changed_date = products.last_price_changed_date
        productToAdd.last_price_changed_by = products.last_price_changed_by
        productToAdd.is_active = products.is_active
        productToAdd.photo = products.photo
        productToAdd.writeToRealm()
    }
}

func deleteProductsFromRealm(){
    let results = uiRealm.objects(ProductModel.self)
    if results.count > 0{
        try! uiRealm.write {
            uiRealm.delete(results)
        }
    }
}
