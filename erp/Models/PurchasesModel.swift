//
//  PurchasesModel.swift
//  erp
//
//  Created by Admin on 18/04/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class PurchaseModel: Object{
    dynamic var uuid: String? = ""
    dynamic var date: Date? = convertStringToDate(Device.getCurrentDate())
    dynamic var invoice_no: String? = ""
    dynamic var serial_no: String? = ""
    var user_id = RealmOptional<Int32>()
    dynamic var supplier_uuid: String? = ""
    dynamic var supplier_name: String? = ""
    var delivered = RealmOptional<Bool>()
    dynamic var notes: String? = ""
    dynamic var payment_method: String? = ""
    var payment_value = RealmOptional<Double>()
    var value = RealmOptional<Double>()
    var discount = RealmOptional<Double>()
    var debt = RealmOptional<Double>()
    var latitude = RealmOptional<Double>()
    var longitude = RealmOptional<Double>()
    var status_id = RealmOptional<Int32>()
    var warehouse_id = RealmOptional<Int32>()
    var is_sync = RealmOptional<Bool>()
    dynamic var warehouse_name: String? = ""
    dynamic var sort_date: String? = ""
    var payments = List<PaymentModel>()
    var details = List<DetailModel>()
    
    override static func primaryKey() -> String? {
        return "uuid"
    }
}
@objcMembers class PaymentModel: Object{
    dynamic var purchase_uuid: String? = ""
    var amount = RealmOptional<Double>()
    dynamic var payment_method: String? = ""
    dynamic var notes: String? = ""
    dynamic var date: String? = ""
    var user_id = RealmOptional<Int32>()
    dynamic var user_full_name: String? = ""
    
    override static func primaryKey() -> String? {
        return "purchase_uuid"
    }
}
@objcMembers class DetailModel: Object{
    var id = RealmOptional<Int32>()
    dynamic var purchase_uuid: String? = ""
    var product_id = RealmOptional<Int32>()
    dynamic var product_name: String? = ""
    dynamic var image_url: String? = ""
    dynamic var measurement_unit: String? = ""
    var amount = RealmOptional<Double>()
    var product_price = RealmOptional<Double>()
    var last_buy_price = RealmOptional<Double>()
    var discount = RealmOptional<Double>()
    var total = RealmOptional<Double>()
    
    override static func primaryKey() -> String? {
        return "purchase_uuid"
    }
}

extension PurchaseModel{
    func writeToRealm(){
        try! uiRealm.write{
            uiRealm.add(self, update: true)
        }
        
    }
}

//get data stored from api and then store them in Realm Database
func storePurchasesInRealm(){
    for purchase in getAllPurchases(){
        let purchaseToAdd = PurchaseModel()
        purchaseToAdd.uuid = purchase.uuid
        purchaseToAdd.date = convertStringToDate(purchase.date!)
        purchaseToAdd.invoice_no = purchase.invoice_no
        purchaseToAdd.warehouse_id.value = purchase.warehouse_id
        purchaseToAdd.warehouse_name = purchase.warehouse_name
        purchaseToAdd.serial_no = purchase.serial_no
        purchaseToAdd.user_id.value = purchase.user_id
        purchaseToAdd.supplier_uuid = purchase.supplier_uuid
        purchaseToAdd.supplier_name = purchase.supplier_name
        purchaseToAdd.delivered.value = purchase.delivered
        purchaseToAdd.notes = purchase.notes
        purchaseToAdd.payment_method = purchase.payment_method
        purchaseToAdd.payment_value.value = purchase.payment_value
        purchaseToAdd.discount.value = purchase.discount
        purchaseToAdd.value.value = purchase.value 
        purchaseToAdd.latitude.value = purchase.latitude
        purchaseToAdd.longitude.value = purchase.longitude
        purchaseToAdd.status_id.value = purchase.status_id
        purchaseToAdd.debt.value = purchase.debt
        purchaseToAdd.is_sync.value = purchase.is_sync
        purchaseToAdd.sort_date = purchase.sort_date
        for payments in purchase.payments!{
            let paymentModel = PaymentModel()
            paymentModel.purchase_uuid = payments.purchase_uuid
            paymentModel.amount.value = payments.amount
            paymentModel.payment_method = payments.payment_method
            paymentModel.notes = payments.notes
            paymentModel.date = payments.date
            paymentModel.user_id.value = payments.user_id
            paymentModel.user_full_name = payments.user_full_name
            purchaseToAdd.payments.append(paymentModel)
        }
        for details in purchase.details!{
            let detailModel = DetailModel()
            detailModel.id.value = purchase.id
            detailModel.purchase_uuid = details.purchase_uuid
            detailModel.product_id.value = details.product_id
            detailModel.product_name = details.product_name
            detailModel.image_url = details.image_url
            detailModel.measurement_unit = details.measurement_unit
            detailModel.amount.value = details.amount
            detailModel.product_price.value = details.product_price
            detailModel.last_buy_price.value = details.last_buy_price
            detailModel.discount.value = details.discount
            detailModel.total.value = details.total
            purchaseToAdd.details.append(detailModel)
        }
        purchaseToAdd.writeToRealm()
    }
}

func storeIndividualPurchaseToRealm(purchase: Purchase){
    let purchaseToAdd = PurchaseModel()
    purchaseToAdd.uuid = purchase.uuid
    purchaseToAdd.date = convertStringToDate(purchase.date!)
    purchaseToAdd.invoice_no = purchase.invoice_no
    purchaseToAdd.warehouse_id.value = purchase.warehouse_id
    purchaseToAdd.warehouse_name = purchase.warehouse_name
    purchaseToAdd.serial_no = purchase.serial_no
    purchaseToAdd.user_id.value = purchase.user_id
    purchaseToAdd.supplier_uuid = purchase.supplier_uuid
    purchaseToAdd.supplier_name = purchase.supplier_name
    purchaseToAdd.delivered.value = purchase.delivered
    purchaseToAdd.notes = purchase.notes
    purchaseToAdd.payment_method = purchase.payment_method
    purchaseToAdd.payment_value.value = purchase.payment_value
    purchaseToAdd.discount.value = purchase.discount
    purchaseToAdd.value.value = purchase.value
    purchaseToAdd.latitude.value = purchase.latitude
    purchaseToAdd.longitude.value = purchase.longitude
    purchaseToAdd.status_id.value = purchase.status_id
    purchaseToAdd.debt.value = purchase.debt
    purchaseToAdd.is_sync.value = false
    purchaseToAdd.sort_date = purchase.sort_date
    for payments in purchase.payments!{
        let paymentModel = PaymentModel()
        paymentModel.purchase_uuid = payments.purchase_uuid
        paymentModel.amount.value = payments.amount
        paymentModel.payment_method = payments.payment_method
        paymentModel.notes = payments.notes
        paymentModel.date = payments.date
        paymentModel.user_id.value = payments.user_id
        paymentModel.user_full_name = payments.user_full_name
        purchaseToAdd.payments.append(paymentModel)
    }
    for details in purchase.details!{
        let detailModel = DetailModel()
        detailModel.id.value = purchase.id
        detailModel.purchase_uuid = details.purchase_uuid
        detailModel.product_id.value = details.product_id
        detailModel.product_name = details.product_name
        detailModel.image_url = details.image_url
        detailModel.measurement_unit = details.measurement_unit
        detailModel.amount.value = details.amount
        detailModel.product_price.value = details.product_price
        detailModel.last_buy_price.value = details.last_buy_price
        detailModel.discount.value = details.discount
        detailModel.total.value = details.total
        purchaseToAdd.details.append(detailModel)
    }
    purchaseToAdd.writeToRealm()
}

func deletePurchasesFromRealm(){
    let results = uiRealm.objects(PurchaseModel.self)
    if results.count > 0{
        try! uiRealm.write {
            uiRealm.delete(results)
        }
    }
}

func updatePurchase(object: PurchaseModel,value: Any, forKey: String){
    try! uiRealm.write {
        object.setValue(value, forKey: forKey)
    }
}
