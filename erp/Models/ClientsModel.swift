//
//  ClientsModel.swift
//  erp
//
//  Created by Admin on 15/05/2018.
//  Copyright © 2018 Akil Rajdho. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class ClientModel: Object{
    dynamic var nuis: String? = ""
    dynamic var uuid: String? = ""
    dynamic var name: String? = ""
    dynamic var owner: String? = ""
    var city_id = RealmOptional<Int32>()
    dynamic var city: String? = ""
    dynamic var zone1: String? = ""
    dynamic var phone: String? = ""
    dynamic var mobile_phone: String? = ""
    dynamic var email: String? = ""
    dynamic var website: String? = ""
    dynamic var contact_name: String? = ""
    var latitude = RealmOptional<Double>()
    var longitude = RealmOptional<Double>()
    var notes: String? = ""
    var is_active = RealmOptional<Bool>()
    var status_id = RealmOptional<Int32>()
    var status: String? = ""
    var industry_type: String? = ""
    var business_type: String? = ""
    var business_type_id = RealmOptional<Int32>()
    var industry_type_id = RealmOptional<Int32>()
    
    override static func primaryKey() -> String? {
        return "uuid"
    }
}

extension ClientModel{
    func writeToRealm(){
        try! uiRealm.write{
            uiRealm.add(self, update: true)
        }
        
    }
}

//get data stored from api and then store them in Realm Database
func storeClientsInRealm(){
    for clients in getAllClients(){
        let clientToAdd = ClientModel()
        clientToAdd.name = clients.name
        clientToAdd.nuis = clients.nuis
        clientToAdd.uuid = clients.uuid
        clientToAdd.owner = clients.owner
        clientToAdd.city_id.value = clients.city_id
        clientToAdd.city = clients.city
        clientToAdd.zone1 = clients.zone
        clientToAdd.phone = clients.phone
        clientToAdd.mobile_phone = clients.mobile_phone
        clientToAdd.email = clients.email
        clientToAdd.website = clients.email
        clientToAdd.contact_name = clients.contact_name
        clientToAdd.latitude.value = clients.latitude
        clientToAdd.longitude.value = clients.longitude
        clientToAdd.notes = clients.notes
        clientToAdd.is_active.value = clients.is_active
        clientToAdd.status_id.value = clients.status_id
        clientToAdd.status = clients.status
        clientToAdd.industry_type = clients.industry_type
        clientToAdd.business_type = clients.business_type
        clientToAdd.business_type_id.value = clients.business_type_id
        clientToAdd.industry_type_id.value = clients.industry_type_id
        clientToAdd.writeToRealm()
        
    }
}

func deleteClientsFromRealm(){
    let results = uiRealm.objects(ClientModel.self)
    if results.count > 0{
        try! uiRealm.write {
            uiRealm.delete(results)
        }
    }
}

